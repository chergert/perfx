lib_LTLIBRARIES =
lib_LTLIBRARIES += libperfx-1.0.la

libperfx_1_0_la_SOURCES =
libperfx_1_0_la_SOURCES += libperfx/perfx-client.c
libperfx_1_0_la_SOURCES += libperfx/perfx-client.h
libperfx_1_0_la_SOURCES += libperfx/perfx-path.c
libperfx_1_0_la_SOURCES += libperfx/perfx-path.h
libperfx_1_0_la_SOURCES += libperfx/perfx-subscription.h

libperfx_1_0_la_LIBADD =
libperfx_1_0_la_LIBADD += $(builddir)/libshared.la

libperfx_1_0_la_CPPFLAGS =
libperfx_1_0_la_CPPFLAGS += -I$(top_srcdir)/shared
