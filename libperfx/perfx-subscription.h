/* perfx-subscription.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PERFX_SUBSCRIPTION_H
#define PERFX_SUBSCRIPTION_H


#include "perfx-macros.h"
#include "perfx-types.h"
#include "perfx-value.h"


PERFX_BEGIN_DECLS


typedef enum {
   PERFX_SUBSCRIPTION_NONE    = 0,
   PERFX_SUBSCRIPTION_RECURSE = 1 << 0,
} PerfxSubscriptionFlags;


typedef void (*PerfxSubscriptionCallback) (uint32_t subscription_id,
                                           const PerfxValue *value,
                                           void *data);


PERFX_END_DECLS


#endif /* PERFX_SUBSCRIPTION_H */
