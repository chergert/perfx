/* perfx-client.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "perfx-client.h"
#include "perfx-debug.h"
#include "perfx-log.h"
#include "perfx-main.h"
#include "perfx-memory.h"


struct _PerfxClient {
   PerfxClientStateHandler state_handler;
   void *state_data;
   PerfxClientState state;
};


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClient_AddSubscription --
 *
 *       Adds a subscription to a path on the remote Perfx daemon. The
 *       subscription can be removed with PerfxClient_RemoveSubscription().
 *
 * Returns:
 *       A handler id for the subscription.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

uint32_t
PerfxClient_AddSubscription(PerfxClient *client,                /* IN */
                            PerfxPath *path,                    /* IN */
                            uint32_t interval_msec,             /* IN */
                            PerfxSubscriptionFlags flags,       /* IN */
                            PerfxSubscriptionCallback callback, /* IN */
                            void *data)                         /* IN */
{
   ENTRY;

   ASSERT(client);
   ASSERT(path);
   ASSERT(callback);

   TODO("Add subscription to daemon.");

   RETURN(0);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClient_Create --
 *
 *       Creates a new instance of PerfxClient. This structure can be used
 *       to connect to a Perfx daemon.
 *
 * Returns:
 *       A newly created PerfxClient to be freed with PerfxClient_Free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxClient *
PerfxClient_Create(void)
{
   PerfxClient *client;

   ENTRY;
   client = perfx_new0(PerfxClient, 1);
   client->state = PERFX_CLIENT_INITIAL;
   RETURN(client);
}


static void
OnConnect(void *data)
{
   PerfxClient *client = data;
   client->state_handler(client, PERFX_CLIENT_ESTABLISHED, client->state_data);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClient_Connect --
 *
 *       Connects to a Perfx daemon located at uri.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxClient_Connect(PerfxClient *client, /* IN */
                    const char *uri)     /* IN */
{
   ASSERT(client);
   ASSERT(uri);

   TODO("Connect to daemon");
   PerfxMain_AddIdle(OnConnect, client);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClient_Disconnect --
 *
 *       Disconnects from the Perfx daemon.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxClient_Disconnect(PerfxClient *client) /* IN */
{
   ASSERT(client);

   TODO("Disconnect from daemon.");
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClient_Free --
 *
 *       Frees the client and the resources associated with it.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       Everything.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxClient_Free(PerfxClient *client) /* IN */
{
   ASSERT(client);

   PerfxClient_SetStateHandler(client, NULL, NULL);
   perfx_free(client);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClient_RemoveSubscription --
 *
 *       Removes a previous subscription to a Perfx daemon.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxClient_RemoveSubscription(PerfxClient *client,      /* IN */
                               uint32_t subscription_id) /* IN */
{
   ASSERT(client);

   TODO("Remove subscription from daemon.");
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClient_SetStateHandler --
 *
 *       Sets the state callback for the PerfxClient.  handler will be
 *       called when the clients state changes.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxClient_SetStateHandler(PerfxClient *client,             /* IN */
                            PerfxClientStateHandler handler, /* IN */
                            void *data)                      /* IN */
{
   ASSERT(client != NULL);
   client->state_handler = handler;
   client->state_data = data;
}
