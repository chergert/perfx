/* perfx-client.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PERFX_CLIENT_H
#define PERFX_CLIENT_H


#include "perfx-macros.h"
#include "perfx-types.h"
#include "perfx-path.h"
#include "perfx-subscription.h"
#include "perfx-value.h"


PERFX_BEGIN_DECLS


typedef struct _PerfxClient PerfxClient;


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClientState --
 *
 *       The PerfxClientState enum represents the various states the
 *       connection to the server.
 *
 *       INITIAL:       No attempt to connect to the server has happened.
 *       CONNECTING:    Currently attempting to connect to the server.
 *       ESTABLISHED:   A connection has been established.
 *       DISCONNECTING: An established connection is disconnecting.
 *       DISCONNECTED:  The connection has been disconnected.
 *       FAILED:        A failure condition has occurred.
 *
 *--------------------------------------------------------------------------
 */

typedef enum {
   PERFX_CLIENT_INITIAL = 0,
   PERFX_CLIENT_CONNECTING,
   PERFX_CLIENT_ESTABLISHED,
   PERFX_CLIENT_DISCONNECTING,
   PERFX_CLIENT_DISCONNECTED,
   PERFX_CLIENT_FAILED,
} PerfxClientState;


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClientStateHandler --
 *
 *       A prototype to handle changes in the client state such as
 *       CONNECTING or ESTABLISHED.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

typedef void (*PerfxClientStateHandler) (PerfxClient *client,
                                         PerfxClientState state,
                                         void *data);


uint32_t     PerfxClient_AddSubscription    (PerfxClient *client,
                                             PerfxPath *path,
                                             uint32_t interval_msec,
                                             PerfxSubscriptionFlags flags,
                                             PerfxSubscriptionCallback callback,
                                             void *data);
void         PerfxClient_Connect            (PerfxClient *client,
                                             const char *uri);
PerfxClient *PerfxClient_Create             (void);
void         PerfxClient_Disconnect         (PerfxClient *client);
void         PerfxClient_Free               (PerfxClient *client);
void         PerfxClient_GetValue           (PerfxClient *client,
                                             PerfxPath *path,
                                             PerfxValueCallback callback,
                                             void *data);
void         PerfxClient_RemoveSubscription (PerfxClient *client,
                                             uint32_t subscription_id);
void         PerfxClient_SetStateHandler    (PerfxClient *client,
                                             PerfxClientStateHandler handler,
                                             void *data);
void         PerfxClient_SetValue           (PerfxClient *client,
                                             PerfxPath *path,
                                             const PerfxValue *value,
                                             PerfxValueCallback callback,
                                             void *data);


PERFX_END_DECLS


#endif /* PERFX_CLIENT_H */
