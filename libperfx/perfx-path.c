/* perfx-path.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "perfx-debug.h"
#include "perfx-log.h"
#include "perfx-memory.h"
#include "perfx-path.h"


struct _PerfxPath {
   uint32_t len;
   uint32_t parts[0];
};


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPath_Create --
 *
 *       Creates a new PerfxPath for the given string path such as
 *       "/perfx/cpu/0".
 *
 * Returns:
 *       A newly created PerfxPath.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxPath *
PerfxPath_Create(const char *path) /* IN */
{
   PerfxPath *ret;
   size_t size;
   uint32_t len = 0;
   const char *p;

   ENTRY;

   ASSERT(path);
   ASSERT(path[0] == '/');

   /*
    * Determine the amount of memory to allocate for the structure.
    */
   for (p = path; *p; p++) {
      if (*p == '/') {
         len++;
      }
   }
   size = sizeof(PerfxPath) + (len * sizeof(uint32_t));

   /*
    * Allocate the dynamically sized structure.
    */
   if (!(ret = perfx_malloc(size))) {
      PANIC("Failed to allocate memory for PerfxPath.");
   }

   TODO("Store a quark for each item in the path.");

   RETURN(ret);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPath_Free --
 *
 *       Free a PerfxPath.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       path is freed.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxPath_Free(PerfxPath *path) /* IN */
{
   perfx_free(path);
}
