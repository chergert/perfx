#include <unistd.h>

#include "perfx-debug.h"
#include "perfx-main.h"
#include "perfx-protocol.h"
#include "perfx-types.h"

#if 0
static PerfxType gTestProtocolType;
static bool      gDataReceived;

static void
TestProtocol_DataReceived(PerfxProtocol *protocol, /* IN */
                          PerfxBuffer *buffer)     /* IN */
{
   char *msg = (char *)buffer->data;

   ASSERT_STR_EQUAL("I loved that girl.", msg);
   gDataReceived = TRUE;
   PerfxMain_Quit();
}

typedef struct {
   PerfxProtocol parent;
} TestProtocol;

typedef struct {
   PerfxProtocolClass parent_class;
} TestProtocolClass;

static void
register_classes(void)
{
   PerfxProtocolClass *class;

   gTestProtocolType =
      PerfxClass_Register("TestProtocol",
                          PERFX_TYPE_PROTOCOL,
                          sizeof(PerfxProtocolClass),
                          sizeof(PerfxProtocol),
                          FALSE);
   class = (PerfxProtocolClass *)PerfxType_GetClass(gTestProtocolType);
   class->DataReceived = TestProtocol_DataReceived;
}
#endif

int
main (int argc,     /* IN */
      char *argv[]) /* IN */
{
#if 0
   PerfxProtocol *protocol;
   PerfxType type;
   char msg[] = "I loved that girl.";
   int fds[2];
   int ret;

   PerfxLog_SetUseStdout(TRUE);
   PerfxMain_Create();

   type = PerfxProtocol_GetType();
   ASSERT_INT(type, !=, 0);
   ASSERT_STR_EQUAL(PerfxType_GetName(type), "PerfxProtocol");

   register_classes();

   protocol = PerfxObject_Create(gTestProtocolType);
   ASSERT(protocol);

   ret = pipe(fds);
   ASSERT_INT(ret, ==, 0);

   PerfxProtocol_SetFd(protocol, fds[0], TRUE);

   ret = write(fds[1], msg, sizeof msg);
   ASSERT_INT(ret, ==, sizeof msg);

   PerfxMain_Run();

   PerfxObject_Unref(protocol);

   ASSERT_INT(gDataReceived, ==, TRUE);

   PerfxMain_Destroy();
#endif

   return 0;
}
