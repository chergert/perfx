#include "perfx-debug.h"
#include "perfx-path.h"

int
main (int   argc,   /* IN */
      char *argv[]) /* IN */
{
   ASSERT(PerfxPath_HasPrefix("libblah.so", "lib"));
   ASSERT(PerfxPath_HasPrefix("/tmp/libblah.so", "lib"));
   ASSERT(!PerfxPath_HasPrefix("/tmp/libblah.so", "/tmp/lib"));

   ASSERT(PerfxPath_HasSuffix("/tmp/libblah.so", ".so"));
   ASSERT(!PerfxPath_HasSuffix("/tmp/libblah.so", ".dylib"));
   ASSERT(!PerfxPath_HasSuffix("/tmp/libblah.so", "tmp"));

   return 0;
}
