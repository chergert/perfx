#include "perfx-array.h"
#include "perfx-debug.h"


static int
compare (const void *a,
         const void *b)
{
   const int *ai = a;
   const int *bi = b;
   return *bi - *ai;
}


int
main (int   argc,   /* IN */
      char *argv[]) /* IN */
{
   PerfxArray *array;
   uint32_t u = 0;
   int32_t lb = 0;
   int32_t ub = 0;

   array = PerfxArray_Create(FALSE, sizeof(uint32_t));
   ASSERT_INT(array->len, ==, 0);

   PerfxArray_Append(array, u);
   PerfxArray_Append(array, u);
   PerfxArray_Append(array, u);
   ASSERT_INT(array->len, ==, 3);

   PerfxArray_Remove(array, 0);
   ASSERT_INT(array->len, ==, 2);

   PerfxArray_RemoveFast(array, 0);
   ASSERT_INT(array->len, ==, 1);

   PerfxArray_RemoveFast(array, 0);
   ASSERT_INT(array->len, ==, 0);

   for (u = 0; u < 1024; u++) {
      ASSERT_INT(array->len, ==, u);
      PerfxArray_Append(array, u);
      ASSERT_INT(array->len, ==, u + 1);
   }
   PerfxArray_Sort(array, compare);
   for (u = 0; u < 1024; u++) {
      ASSERT_INT(1023 - u, ==, PerfxArray_Index(array, uint32_t, u));
   }

   u = 888;
   ASSERT_INT(135, ==, PerfxArray_Search(array, &u, compare));

   u = 1025;
   ASSERT_INT(-1, ==, PerfxArray_SearchFull(array, &u, compare, &lb, &ub));
   ASSERT_INT(lb, ==, -1);
   ASSERT_INT(ub, ==, 0);

   u = -1;
   ASSERT_INT(-1, ==, PerfxArray_SearchFull(array, &u, compare, &lb, &ub));
   ASSERT_INT(lb, ==, 1023);
   ASSERT_INT(ub, ==, 1024);

   u = 444;
   u = PerfxArray_Search(array, &u, compare);
   ASSERT_INT(u, ==, 579);
   PerfxArray_Remove(array, u);
   ASSERT_INT(1023, ==, array->len);
   u = 444;
   ASSERT_INT(-1, ==, PerfxArray_SearchFull(array, &u, compare, &lb, &ub));
   ASSERT_INT(lb, ==, 578);
   ASSERT_INT(ub, ==, 579);

   PerfxArray_Free(array);

   return 0;
}
