#include <string.h>

#include "perfx-bson.h"
#include "perfx-debug.h"

static const uint8_t hwbuf[] = "\x16\x00\x00\x00\x02hello\x00\x06\x00\x00\x00world\x00\x00";
static const uint8_t awebuf[] = {49, 0, 0, 0, 4, 66, 83, 79, 78, 0, 38, 0,
                                 0, 0, 2, 48, 0, 8, 0, 0, 0, 97, 119, 101,
                                 115, 111, 109, 101, 0, 1, 49, 0, 51, 51, 51,
                                 51, 51, 51, 20, 64, 16, 50, 0, 194, 7, 0,
                                 0, 0, 0};

int
main (int   argc,
      char *argv[])
{
   PerfxBson *bson;
   PerfxBson *ar;
   const uint8_t *buf;
   size_t buflen = 0;

   bson = PerfxBson_Create();
   PerfxBson_AppendInt(bson, "0", 1234);
   PerfxBson_AppendString(bson, "0", "abcd");
   PerfxBson_Free(bson);

   bson = PerfxBson_Create();
   PerfxBson_AppendString(bson, "hello", "world");
   buf = PerfxBson_GetData(bson, &buflen);
   ASSERT_INT(buflen, ==, 22);
   ASSERT_INT(0, ==, memcmp(hwbuf, buf, 22));
   PerfxBson_Free(bson);

   ar = PerfxBson_Create();
   PerfxBson_AppendString(ar, "0", "awesome");
   PerfxBson_AppendDouble(ar, "1", 5.05);
   PerfxBson_AppendInt(ar, "2", 1986);
   bson = PerfxBson_Create();
   PerfxBson_AppendArray(bson, "BSON", ar);
   PerfxBson_Free(ar);
   buf = PerfxBson_GetData(bson, &buflen);
   ASSERT_INT(buflen, ==, sizeof awebuf);
   ASSERT_INT(0, ==, memcmp(awebuf, buf, sizeof awebuf));
   PerfxBson_Free(bson);

   return 0;
}
