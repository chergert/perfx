#include "perfx-perf-tree-path.h"
#include "perfx-debug.h"

#define WAY_LONG_PATH "/0/1/2/3/4/5/6/7/8/9/10/11/12/13/14/15/16/17/18/19/20/21/22/23/24/25/26/27/28/29/30/31/32/33/34/35/36/37/38/39/40/41/42/43/44/45/46/47/48/49/50/51/52/53/54/55/56/57/58/59/60/61/62/63/64/65/66/67/68/69/70/71/72/73/74/75/76/77/78/79/80/81/82/83/84/85/86/87/88/89/90/91/92/93/94/95/96/97/98/99/100/101/102/103/104/105/106/107/108/109/110/111/112/113/114/115/116/117/118/119/120/121/122/123/124/125/126/127/128/129/130/131/132/133/134/135/136/137/138/139/140/141/142/143/144/145/146/147/148/149/150/151/152/153/154/155/156/157/158/159/160/161/162/163/164/165/166/167/168/169/170/171/172/173/174/175/176/177/178/179/180/181/182/183/184/185/186/187/188/189/190/191/192/193/194/195/196/197/198/199"
#define LONG_OK_PATH  "/0/1/2/3/4/5/6/7/8/9/10/11/12/13/14/15/16/17/18/19/20/21/22/23/24/25/26/27/28/29/30/31/32/33/34/35/36/37/38/39/40/41/42/43/44/45"
#define TOO_LONG_PATH "/0/1/2/3/4/5/6/7/8/9/10/11/12/13/14/15/16/17/18/19/20/21/22/23/24/25/26/27/28/29/30/31/32/33/34/35/36/37/38/39/40/41/42/43/44/454"


int
main (int argc,     /* IN */
      char *argv[]) /* IN */
{
   PerfxPerfTreePath *path;

   path = PerfxPerfTreePath_Create("/cpu/0/idle");
   ASSERT(path);
   ASSERT_INT(path->len, ==, 3);
   ASSERT_STR_EQUAL("cpu", PerfxPerfTreePath_GetPart(path, 0));
   ASSERT_STR_EQUAL("0", PerfxPerfTreePath_GetPart(path, 1));
   ASSERT_STR_EQUAL("idle", PerfxPerfTreePath_GetPart(path, 2));
   ASSERT(!PerfxPerfTreePath_GetPart(path, 3));
   PerfxPerfTreePath_Free(path);

   ASSERT(!PerfxPerfTreePath_Create(""));
   ASSERT(!PerfxPerfTreePath_Create("/"));
   ASSERT(!PerfxPerfTreePath_Create(TOO_LONG_PATH));
   ASSERT(!PerfxPerfTreePath_Create(WAY_LONG_PATH));

   path = PerfxPerfTreePath_Create("/a");
   ASSERT(path);
   ASSERT_INT(path->len, ==, 1);
   ASSERT_STR_EQUAL("a", PerfxPerfTreePath_GetPart(path, 0));
   ASSERT(!PerfxPerfTreePath_GetPart(path, 1));
   PerfxPerfTreePath_Free(path);

   path = PerfxPerfTreePath_Create(LONG_OK_PATH);
   ASSERT(path);
   ASSERT_INT(path->len, ==, 46);
   ASSERT_STR_EQUAL("0", PerfxPerfTreePath_GetPart(path, 0));
   ASSERT_STR_EQUAL("32", PerfxPerfTreePath_GetPart(path, 32));
   ASSERT_STR_EQUAL("45", PerfxPerfTreePath_GetPart(path, 45));
   PerfxPerfTreePath_Free(path);

   return 0;
}
