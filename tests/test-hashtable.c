#include "perfx-debug.h"
#include "perfx-hashtable.h"

int
main (int   argc,   /* IN */
      char *argv[]) /* IN */
{
   PerfxHashTable *hashtable;
   uint32_t i;

   hashtable = PerfxHashTable_Create(1024,
                                     perfx_direct_hash,
                                     perfx_direct_equal,
                                     NULL,
                                     NULL);
   for (i = 0; i < 100000; i++) {
      PerfxHashTable_Insert(hashtable, INT_TO_POINTER(i), INT_TO_POINTER(i));
   }
   ASSERT_INT(PerfxHashTable_CountKeys(hashtable), ==, 100000);
   for (i = 0; i < 100000; i++) {
      ASSERT(PerfxHashTable_Contains(hashtable, INT_TO_POINTER(i)));
   }
   ASSERT(!PerfxHashTable_Contains(hashtable, INT_TO_POINTER(100001)));
   PerfxHashTable_Free(hashtable);

   return 0;
}
