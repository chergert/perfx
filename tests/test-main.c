#include <unistd.h>

#include "perfx-debug.h"
#include "perfx-main.h"

static int      gPipeFds[2];
static int      gReadCount;
static uint32_t gPollHandler;


static void
OnCanReadPipe(int fd,                   /* IN */
              PerfxMainPollFlags flags, /* IN */
              void *data)               /* IN/UNUSED */
{
   char buf[1];

   gReadCount++;

   ASSERT(flags & PERFX_MAIN_POLL_IN);
   ASSERT_INT(read(fd, buf, sizeof buf), ==, 1);
   ASSERT_INT(write(gPipeFds[1], buf, sizeof buf), ==, 1);

   if (gReadCount == 100) {
      PerfxMain_Remove(gPollHandler);
      PerfxMain_Quit();
   }
}


static void
GetItStarted(void *data) /* IN/UNUSED */
{
   static uint32_t call_count = 0;
   char buf[1] = { 0 };

   ASSERT_INT(call_count, ==, 0);
   call_count++;

   ASSERT_INT(pipe(gPipeFds), ==, 0);
   gPollHandler = PerfxMain_AddPoll(gPipeFds[0],
                                    PERFX_MAIN_POLL_IN | PERFX_MAIN_POLL_HUP,
                                    OnCanReadPipe,
                                    NULL);
   ASSERT_INT(write(gPipeFds[1], buf, sizeof buf), ==, sizeof buf);
}


int
main (int argc,     /* IN */
      char *argv[]) /* IN */
{
   uint32_t handlers[4096];
   uint32_t i;

   PerfxMain_Create();

   /*
    * Start by testing and removing 1000 Idle callbacks. These are the
    * same as a timeout callback, except it is a bit cleaner since we
    * don't have to always return FALSE.
    */
   for (i = 0; i < 4096; i++) {
      handlers[i] = PerfxMain_AddIdle(GetItStarted, NULL);
   }
   for (i = 0; i < 4096; i++) {
      PerfxMain_Remove(handlers[i]);
   }

   /*
    * This test is adding some poll and timeout callbacks from within
    * the main loop. The poll fds will have writes to them so that they
    * can trigger a certain number of events. All of which, we will
    * assert at the end.
    */

   PerfxMain_AddIdle(GetItStarted, NULL);
   PerfxMain_Run();

   /*
    * Our read ready callback should have been called exactly 100 times.
    */
   ASSERT_INT(gReadCount, ==, 100);

   /*
    * The read handler should not have been removed from the hash table
    * yet because no futher work would be done on the main loop.
    * Additionally, there is the internal pipe() so two handlers are left.
    */
   ASSERT_INT(PerfxMain_CountHandlers(), ==, 2);

   /*
    * Lets try to flush it out.
    */
   PerfxMain_Iteration();
   ASSERT_INT(PerfxMain_CountHandlers(), ==, 1);

   PerfxMain_Destroy();

   return 0;
}
