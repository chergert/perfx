#include "perfx-debug.h"
#include "perfx-list.h"
#include "perfx-types.h"


int
main (int argc,     /* IN */
      char *argv[]) /* IN */
{
   PerfxList *list = NULL;

   list = PerfxList_Append(list, INT_TO_POINTER(1));
   list = PerfxList_Append(list, INT_TO_POINTER(2));
   list = PerfxList_Append(list, INT_TO_POINTER(3));
   list = PerfxList_Append(list, INT_TO_POINTER(4));
   list = PerfxList_Prepend(list, INT_TO_POINTER(0));

   ASSERT_INT(PerfxList_Length(list), ==, 5);

   PerfxList_Free(list);

   return 0;
}
