#include "perfx-debug.h"
#include "perfx-main.h"
#include "perfx-resolver.h"

static void
GetHostNameCb(PerfxResolver *resolver, /* IN */
              const char *hostname,    /* IN */
              struct in_addr *addr,    /* IN */
              void *data)
{
   bool *success = data;
   ASSERT(success);
   *success = TRUE;
   PerfxMain_Quit();
}

int
main(int argc,
     char *argv[])
{
   PerfxResolver *resolver;
   bool success = FALSE;

   PerfxMain_Create();

   resolver = PerfxResolver_Create();
   PerfxResolver_GetHostByName(resolver, "audidude.com",
                               GetHostNameCb, &success);
   PerfxMain_Run();
   ASSERT_INT(success, ==, TRUE);
   PerfxObject_Unref(resolver);

   PerfxMain_Destroy();

   return 0;
}
