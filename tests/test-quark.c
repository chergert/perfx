#include "perfx-debug.h"
#include "perfx-quark.h"

int
main(int argc,     /* IN */
     char *argv[]) /* IN */
{
   PerfxQuark q;

   PerfxQuark_Init();

   ASSERT_INT(0, ==, PerfxQuark_TryFromString("test-string"));
   q = PerfxQuark_FromStaticString("test-string");
   ASSERT_INT(q, !=, 0);
   ASSERT_INT(q, ==, PerfxQuark_TryFromString("test-string"));

   ASSERT_INT(0, ==, PerfxQuark_TryFromString("test2"));
   q = PerfxQuark_FromString("test2");
   ASSERT_INT(q, !=, 0);
   ASSERT_INT(q, ==, PerfxQuark_TryFromString("test2"));

   PerfxQuark_Destroy();

   return 0;
}
