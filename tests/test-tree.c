#include <stdlib.h>
#include <string.h>

#include "perfx-debug.h"
#include "perfx-tree.h"


static void
insert(PerfxTree *tree,
       const char *key,
       const char *value)
{
   PerfxTree_Insert(tree, strdup(key), strdup(value));
}


int
main (int argc,     /* IN */
      char *argv[]) /* IN */
{
   PerfxTree tree;

   PerfxTree_Init(&tree, (PerfxCompareFunc)strcmp, free, free);

   insert(&tree, "hello", "world");
   insert(&tree, "hello", "world");
   insert(&tree, "hello", "world");
   ASSERT_INT(tree.size, ==, 1);

   ASSERT_STR_EQUAL("world", (char*)PerfxTree_Lookup(&tree, "hello"));

   PerfxTree_Remove(&tree, "hello");
   ASSERT(!PerfxTree_Lookup(&tree, "hello"));
   insert(&tree, "hello", "world");
   ASSERT_INT(tree.size, ==, 1);
   insert(&tree, "move", "over");
   ASSERT_INT(tree.size, ==, 2);
   ASSERT_STR_EQUAL("world", (char*)PerfxTree_Lookup(&tree, "hello"));
   ASSERT_STR_EQUAL("over", (char*)PerfxTree_Lookup(&tree, "move"));

   PerfxTree_Destroy(&tree);

   return 0;
}
