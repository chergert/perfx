TESTS =
TESTS += test-array
TESTS += test-bson
TESTS += test-connector
TESTS += test-hashtable
TESTS += test-list
TESTS += test-listener
TESTS += test-main
TESTS += test-message
TESTS += test-path
TESTS += test-perf-tree-path
TESTS += test-port
TESTS += test-protocol
TESTS += test-quark
TESTS += test-resolver
TESTS += test-tree


noinst_PROGRAMS =
noinst_PROGRAMS += $(TESTS)


RUN_TESTS = $(foreach prog,$(TESTS),run-$(prog))


run-%: %
	$(builddir)/$*


tests: $(RUN_TESTS)


test_array_SOURCES = tests/test-array.c
test_array_LDADD = libshared.la
test_array_CPPFLAGS = -I$(top_srcdir)/shared


test_bson_SOURCES = tests/test-bson.c
test_bson_LDADD = libshared.la
test_bson_CPPFLAGS = -I$(top_srcdir)/shared


test_connector_SOURCES = tests/test-connector.c
test_connector_LDADD = libshared.la
test_connector_CPPFLAGS = -I$(top_srcdir)/shared


test_list_SOURCES = tests/test-list.c
test_list_LDADD = libshared.la
test_list_CPPFLAGS = -I$(top_srcdir)/shared


test_listener_SOURCES = tests/test-listener.c
test_listener_LDADD = libshared.la
test_listener_CPPFLAGS = -I$(top_srcdir)/shared


test_main_SOURCES = tests/test-main.c
test_main_LDADD = libshared.la
test_main_CPPFLAGS = -I$(top_srcdir)/shared


test_message_SOURCES = tests/test-message.c
test_message_LDADD = libshared.la
test_message_CPPFLAGS = -I$(top_srcdir)/shared


test_hashtable_SOURCES = tests/test-hashtable.c
test_hashtable_LDADD = libshared.la
test_hashtable_CPPFLAGS = -I$(top_srcdir)/shared


test_path_SOURCES = tests/test-path.c
test_path_LDADD = libshared.la
test_path_CPPFLAGS = -I$(top_srcdir)/shared


test_perf_tree_path_SOURCES = tests/test-perf-tree-path.c
test_perf_tree_path_LDADD = libshared.la
test_perf_tree_path_CPPFLAGS = -I$(top_srcdir)/shared


test_port_SOURCES = tests/test-port.c
test_port_LDADD = libshared.la
test_port_CPPFLAGS = -I$(top_srcdir)/shared


test_protocol_SOURCES = tests/test-protocol.c
test_protocol_LDADD = libshared.la
test_protocol_CPPFLAGS = -I$(top_srcdir)/shared


test_quark_SOURCES = tests/test-quark.c
test_quark_LDADD = libshared.la
test_quark_CPPFLAGS = -I$(top_srcdir)/shared


test_resolver_SOURCES = tests/test-resolver.c
test_resolver_LDADD = libshared.la
test_resolver_CPPFLAGS = -I$(top_srcdir)/shared


test_tree_SOURCES = tests/test-tree.c
test_tree_LDADD = libshared.la
test_tree_CPPFLAGS = -I$(top_srcdir)/shared
