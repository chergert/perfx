#include "perfx-connector.h"
#include "perfx-debug.h"
#include "perfx-main.h"


static void
test1_cb(PerfxConnector *connector, /* IN */
         bool success,              /* IN */
         void *data)                /* IN */
{
   ASSERT(success);
   *(bool *)data = success;
   PerfxMain_Quit();
}

static void
test1(void)
{
   PerfxConnector *c;
   bool state = FALSE;
   int read_fd;
   int write_fd;

   PerfxMain_Create();

   c = PerfxConnector_CreatePipe();
   ASSERT(c);

   PerfxConnector_SetCallback(c, test1_cb, &state);

   ASSERT_INT(0, ==, PerfxConnector_GetFds(c, &read_fd, &write_fd));

   PerfxConnector_Connect(c);

   PerfxMain_Run();

   ASSERT_INT(state, ==, TRUE);

   PerfxObject_Unref(c);
   PerfxMain_Destroy();
}

#if 0
/*
 * TODO: Reenable when we have a listener we can test with.
 */
static void
test2(void)
{
   PerfxConnector *c;
   bool state = FALSE;
   int read_fd;
   int write_fd;

   PerfxMain_Create();

   c = PerfxConnector_CreateUnix("/tmp/test.socket");
   ASSERT(c);

   PerfxConnector_SetCallback(c, test1_cb, &state);

   ASSERT_INT(0, ==, PerfxConnector_GetFds(c, &read_fd, &write_fd));

   PerfxConnector_Connect(c);

   PerfxMain_Run();

   ASSERT_INT(state, ==, TRUE);

   PerfxObject_Unref(c);
   PerfxMain_Destroy();
}
#endif

static void
test3(void)
{
   PerfxConnector *c;
   bool state = FALSE;
   int read_fd;
   int write_fd;

   PerfxMain_Create();

   c = PerfxConnector_CreateTcp("google.com", 80);
   ASSERT(c);

   PerfxConnector_SetCallback(c, test1_cb, &state);

   ASSERT_INT(0, ==, PerfxConnector_GetFds(c, &read_fd, &write_fd));

   PerfxConnector_Connect(c);

   PerfxMain_Run();

   ASSERT_INT(1, ==, PerfxConnector_GetFds(c, &read_fd, &write_fd));
   ASSERT_INT(read_fd, !=, -1);
   ASSERT_INT(write_fd, !=, -1);
   ASSERT_INT(state, ==, TRUE);

   PerfxObject_Unref(c);
   PerfxMain_Destroy();
}

int
main (int argc,     /* IN */
      char *argv[]) /* IN */
{
   test1();
#if 0
   test2();
#endif
   test3();

   return 0;
}
