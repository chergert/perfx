#include "perfx-debug.h"
#include "perfx-message.h"

#include <stdio.h>

typedef struct {
   PerfxMessage message;
   bool success;
} Msg;

static void
MarkSuccess (void *data) /* OUT */
{
   Msg *msg = data;
   msg->success = TRUE;
}

int
main(int argc,     /* IN */
     char *argv[]) /* IN */
{
   PerfxMessage *m;
   Msg *msg;

   m = PerfxMessage_CreateEmpty();
   PerfxMessage_Free(m);

   msg = PerfxMessage_Create(sizeof *msg, NULL);
   PerfxMessage_Free(msg);

   msg = PerfxMessage_Create(sizeof *msg, MarkSuccess);
   ASSERT(msg->message.data_destroy == MarkSuccess);
   msg->success = FALSE;
   PerfxMessage_Free(msg);
   ASSERT_INT(msg->success, ==, TRUE); /* unsafe */

   return 0;
}
