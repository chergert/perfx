#include "perfx-atomic.h"
#include "perfx-debug.h"
#include "perfx-log.h"
#include "perfx-main.h"
#include "perfx-port.h"


static uint32_t   gCount;
static PerfxPort *gPort;


static void
PortHandler2(PerfxPort *port,
             PerfxMessage *message,
             void *data)
{
   ASSERT(port);
   ASSERT(message);

   PerfxAtomicInt_Increment(&gCount);
}


static void
PortHandler(PerfxPort *port,
            PerfxMessage *message,
            void *data)
{
   PerfxMessage *message2;
   uint32_t i;

   ASSERT(port);
   ASSERT(message);

   PerfxAtomicInt_Increment(&gCount);

   for (i = 0; i < 1000; i++) {
      message2 = PerfxMessage_CreateEmpty();
      PerfxPort_Post(gPort, message2);
   }
}


int
main (int argc,
      char *argv[])
{
   PerfxMessage *message;
   PerfxPort *port;
   uint32_t i;

   PerfxMain_Create();

   port = PerfxPort_Create(0, PortHandler, NULL);
   gPort = PerfxPort_Create(0, PortHandler2, NULL);

   for (i = 0; i < 1000; i++) {
      message = PerfxMessage_CreateEmpty();
      PerfxPort_Post(port, message);
   }

   PerfxPort_Free(port);
   PerfxPort_Free(gPort);

   ASSERT_INT(gCount, ==, 1001000);

   return 0;
}
