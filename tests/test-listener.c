#include "perfx-connection.h"
#include "perfx-debug.h"
#include "perfx-line-protocol.h"
#include "perfx-listener.h"
#include "perfx-log.h"
#include "perfx-main.h"


static void
ConnectionMade(PerfxConnection *connection, /* IN */
               void *data)                  /* IN */
{
   bool *success = data;

   ASSERT(success);

   *success = TRUE;

   if (PerfxConnection_IsConnected(connection)) {
      PerfxConnection_Disconnect(connection);
   }

   PerfxMain_Quit();
}


int
main(int argc,     /* IN */
     char *argv[]) /* IN */
{
   PerfxListener *listener;
   PerfxConnection *connection;
   bool success = FALSE;

   PerfxMain_Create();

   listener = PerfxListener_CreateTcp(NULL, 26543);
   PerfxListener_SetProtocolType(listener, PERFX_TYPE_LINE_PROTOCOL);
   PerfxListener_Start(listener);

   connection = PerfxConnection_CreateTcp("0.0.0.0", 26543);
   PerfxConnection_SetProtocolType(connection, PERFX_TYPE_LINE_PROTOCOL);
   PerfxConnection_SetStateCallback(connection, ConnectionMade, &success);
   PerfxConnection_Connect(connection);

   PerfxMain_Run();
   PerfxListener_Stop(listener);
   PerfxObject_Unref(listener);
   PerfxMain_Destroy();

   ASSERT_INT(success, ==, TRUE);

   return 0;
}
