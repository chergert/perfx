/* perfx-array.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_ARRAY_H
#define PERFX_ARRAY_H


#include "perfx-macros.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


#define PERFX_ARRAY_INITIALIZER(type) { 0, NULL, 0, sizeof(type), FALSE }
#define PerfxArray_Append(array, val) PerfxArray_AppendRange(array, 1, &(val))
#define PerfxArray_Index(array, type, index) (((type *)(array)->data)[index])


typedef struct
{
   uint32_t len;
   void *data;

   /*< private >*/
   uint32_t allocated_len;
   uint32_t element_size;
   bool zeroed;
} PerfxArray;


void        PerfxArray_AppendRange (PerfxArray *array,
                                    uint32_t count,
                                    const void *range);
void        PerfxArray_Clear       (PerfxArray *array);
PerfxArray *PerfxArray_Create      (bool zeroed,
                                    uint32_t element_size);
PerfxArray *PerfxArray_CreateSized (bool zeroed,
                                    uint32_t element_size,
                                    uint32_t length);
void        PerfxArray_Destroy     (PerfxArray *array);
void        PerfxArray_Free        (PerfxArray *array);
void        PerfxArray_Remove      (PerfxArray *array,
                                    uint32_t index);
void        PerfxArray_RemoveFast  (PerfxArray *array,
                                    uint32_t index);
int32_t     PerfxArray_Search      (PerfxArray *array,
                                    const void *target,
                                    PerfxCompareFunc compare);
int32_t     PerfxArray_SearchFull  (PerfxArray *array,
                                    const void *target,
                                    PerfxCompareFunc compare,
                                    int32_t *lower_bound,
                                    int32_t *upper_bound);
void       *PerfxArray_StealData   (PerfxArray *array,
                                    size_t *len);
void        PerfxArray_Sort        (PerfxArray *array,
                                    PerfxCompareFunc compare);


PERFX_END_DECLS

#endif /* PERFX_ARRAY_H */
