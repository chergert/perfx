/* perfx-bson.h
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_BSON_H
#define PERFX_BSON_H


#include <time.h>

#include "perfx-macros.h"
#include "perfx-memory.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


typedef struct _PerfxBson     PerfxBson;
typedef struct _PerfxBsonId   PerfxBsonId;
typedef struct _PerfxBsonIter PerfxBsonIter;
typedef enum   _PerfxBsonType PerfxBsonType;


enum _PerfxBsonType
{
   PERFX_BSON_DOUBLE    = 0x01,
   PERFX_BSON_UTF8      = 0x02,
   PERFX_BSON_DOCUMENT  = 0x03,
   PERFX_BSON_ARRAY     = 0x04,

   PERFX_BSON_UNDEFINED = 0x06,
   PERFX_BSON_OBJECT_ID = 0x07,
   PERFX_BSON_BOOLEAN   = 0x08,
   PERFX_BSON_DATE_TIME = 0x09,
   PERFX_BSON_NULL      = 0x0A,
   PERFX_BSON_REGEX     = 0x0B,

   PERFX_BSON_INT32     = 0x10,

   PERFX_BSON_INT64     = 0x12,
};

struct _PerfxBsonIter
{
   /*< private >*/
   void *user_data1; /* Raw data buffer */
   void *user_data2; /* Raw buffer length */
   void *user_data3; /* Offset */
   void *user_data4; /* Key */
   void *user_data5; /* Type */
   void *user_data6; /* Value1 */
   void *user_data7; /* Value2 */
   void *user_data8; /* Ignore UTF-8 validation */
};


PerfxBson     *PerfxBson_Create            (void);
PerfxBson     *PerfxBson_CreateFromData    (const uint8_t *buffer,
                                            size_t length);
PerfxBson     *PerfxBson_CreateFromStatic  (uint8_t *buffer,
                                            size_t length,
                                            PerfxFreeFunc destroy);
void           PerfxBson_Free              (PerfxBson *bson);
const uint8_t *PerfxBson_GetData           (PerfxBson *bson,
                                            size_t *length);

void           PerfxBson_AppendArray       (PerfxBson *bson,
                                            const char *key,
                                            PerfxBson *value);
void           PerfxBson_AppendBoolean     (PerfxBson *bson,
                                            const char *key,
                                            bool value);
void           PerfxBson_AppendDocument    (PerfxBson *bson,
                                            const char *key,
                                            PerfxBson *value);
void           PerfxBson_AppendDouble      (PerfxBson *bson,
                                            const char *key,
                                            double value);
void           PerfxBson_AppendId          (PerfxBson *bson,
                                            const char *key,
                                            PerfxBsonId *value);
void           PerfxBson_AppendInt         (PerfxBson *bson,
                                            const char *key,
                                            int32_t value);
void           PerfxBson_AppendInt64       (PerfxBson *bson,
                                            const char *key,
                                            int64_t value);
void           PerfxBson_AppendNull        (PerfxBson *bson,
                                            const char *key);
void           PerfxBson_AppendRegex       (PerfxBson *bson,
                                            const char *key,
                                            const char *regex,
                                            const char *option);
void           PerfxBson_AppendString      (PerfxBson *bson,
                                            const char *key,
                                            const char *value);
void           PerfxBson_AppendTimeval     (PerfxBson *bson,
                                            const char *key,
                                            struct timeval *value);
void           PerfxBson_AppendUndefined   (PerfxBson *bson,
                                            const char *key);
bool           PerfxBson_GetEmpty          (PerfxBson *bson);
char          *PerfxBson_ToString          (PerfxBson *bson,
                                            bool is_array);

PerfxBsonId   *PerfxBsonId_Create          (const uint8_t id_bytes[12]);
char          *PerfxBsonId_ToString        (const PerfxBsonId *id);
void           PerfxBsonId_Free            (PerfxBsonId *id);

void           PerfxBsonIter_Init          (PerfxBsonIter *iter,
                                            PerfxBson *bson);
bool           PerfxBsonIter_Find          (PerfxBsonIter *iter,
                                            const char *key);
bool           PerfxBsonIter_Next          (PerfxBsonIter *iter);
const char    *PerfxBsonIter_GetKey        (PerfxBsonIter *iter);
PerfxBson     *PerfxBsonIter_GetArray      (PerfxBsonIter *iter);
bool           PerfxBsonIter_GetBoolean    (PerfxBsonIter *iter);
void           PerfxBsonIter_GetTimeval    (PerfxBsonIter *iter,
                                            struct timeval *value);
PerfxBson     *PerfxBsonIter_GetDocument   (PerfxBsonIter *iter);
double         PerfxBsonIter_GetDouble     (PerfxBsonIter *iter);
PerfxBsonId   *PerfxBsonIter_GetId         (PerfxBsonIter *iter);
int32_t        PerfxBsonIter_GetInt        (PerfxBsonIter *iter);
int64_t        PerfxBsonIter_GetInt64      (PerfxBsonIter *iter);
void           PerfxBsonIter_GetRegex      (PerfxBsonIter *iter,
                                            char **regex,
                                            char **option);
const char    *PerfxBsonIter_GetString     (PerfxBsonIter *iter,
                                            size_t *length);
PerfxBsonType  PerfxBsonIter_GetBsonType   (PerfxBsonIter *iter);
bool           PerfxBsonIter_Recurse       (PerfxBsonIter *iter,
                                            PerfxBsonIter *child);


PERFX_END_DECLS


#endif /* PERFX_BSON_H */
