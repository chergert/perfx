/* perfx-log.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_LOG_H
#define PERFX_LOG_H


#include "perfx-macros.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


#ifndef LOG_DOMAIN
#define LOG_DOMAIN "perfx"
#endif


#define LOG(msg, ...)   PerfxLog_Log(LOG_DOMAIN, msg, ## __VA_ARGS__)
#define PANIC(msg, ...) PerfxLog_Panic(LOG_DOMAIN, __FILE__, __LINE__, msg, ## __VA_ARGS__)
#define TODO(msg, ...)  PerfxLog_Log(LOG_DOMAIN, "TODO(%s:%d): "msg, __FILE__, __LINE__, ## __VA_ARGS__)


void PerfxLog_AddFile      (const char *filename);
void PerfxLog_Log          (const char *domain,
                            const char *format,
                            ...) __attribute__((format(printf, 2, 3)));
void PerfxLog_Panic        (const char *domain,
                            const char *filename,
                            uint32_t lineno,
                            const char *format,
                            ...) __attribute__((noreturn format(printf, 4, 5)));
void PerfxLog_SetUseStdout (bool use_stdout);


PERFX_END_DECLS


#endif /* PERFX_LOG_H */
