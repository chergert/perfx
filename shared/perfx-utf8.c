/* perfx-utf8.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#if defined(__linux__)
#include <unistr.h>
#elif defined(__APPLE__)
#include <CoreFoundation/CFString.h>
#endif

#include "perfx-debug.h"
#include "perfx-memory.h"
#include "perfx-utf8.h"


/*
 *--------------------------------------------------------------------------
 *
 * perfx_utf8_validate --
 *
 *       Validates the content of a UTF-8 string.
 *
 * Returns:
 *       TRUE if str is valid UTF-8, otherwise FALSE and @end is set to the
 *       first byte that is invalid.
 *
 * Side effects:
 *       end is set to the first invalid byte, or NULL if successful.
 *
 *--------------------------------------------------------------------------
 */

bool
perfx_utf8_validate(const char *str,  /* IN */
                    ssize_t max_len,  /* IN */
                    const char **end) /* IN */
{
#if defined(__linux__)
   const uint8_t *real_end;

   ASSERT(str);

   real_end = u8_check((uint8_t *)str, (max_len < 0) ? strlen(str) : max_len);
   if (end)
      *end = (const char *)real_end;
   return !real_end;
#elif defined(__APPLE__)
   CFStringRef ptr;

   ASSERT(str);

   /*
    * XXX: This is totally janky. It fails on the use case where we want
    *      to validate a portion of a buffer as utf8 without a NUL byte
    *      at the end.
    */

   if (max_len < 0) {
      max_len = strlen(str);
   }

   if (str[max_len] != '\0') {
      return FALSE;
   }

   ptr = CFStringCreateWithCString(NULL, str, kCFStringEncodingUTF8);
   if (ptr) {
      CFRelease(ptr);
   }

   return !!ptr;
#else
#error platform not supported.
#endif
}
