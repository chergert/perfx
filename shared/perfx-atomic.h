/* perfx-atomic.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_ATOMIC_H
#define PERFX_ATOMIC_H


#include "perfx-macros.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


#define PerfxAtomicInt_Increment(p)        (__sync_fetch_and_add(p, 1))
#define PerfxAtomicInt_Decrement(p)        (__sync_fetch_and_sub(p, 1))
#define PerfxAtomicInt_DecrementAndTest(p) (__sync_sub_and_fetch(p, 1) == 0)


static inline int32_t
PerfxAtomicInt_Get(int32_t *ptr) /* IN */
{
   __sync_synchronize();
   return (int32_t)*ptr;
}


static inline void
PerfxAtomicInt_Set(int32_t *ptr,    /* IN */
                   int32_t new_int) /* IN */
{
   *ptr = new_int;
   __sync_synchronize();
}


static inline void *
PerfxAtomicPtr_Get(void **ptr) /* IN */
{
   __sync_synchronize();
   return (void *)*ptr;
}


static inline void
PerfxAtomicPtr_Set(void **ptr,    /* OUT */
                   void *new_ptr) /* IN */
{
   *ptr = new_ptr;
   __sync_synchronize();
}


PERFX_END_DECLS


#endif /* PERFX_ATOMIC_H */
