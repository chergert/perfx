/* perfx-value.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "perfx-debug.h"
#include "perfx-memory.h"
#include "perfx-value.h"


/*
 *--------------------------------------------------------------------------
 *
 * PerfxValue_Init --
 *
 *       Initialize the PerfxValue to store the given type.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxValue_Init(PerfxValue *value,   /* OUT */
                PerfxValueType type) /* IN */
{
   ASSERT(value);
   ASSERT(value->type == 0);

   value->type = type;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxValue_Unset --
 *
 *       Unsets the value and releases any resources.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxValue_Unset(PerfxValue *value) /* OUT */
{
   ASSERT(value);

   if (value->type == PERFX_TYPE_STRING) {
      free(value->data.v_str);
   }
   memset(value, 0, sizeof *value);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxValue_SetInt --
 *
 *       Sets value to the integer provided.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       value is modified.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxValue_SetInt(PerfxValue *value, /* OUT */
                  int32_t v_int)     /* IN */
{
   ASSERT(value);
   ASSERT(value->type == PERFX_VALUE_INT);

   value->data.v_int = v_int;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxValue_SetUInt --
 *
 *       Sets value to the unsigned integer provided.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       value is modified.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxValue_SetUInt(PerfxValue *value, /* OUT */
                   uint32_t v_uint)   /* IN */
{
   ASSERT(value);
   ASSERT(value->type == PERFX_VALUE_UINT);

   value->data.v_uint = v_uint;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxValue_SetBool --
 *
 *       Sets the value to the boolean value.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       value is set to the boolean value.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxValue_SetBool(PerfxValue *value, /* OUT */
                   bool v_bool)       /* IN */
{
   ASSERT(value);
   ASSERT(value->type == PERFX_VALUE_BOOL);

   value->data.v_bool = v_bool;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxValue_SetString --
 *
 *       Sets the string for the value.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       value is set to a copy of the string.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxValue_SetString(PerfxValue *value, /* OUT */
                     const char *v_str) /* IN */
{
   ASSERT(value);
   ASSERT(value->type == PERFX_VALUE_STRING);

   free(value->data.v_str);
   value->data.v_str = strdup(v_str);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxValue_SetDouble --
 *
 *       Sets the double for the value. Requires the value was initialized
 *       with PERFX_VALUE_DOUBLE.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxValue_SetDouble(PerfxValue *value, /* IN */
                     double v_dbl)      /* IN */
{
   ASSERT(value);
   ASSERT(value->type == PERFX_VALUE_DOUBLE);

   value->data.v_dbl = v_dbl;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxValue_TakeString --
 *
 *       Sets the string for the value. The function takes ownership of
 *       the string.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       value is modified with teh string.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxValue_TakeString(PerfxValue *value, /* OUT */
                      char *v_str)       /* IN */
{
   ASSERT(value);
   ASSERT(value->type == PERFX_VALUE_STRING);

   free(value->data.v_str);
   value->data.v_str = v_str;
}


char *
PerfxValue_ToString(PerfxValue *value) /* IN */
{
   char *ret = NULL;

   ASSERT(value);

   switch (value->type) {
   case PERFX_VALUE_STRING:
      ret = strdup(value->data.v_str);
      break;
   case PERFX_VALUE_BOOL:
      asprintf(&ret, "%s", value->data.v_bool ? "TRUE" : "FALSE");
      break;
   case PERFX_VALUE_UINT:
      asprintf(&ret, "%u", value->data.v_uint);
      break;
   case PERFX_VALUE_INT:
      asprintf(&ret, "%d", value->data.v_int);
      break;
   case PERFX_VALUE_DOUBLE:
      asprintf(&ret, "%f", value->data.v_dbl);
      break;
   default:
      break;
   }

   return ret;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxValue_Copy --
 *
 *       Copy the contents of the boxed value from @src to @dst.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxValue_Copy(const PerfxValue *src, /* IN */
                PerfxValue *dst)       /* OUT */
{
   ASSERT(src);
   ASSERT(dst);

   memcpy(dst, src, sizeof *dst);
   if (dst->type == PERFX_VALUE_STRING) {
      dst->data.v_str = strdup(dst->data.v_str);
   }
}
