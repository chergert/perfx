/* perfx-main.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PERFX_MAIN_H
#define PERFX_MAIN_H


#include "perfx-macros.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


typedef enum {
   PERFX_MAIN_POLL_IN  = 1 << 0,
   PERFX_MAIN_POLL_OUT = 1 << 1,
   PERFX_MAIN_POLL_HUP = 1 << 2,
} PerfxMainPollFlags;


typedef void (*PerfxMainPollFunc)    (int fd,
                                      PerfxMainPollFlags flags,
                                      void *data);
typedef bool (*PerfxMainTimeoutFunc) (void *data);
typedef void (*PerfxMainIdleFunc)    (void *data);


void     PerfxMain_ClearHandler      (uint32_t *handler_location);
uint32_t PerfxMain_CountHandlers     (void);
void     PerfxMain_Create            (void);
uint32_t PerfxMain_AddTimeoutFull    (uint32_t interval_msec,
                                      PerfxMainTimeoutFunc callback,
                                      void *data,
                                      PerfxFreeFunc destroy);
uint32_t PerfxMain_AddTimeout        (uint32_t interval_msec,
                                      PerfxMainTimeoutFunc callback,
                                      void *data);
uint32_t PerfxMain_AddTimeoutSeconds (uint32_t seconds,
                                      PerfxMainTimeoutFunc callback,
                                      void *data);
uint32_t PerfxMain_AddIdle           (PerfxMainIdleFunc callback,
                                      void *data);
uint32_t PerfxMain_AddPoll           (int fd,
                                      PerfxMainPollFlags flags,
                                      PerfxMainPollFunc callback,
                                      void *data);
void     PerfxMain_Iteration         (void);
void     PerfxMain_Remove            (uint32_t handler_id);
void     PerfxMain_Run               (void);
void     PerfxMain_Quit              (void);
void     PerfxMain_Destroy           (void);
void     PerfxMain_Wakeup            (void);
bool     PerfxMain_IsMainThread      (void);


PERFX_END_DECLS


#endif /* PERFX_MAIN_H */
