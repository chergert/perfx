/* perfx-error.c
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>

#include "perfx-error.h"


/*
 *--------------------------------------------------------------------------
 *
 * PerfxError_CreateV --
 *
 *       Create a new instance of an error.
 *
 * Returns:
 *       A newly allocated PerfxError that should be freed with
 *       PerfxError_Free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxError *
PerfxError_CreateV(uint32_t domain,    /* IN */
                   uint32_t code,      /* IN */
                   const char *format, /* IN */
                   va_list args)       /* IN */
{
   PerfxError *err;

   err = perfx_new(PerfxError, 1);
   err->domain = domain;
   err->code = code;
   if (-1 == vasprintf(&err->message, format, args)) {
      err->message = strdup("invalid error format in PerfxError_CreateV().");
   }

   return err;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxError_Create --
 *
 *       Create a new instance of PerfxError. Free with PerfxError_Free()
 *       or PerfxError_Clear(&error);
 *
 * Returns:
 *       A newly allocated PerfxError.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxError *
PerfxError_Create(uint32_t domain,    /* IN */
                  uint32_t code,      /* IN */
                  const char *format, /* IN */
                  ...)
{
   PerfxError *ret;
   va_list args;

   va_start(args, format);
   ret = PerfxError_CreateV(domain, code, format, args);
   va_end(args);

   return ret;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxError_Set --
 *
 *       If @error is non-NULL, then create a new PerfxError and set
 *       it to the location pointed to by @error.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxError_Set(PerfxError **error, /* OUT */
               uint32_t domain,    /* IN */
               uint32_t code,      /* IN */
               const char *format, /* IN */
               ...)
{
   va_list args;

   if (error) {
      va_start(args, format);
      *error = PerfxError_CreateV(domain, code, format, args);
      va_end(args);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxError_Free --
 *
 *       Free the @error provided.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxError_Free(PerfxError *error) /* IN */
{
   perfx_free(error->message);
   perfx_free(error);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxError_Clear --
 *
 *       If the error pointed to by @error is non-NULL, free it and
 *       set the location to NULL.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       *error is freed and set to NULL.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxError_Clear(PerfxError **error) /* OUT */
{
   if (*error) {
      PerfxError_Free(*error);
      *error = NULL;
   }
}
