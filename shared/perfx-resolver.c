/* perfx-resolver.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <netdb.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

#include "perfx-debug.h"
#include "perfx-main.h"
#include "perfx-memory.h"
#include "perfx-resolver.h"


typedef struct {
   PerfxResolver *resolver;
   char *hostname;
   PerfxResolverCallback callback;
   void *data;
   bool success;
   struct in_addr addr;
} GetHostByNameOp;


static void
PerfxResolver_GetHostByNameCb(void *data) /* IN */
{
   GetHostByNameOp *op = data;
   struct in_addr *addr = NULL;

   ASSERT(op);
   ASSERT(op->callback);

   if (op->success) {
      addr = &op->addr;
   }

   op->callback(op->resolver, op->hostname, addr, op->data);

   free(op->hostname);
   PerfxObject_Unref(op->resolver);
   perfx_free(op);
}


static void *
PerfxResolver_GetHostByNameThread(void *data) /* IN */
{
   GetHostByNameOp *op = data;
   struct hostent *entry;

   ENTRY;

   ASSERT(op);
   ASSERT(op->hostname);

   if ((entry = gethostbyname(op->hostname))) {
      ASSERT(entry->h_addr);
      op->addr = *(struct in_addr *)entry->h_addr;
      op->success = TRUE;
   }

   PerfxMain_AddIdle(PerfxResolver_GetHostByNameCb, op);

   RETURN(NULL);
}


void
PerfxResolver_GetHostByName(PerfxResolver *resolver,        /* IN */
                            const char *hostname,           /* IN */
                            PerfxResolverCallback callback, /* IN */
                            void *data)                     /* IN */
{
   pthread_t thread = 0;
   GetHostByNameOp *op;
   int ret;

   ENTRY;

   ASSERT(resolver);
   ASSERT(hostname);
   ASSERT(callback);

   op = perfx_new0(GetHostByNameOp, 1);
   op->resolver = PerfxObject_Ref(resolver);
   op->hostname = strdup(hostname);
   op->callback = callback;
   op->data = data;

   ret = pthread_create(&thread, NULL, PerfxResolver_GetHostByNameThread, op);
   if (ret != 0) {
      PerfxMain_AddIdle(PerfxResolver_GetHostByNameCb, op);
   }

   EXIT;
}


static void
PerfxResolver_Destroy(PerfxObject *object) /* IN */
{
   ENTRY;
   EXIT;
}


PerfxType
PerfxResolver_GetType(void)
{
   PerfxClass *class;
   static PerfxType type = 0;

   if (!type) {
      type = PerfxClass_Register("PerfxResolver", 0,
                                 sizeof(PerfxResolverClass),
                                 sizeof(PerfxResolver),
                                 TRUE);
      class = PerfxType_GetClass(type);
      class->Destroy = PerfxResolver_Destroy;
   }

   return type;
}


PerfxResolver *
PerfxResolver_Create(void)
{
   return PerfxObject_Create(PERFX_TYPE_RESOLVER);
}
