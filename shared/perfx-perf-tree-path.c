/* perfx-perf-tree-path.c
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include <string.h>

#include "perfx-debug.h"
#include "perfx-memory.h"
#include "perfx-perf-tree-path.h"


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPerfTreePath_Compare --
 *
 *       Compare tree paths by comparing each of their path sections.
 *
 * Returns:
 *       a qsort() style integer return value.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

int
PerfxPerfTreePath_Compare(PerfxPerfTreePath *left,  /* IN */
                          PerfxPerfTreePath *right) /* IN */
{
   size_t data_len;
   int ret;

   ASSERT(left);
   ASSERT(right);

   data_len = MIN(left->data_len, right->data_len);
   if (!(ret = memcmp(left->data, right->data, data_len))) {
      return left->data_len - right->data_len;
   }

   return ret;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPerfTreePath_GetPart --
 *
 *       Fetches a certain part of the tree structure.
 *
 * Returns:
 *       A NUL-terminated string which should not be modified or freed.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

const char *
PerfxPerfTreePath_GetPart(PerfxPerfTreePath *path, /* IN */
                          uint8_t idx)             /* IN */
{
   ASSERT(path);

   if (idx < path->len) {
      return (char *)&path->data[path->len + path->data[idx]];
   }

   return NULL;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPerfTreePath_Create --
 *
 *       Create a new PerfxPerfTreePath structure by parsing the @pathstr
 *       provided.
 *
 * Returns:
 *       A newly allocated PerfxPerfTreePath structure.
 *       Free with PerfxPerfTreePath_Free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxPerfTreePath *
PerfxPerfTreePath_Create(const char *pathstr) /* IN */
{
   PerfxPerfTreePath *path;
   uint16_t i;
   uint8_t n_parts = 0;
   uint8_t next = 0;
   uint8_t j = 0;
   uint8_t len;

   ASSERT(pathstr);

   if (pathstr[0] != '/') {
      return NULL;
   }

   /*
    * Determine the amount of memory we need for our dynamically sized
    * structure. This needs to contain the header information and the
    * path string layed out as well as indexes into the string for the
    * given NUL-terminated strings.
    */
   for (i = 0; pathstr[i]; i++) {
      if (i > PERFX_PERF_TREE_PATH_MAX) {
         return NULL;
      }
      if (pathstr[i] == '/' || pathstr[i] == '\0') {
         n_parts++;
      }
   }

   /*
    * Make sure we found a valid path.
    */
   if (!n_parts || (i < 2)) {
      return NULL;
   }

   /*
    * Skip past our first / and adjust length for it. Since we subtract
    * the first / and need to add a NUL-terminator, it all comes out
    * equal so we dont (+ 1 - 1) i to derive len;
    */
   pathstr++;
   len = i;

   /*
    * Create our path structure and copy in our path string.
    */
   path = perfx_malloc((sizeof *path) + n_parts + len);
   path->len = n_parts;
   path->data_len = n_parts + len;
   memcpy(&path->data[n_parts], pathstr, len);

   /*
    * Fill in the indexes and NUL-terminate each of the parts in path.
    */
   for (i = 0; i < len; i++) {
      if (path->data[n_parts + i] == '/' || path->data[n_parts + i] == '\0') {
         path->data[n_parts + i] = '\0';
         path->data[j++] = next;
         next = i + 1;
      }
   }

   DUMP_BYTES("path", (uint8_t*)path, (sizeof *path + len + n_parts));

   return path;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPerfTreePath_Free --
 *
 *       Free the @path structure.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxPerfTreePath_Free(PerfxPerfTreePath *path) /* IN */
{
   perfx_free(path);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPerfTreePath_ToString --
 *
 *       Build a string out of a PerfxPerfTreePath.
 *
 * Returns:
 *       A newly allocated string that should be freed with perfx_free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

char *
PerfxPerfTreePath_ToString(PerfxPerfTreePath *path) /* IN */
{
   uint32_t i;
   char *str;

   ASSERT(path);

   str = perfx_malloc(path->data_len - path->len + 1);
   str[0] = '/';
   memcpy(str + 1, path->data + path->len, path->data_len - path->len);
   for (i = 1; i < path->len; i++) {
      str[path->data[i]] = '/';
   }

   return str;
}
