/* perfx-log.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#define _GNU_SOURCE

#include <ctype.h>
#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#ifdef __linux__
#include <sys/utsname.h>
#include <sys/types.h>
#include <sys/syscall.h>
#endif

#define LOG_DOMAIN "log"

#include "perfx-array.h"
#include "perfx-clock.h"
#include "perfx-debug.h"
#include "perfx-log.h"


static PerfxArray      gLogFds = PERFX_ARRAY_INITIALIZER(int);
static pthread_mutex_t gMutex = PTHREAD_MUTEX_INITIALIZER;
static char            gHostname[64];


static void
PerfxLog_InitOnce(void)
{
#if defined(__linux__)
   struct utsname u;
   uname(&u);
   memcpy(gHostname, u.nodename, sizeof gHostname);
#elif defined(__APPLE__)
   gethostname(gHostname, sizeof gHostname);
#else
#error "You're platform is not supported."
#endif
}


static inline int
PerfxLog_GetThread(void)
{
#if defined(__linux__)
   return syscall(SYS_gettid);
#else
   return getpid();
#endif
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxLog_Log --
 *
 *       A generic logging function.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxLog_Log(const char *domain, /* IN */
             const char *format, /* IN */
             ...)
{
   static pthread_once_t once = PTHREAD_ONCE_INIT;
   struct timespec ts = { 0 };
   struct tm tt;
   uint32_t i;
   va_list args;
   time_t t;
   char *formatted;
   char *message;
   char ftime[32];
   int fd;

   ASSERT(domain);
   ASSERT(format);

   pthread_once(&once, PerfxLog_InitOnce);

   PerfxClock_Now(&ts);
   t = ts.tv_sec;
   localtime_r(&t, &tt);
   strftime(ftime, sizeof ftime - 1, "%Y/%m/%d %H:%M:%S", &tt);

   va_start(args, format);
   if (-1 == vasprintf(&formatted, format, args)) {
      fprintf(stderr, "Format \"%s\" is invalid.\n", format);
      fflush(stderr);
      abort();
   }

   va_end(args);

   if (-1 == asprintf(&message,
                      "%s.%04ld  %s: %12s[%d]: %s\n",
                      ftime,
                      ts.tv_nsec / 100000,
                      gHostname,
                      domain,
                      PerfxLog_GetThread(),
                      formatted)) {
      abort();
   }

   pthread_mutex_lock(&gMutex);

   for (i = 0; i < gLogFds.len; i++) {
      fd = PerfxArray_Index(&gLogFds, int, i);
      write(fd, message, strlen(message));
   }

   pthread_mutex_unlock(&gMutex);

   free(formatted);
   free(message);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxLog_Panic --
 *
 *       Log a message and abort the process.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxLog_Panic(const char *domain, /* IN */
               const char *file,   /* IN */
               uint32_t lineno,    /* IN */
               const char *format, /* IN */
               ...)
{
   uint32_t i;
   va_list args;
   char *message;
   char *full_message;
   int n_bytes;
   int fd;

   if (!format) {
      abort();
   }

   va_start(args, format);
   if (vasprintf(&message, format, args) == -1) {
      message = strdup("");
   }
   va_end(args);

   n_bytes = asprintf(&full_message, "%s at %s:%u\n", message, file, lineno);
   if (n_bytes > 0) {
      write(STDERR_FILENO, full_message, n_bytes);
      pthread_mutex_lock(&gMutex);
      for (i = 0; i < gLogFds.len; i++) {
         fd = PerfxArray_Index(&gLogFds, int, i);
         write(fd, full_message, n_bytes);
      }
      pthread_mutex_unlock(&gMutex);
   }

   free(full_message);
   free(message);

   abort();
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxLog_SetUseStdout --
 *
 *       Enables or disables the use of stdout for logging.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       Logging is enabled or disabled for STDOUT_FILENO.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxLog_SetUseStdout(bool use_stdout) /* IN */
{
   int fd = STDOUT_FILENO;
   uint32_t i;

   pthread_mutex_lock(&gMutex);

   if (use_stdout) {
      PerfxArray_Append(&gLogFds, fd);
   } else {
      for (i = 0; i < gLogFds.len; i++) {
         if (PerfxArray_Index(&gLogFds, uint32_t, i) == fd) {
            PerfxArray_RemoveFast(&gLogFds, i);
            break;
         }
      }
   }

   pthread_mutex_unlock(&gMutex);

   LOG("stdout %s.", use_stdout ? "enabled" : "disabled");
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxDebug_DumpBytes --
 *
 *       Dump the bytes provided to the log file.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxDebug_DumpBytes(const char *domain, /* IN */
                     const char *name,   /* IN */
                     uint8_t *bytes,     /* IN */
                     uint32_t len)       /* IN */
{
   PerfxString *astr;
   PerfxString *str;
   uint32_t i;

   ASSERT(name);
   ASSERT(len);
   ASSERT(bytes);

   PerfxLog_Log(domain, "%s = %p [%u]", name, bytes, len);

   astr = PerfxString_Create(NULL);
   str = PerfxString_Create(NULL);

   for (i = 0; i < len; i++) {
      if (!(i % 16)) {
         PerfxString_AppendPrintf(str, "%06x: ", i);
      }
      PerfxString_AppendPrintf(str, " %02x", bytes[i]);
      if (isprint(bytes[i])) {
         PerfxString_AppendPrintf(astr, " %c", bytes[i]);
      } else {
         PerfxString_Append(astr, " .");
      }
      if ((i % 16) == 15) {
         PerfxLog_Log(domain, "%s  %s", str->str, astr->str);
         PerfxString_Truncate(str, 0);
         PerfxString_Truncate(astr, 0);
      } else if ((i % 16) == 7) {
         PerfxString_Append(astr, " ");
         PerfxString_Append(str, " ");
      }
   }
   if (i != 16) {
      PerfxLog_Log(domain, "%-57s  %s", str->str, astr->str);
   }
   PerfxString_Free(str, TRUE);
   PerfxString_Free(astr, TRUE);
}
