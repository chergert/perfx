/* perfx-types.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <errno.h>
#include <limits.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

#include "perfx-atomic.h"
#include "perfx-debug.h"
#include "perfx-hashtable.h"
#include "perfx-memory.h"
#include "perfx-types.h"


#define FUNDAMENTAL_OFFSET 64


/*
 *--------------------------------------------------------------------------
 *
 * TODO:
 *
 *       - Make this as thread-safe as we need to. Not all classes need
 *         to have ref counting that is thread safe. But accessing the
 *         type type information probably should be (that way not all
 *         classes need to be registered up front, from the main thread.)
 *
 *         However, locking the type system after startup might actually
 *         be kind of nice.
 *
 *       - Free the class data on shutdown.
 *
 *--------------------------------------------------------------------------
 */


static uint32_t        gClassesSeq;
static PerfxHashTable *gClasses;


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClass_InitClasses --
 *
 *       Initialize the required class infrastructure.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       gClasses is initialized.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxClass_InitClasses(void)
{
   /*
    * TODO: Free the class data.
    */
   gClasses = PerfxHashTable_Create(1024,
                                    perfx_direct_hash,
                                    perfx_direct_equal,
                                    NULL,
                                    NULL);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClass_GetClasses --
 *
 *       Retrieve the classes hash table, initializing if necessary.
 *
 * Returns:
 *       A PerfxHashTable.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static PerfxHashTable *
PerfxClass_GetClasses(void)
{
   static pthread_once_t gClassesOnce = PTHREAD_ONCE_INIT;
   pthread_once(&gClassesOnce, PerfxClass_InitClasses);
   return gClasses;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxObject_Destroy --
 *
 *       Handle destruction of the object.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       Objects resources are released. Structure is not yet freed.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxObject_Destroy(PerfxObject *object) /* IN */
{
   ASSERT(object);

   if (object->class->Destroy) {
      object->class->Destroy(object);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxObject_Free --
 *
 *       Handles freeing the perfx object itself. This is usually called
 *       after object destruction has occurred.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       Object is freed.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxObject_Free(PerfxObject *object) /* IN */
{
   ASSERT(object);
   perfx_free(object);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxObject_Ref --
 *
 *       Increments the reference count of object by one.
 *
 * Returns:
 *       The object provided with its reference count incremented.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void *
PerfxObject_Ref(void *object) /* IN */
{
   PerfxObject *pobj = object;

   ASSERT(pobj);
   ASSERT(pobj->class);
   ASSERT(pobj->class->type);
   ASSERT_INT(pobj->ref_count, >, 0);

   if (pobj->class->thread_safe) {
      PerfxAtomicInt_Increment(&pobj->ref_count);
   } else {
      pobj->ref_count++;
   }

   return pobj;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxObject_Unref --
 *
 *       Decrement the reference count of the object by one. If the
 *       reference count reaches zero, free the object.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       Object may be freed.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxObject_Unref(void *object) /* IN */
{
   PerfxObject *pobj = object;

   ASSERT(pobj);
   ASSERT(pobj->class);
   ASSERT(pobj->class->type);
   ASSERT_INT(pobj->ref_count, >, 0);

   if (pobj->class->thread_safe) {
      if (PerfxAtomicInt_DecrementAndTest(&pobj->ref_count)) {
         goto destroy;
      }
   } else {
      if (!(--pobj->ref_count)) {
         goto destroy;
      }
   }

   return;

destroy:
   PerfxObject_Destroy(object);
   PerfxObject_Free(object);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxType_GetName --
 *
 *       Return the name of a type.
 *
 * Returns:
 *       A string that should not be modified; or NULL.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

const char *
PerfxType_GetName(PerfxType type) /* IN */
{
   PerfxHashTable *classes;
   PerfxClass *class;

   ASSERT(type);

   classes = PerfxClass_GetClasses();
   if ((class = PerfxHashTable_Lookup(classes, INT_TO_POINTER(type)))) {
      return class->name;
   }
   return NULL;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxType_GetClass --
 *
 *       Retrieves the class structure for a given type.
 *
 * Returns:
 *       A class structure if successful; otherwise NULL.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxClass *
PerfxType_GetClass(PerfxType type) /* IN */
{
   PerfxHashTable *classes;

   ASSERT(type);

   classes = PerfxClass_GetClasses();
   return PerfxHashTable_Lookup(classes, INT_TO_POINTER(type));
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClass_Register --
 *
 *       Register a new PerfxObject type.
 *
 * Returns:
 *       The objects type identifier.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxType
PerfxClass_Register(const char *name,  /* IN */
                    PerfxType parent,  /* IN */
                    size_t class_size, /* IN */
                    size_t inst_size,  /* IN */
                    bool thread_safe)  /* IN */
{
   PerfxHashTable *classes;
   PerfxClass *parent_class = NULL;
   PerfxClass *class;

   ASSERT(name);
   ASSERT_INT(strlen(name), <, 64);
   ASSERT_INT(class_size, >=, sizeof(PerfxClass));
   ASSERT_INT(inst_size, >=, sizeof(PerfxObject));

   classes = PerfxClass_GetClasses();

   class = perfx_malloc0(class_size);

   if (parent) {
      parent_class = PerfxHashTable_Lookup(classes, INT_TO_POINTER(parent));
      ASSERT(parent_class);
      ASSERT_INT(class_size, >=, parent_class->class_size);
      ASSERT_INT(inst_size, >=, parent_class->inst_size);
      memcpy(class, parent_class, parent_class->class_size);
   }

   class->type = ++gClassesSeq + FUNDAMENTAL_OFFSET;
   class->parent_type = parent;
   class->parent_class = parent_class;
   strncpy(class->name, name, sizeof class->name - 1);
   class->class_size = class_size;
   class->inst_size = inst_size;
   class->thread_safe = thread_safe;

   PerfxHashTable_Insert(classes, INT_TO_POINTER(class->type), class);

   return class->type;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxObject_Cast --
 *
 *       Checks that a cast to a given type is valid for a PerfxObject.
 *
 * Returns:
 *       The instance if it is valid; otherwise NULL.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void *
PerfxObject_Cast(void *object,   /* IN */
                 PerfxType type) /* IN */
{
   PerfxObject *pobj = object;
   PerfxClass *class;

   ASSERT(pobj);
   ASSERT(pobj->class);

   for (class = pobj->class; class;
        class = PerfxType_GetClass(class->parent_type)) {
      if (class->type == type) {
         return object;
      }
   }

   LOG("Cast from \"%s\" to \"%s\" is invalid.",
       PerfxType_GetName(pobj->class->type),
       PerfxType_GetName(type));

   return NULL;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxObject_Create --
 *
 *       Create a new instance of the object type.
 *
 * Returns:
 *       A newly allocated PerfxObject that should be freed with
 *       PerfxObject_Unref().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void *
PerfxObject_Create(PerfxType type) /* IN */
{
   PerfxHashTable *classes;
   PerfxObject *object;
   PerfxClass *class;

   ASSERT(type);

   classes = PerfxClass_GetClasses();
   if (!(class = PerfxHashTable_Lookup(classes, INT_TO_POINTER(type)))) {
      PANIC("Could not find type %u", type);
   }

   object = perfx_malloc0(class->inst_size);
   object->class = class;
   object->ref_count = 1;

   if (class->Init) {
      class->Init(object);
   }

   return object;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxType_IsA --
 *
 *       Check to see if a type is an instance of other or any subclass
 *       of other.
 *
 * Returns:
 *       TRUE if type is, or is a subclass of, other.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxType_IsA(PerfxType type,  /* IN */
              PerfxType other) /* IN */
{
   PerfxClass *class;

   ASSERT(type);
   ASSERT_INT(type, >, FUNDAMENTAL_OFFSET);
   ASSERT(other);
   ASSERT_INT(other, >, FUNDAMENTAL_OFFSET);

   if ((class = PerfxType_GetClass(type))) {
      do {
         if (class->type == other) {
            return TRUE;
         }
      } while ((class = class->parent_class));
   }

   return FALSE;
}


/*
 *--------------------------------------------------------------------------
 *
 * perfx_strcmp0 --
 *
 *       Compares two strings like strcmp(). However, it handles NULL
 *       gracefully by converting it to an empty string.
 *
 * Returns:
 *       -1, 0, or 1 in qsort() style.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

int32_t
perfx_strcmp0(const char *a, /* IN */
              const char *b) /* IN */
{
   a = a ? a : "";
   b = b ? b : "";
   return strcmp(a, b);
}


/*
 *--------------------------------------------------------------------------
 *
 * perfx_int_parse --
 *
 *       Helper function to parse a 32-bit integer and handle the
 *       overflow and underflow cases.
 *
 *       If @str isn't fully parsed, FALSE is returned.
 *
 * Returns:
 *       TRUE if successful; otherwise FALSE.
 *
 * Side effects:
 *       @inptr is set if successful.
 *
 *--------------------------------------------------------------------------
 */

bool
perfx_int_parse(int32_t *inptr,  /* OUT */
                const char *str, /* IN */
                uint32_t base)   /* IN */
{
   long int val;
   char *endptr = NULL;

   ASSERT(inptr);
   ASSERT(str);
   ASSERT(base);

   errno = 0;
   val = strtol(str, &endptr, base);
   if (((val == LONG_MIN) || (val == LONG_MAX)) && errno == ERANGE) {
      return FALSE;
   }
   *inptr = val;
   return endptr && (*endptr == '\0');
}
