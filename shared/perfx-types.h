/* perfx-types.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_TYPES_H
#define PERFX_TYPES_H


#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <sys/types.h>

#include "perfx-macros.h"


PERFX_BEGIN_DECLS


#ifndef FALSE
#define FALSE false
#endif


#ifndef TRUE
#define TRUE true
#endif


#define PERFX_TYPE_INT8        1
#define PERFX_TYPE_UINT8       2
#define PERFX_TYPE_INT16       3
#define PERFX_TYPE_UINT16      4
#define PERFX_TYPE_INT32       5
#define PERFX_TYPE_UINT32      6
#define PERFX_TYPE_INT64       7
#define PERFX_TYPE_UINT64      8
#define PERFX_TYPE_BOOL        9
#define PERFX_TYPE_STRING     10
#define PERFX_TYPE_FLOAT      11
#define PERFX_TYPE_DOUBLE     12


typedef uint32_t (*PerfxHashFunc)    (const void *data);
typedef bool     (*PerfxEqualFunc)   (const void *data1,
                                      const void *data2);
typedef void     (*PerfxFreeFunc)    (void *data);
typedef int      (*PerfxCompareFunc) (const void *data1,
                                      const void *data2);


typedef uint32_t PerfxType;
typedef struct _PerfxClass PerfxClass;
typedef struct _PerfxObject PerfxObject;


uint32_t perfx_str_hash  (const void *data);
bool     perfx_str_equal (const void *data1,
                          const void *data2);
int32_t  perfx_strcmp0   (const char *a,
                          const char *b);
#define  perfx_strdup(s) ((s) ? strdup((s)) : NULL)


uint32_t perfx_int_hash  (const void *data);
bool     perfx_int_equal (const void *data1,
                          const void *data2);

uint32_t perfx_direct_hash  (const void *data);
bool     perfx_direct_equal (const void *data1,
                             const void *data2);


bool perfx_int_parse (int32_t *val,
                      const char *str,
                      uint32_t base);


#ifndef PERFX_DISABLE_CAST_CHECKS
#define PERFX_OBJECT_CAST(o, T, t) ((T *)PerfxObject_Cast(o, t))
#else
#define PERFX_OBJECT_CAST(o, T, t) ((T *)o)
#endif


#define PERFX_OBJECT_GET_CLASS(o)          ((PerfxClass *)((PerfxObject *)o)->class)
#define PERFX_TYPE_FROM_INSTANCE(o)        (((PerfxObject *)o)->class->type)
#define PERFX_PARENT_TYPE_FROM_INSTANCE(o) (((PerfxObject *)o)->class->parent_type)
#define PERFX_OBJECT_SUPER(o, T, f, args)                           \
   do {                                                             \
      if (((T *)(((PerfxObject *)o)->class->parent_class))->f) {    \
         ((T *)(((PerfxObject *)o)->class->parent_class))->f args ; \
      }                                                             \
   } while (0)


struct _PerfxClass {
   PerfxType type;
   PerfxType parent_type;
   PerfxClass *parent_class;
   char name[64];
   bool thread_safe;
   uint16_t class_size;
   uint16_t inst_size;

   void (*Init)    (PerfxObject *object);
   void (*Destroy) (PerfxObject *object);
};


struct _PerfxObject {
   PerfxClass *class;
   int32_t ref_count;
};


void *PerfxObject_Create(PerfxType type);
void *PerfxObject_Ref   (void *object);
void  PerfxObject_Unref (void *object);
void *PerfxObject_Cast  (void *object,
                         PerfxType type);


PerfxType PerfxClass_Register(const char *name,
                              PerfxType parent,
                              size_t class_size,
                              size_t inst_size,
                              bool thread_safe);


const char *PerfxType_GetName (PerfxType type);
PerfxClass *PerfxType_GetClass(PerfxType type);
bool        PerfxType_IsA     (PerfxType type,
                               PerfxType other);


PERFX_END_DECLS


#endif /* PERFX_TYPES_H */
