/* perfx-message.h
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <string.h>

#include "perfx-debug.h"
#include "perfx-memory.h"
#include "perfx-message.h"


PerfxMessage *
PerfxMessage_CreateEmpty(void)
{
   return PerfxMessage_Create(sizeof(PerfxMessage), NULL);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMessage_Create --
 *
 *       Creates a new instance of PerfxMessage.
 *
 *       @struct_len should be the number of bytes to allocate for the
 *       entire message structure. It must be >= sizeof(PerfxMessage).
 *
 *       @data_destroy should be a callback that can be executed to
 *       free any contents associated with the data stored. It does
 *       not need to free the data pointer itself, as that allocation
 *       is managed by the message itself.
 *
 *       message->data is not set to zero, so the caller is repsonsible
 *       to do so if necessary.
 *
 * Returns:
 *       The newly created message, which may be freed with
 *       PerfxMessage_Free(). Sending a message transfers ownership of
 *       the message to the PerfxPort.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void *
PerfxMessage_Create(size_t struct_len,          /* IN */
                    PerfxFreeFunc data_destroy) /* IN */
{
   PerfxMessage *message;

   ASSERT_INT(struct_len, >=, sizeof *message);

   message = perfx_malloc(struct_len);
   message->next = NULL;
   message->port = NULL;
   message->data_destroy = data_destroy;

   return message;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMessage_Free --
 *
 *       Free the contents of a message.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxMessage_Free(void *data) /* IN */
{
   PerfxMessage *message = data;

   if (message) {
      if (message->data_destroy) {
         message->data_destroy(message);
      }
      perfx_free(message);
   }
}
