/* perfx-buffer.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <string.h>

#include "perfx-atomic.h"
#include "perfx-buffer.h"
#include "perfx-debug.h"
#include "perfx-memory.h"


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBuffer_Create --
 *
 *       Create a new buffer using the data provided. It will not be copied,
 *       the data should be considered owned by the buffer after calling
 *       this function.
 *
 * Returns:
 *       A newly allocated PerfxBuffer that should be freed with
 *       PerfxBuffer_Unref().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxBuffer *
PerfxBuffer_Create(uint8_t *data,         /* IN */
                   size_t len,            /* IN */
                   PerfxFreeFunc destroy) /* IN */
{
   PerfxBuffer *buffer;

   buffer = perfx_new0(PerfxBuffer, 1);
   buffer->ref_count = 1;
   buffer->data = data;
   buffer->len = len;
   buffer->destroy = destroy;

   return buffer;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBuffer_CreateCopied --
 *
 *       Create a new PerfxBuffer by copying the provided data.
 *
 * Returns:
 *       A new PerfxBuffer.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxBuffer *
PerfxBuffer_CreateCopied(const uint8_t *data, /* IN */
                         size_t len)          /* IN */
{
   uint8_t *copied;

   ASSERT(data || !len);

   if (!data) {
      return PerfxBuffer_Create(NULL, 0, NULL);
   }

   copied = perfx_malloc(len);
   memcpy(copied, data, len);
   return PerfxBuffer_Create(copied, len, perfx_free);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBuffer_Free --
 *
 *       Free the buffer and associated memory.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       buffer is freed.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxBuffer_Free(PerfxBuffer *buffer) /* IN */
{
   if (buffer) {
      if (buffer->destroy) {
         buffer->destroy(buffer->data);
      }
      perfx_free(buffer);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBuffer_Ref --
 *
 *       Increment the reference count of the buffer by one.
 *
 * Returns:
 *       The buffer.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxBuffer *
PerfxBuffer_Ref(PerfxBuffer *buffer) /* IN */
{
   ASSERT(buffer);
   ASSERT_INT(buffer->ref_count, >, 0);

   PerfxAtomicInt_Increment(&buffer->ref_count);
   return buffer;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBuffer_Unref --
 *
 *       Decrements the reference count of the buffer by one. If the
 *       reference count reaches zero, the structure will be freed.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxBuffer_Unref(PerfxBuffer *buffer) /* IN */
{
   ASSERT(buffer);
   ASSERT_INT(buffer->ref_count, >, 0);

   if (PerfxAtomicInt_DecrementAndTest(&buffer->ref_count)) {
      PerfxBuffer_Free(buffer);
   }
}
