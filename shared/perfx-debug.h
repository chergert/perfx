/* perfx-debug.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_DEBUG_H
#define PERFX_DEBUG_H


#include <assert.h>

#include "perfx-log.h"
#include "perfx-macros.h"
#include "perfx-string.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


#ifndef ASSERT
#define ASSERT(a) assert(a)
#endif


#ifndef ASSERT_INT
#define ASSERT_INT(a,eq,b) if (!((a) eq (b))) { PANIC("(%lld " #eq " %lld)", (long long)a, (long long)b); }
#endif


#ifndef ASSERT_STR_EQUAL
#define ASSERT_STR_EQUAL(a,b) do { if (perfx_strcmp0(a,b)) { PANIC("(\"%s\" == \"%s\")", a, b); } } while (0)
#endif


#ifndef ASSERT_NOT_REACHED
#define ASSERT_NOT_REACHED() PANIC("Code should not be reached.")
#endif


#ifdef PERFX_TRACE
#define ENTRY     do { Perfx_Log("ENTRY: %s(): %s:%d", __FUNCTION__, __FILE__, __LINE__); } while (0)
#define EXIT      do { Perfx_Log(" EXIT: %s(): %s:%d", __FUNCTION__, __FILE__, __LINE__); return; } while (0)
#define RETURN(r) do { Perfx_Log(" EXIT: %s(): %s:%d", __FUNCTION__, __FILE__, __LINE__); return (r); } while (0)
#define GOTO(l)   do { Perfx_Log(" GOTO: %s(): %s:%d: %s", __FUNCTION__, __FILE__, __LINE__, #l); goto l; } while (0)
#define DUMP_BYTES(_n, _b, _l) PerfxDebug_DumpBytes(LOG_DOMAIN, _n, _b, _l)
#else
#define ENTRY
#define EXIT      return
#define RETURN(r) return (r)
#define GOTO(l)   goto l
#define DUMP_BYTES(_n, _b, _l)
#endif

void PerfxDebug_DumpBytes(const char *domain,
                          const char *name,
                          uint8_t *bytes,
                          uint32_t len);


PERFX_END_DECLS


#endif /* PERFX_DEBUG_H */
