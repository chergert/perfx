/* perfx-plugin.h
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_PLUGIN_H
#define PERFX_PLUGIN_H


#include "perfx-macros.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


typedef struct _PerfxPlugin PerfxPlugin;
typedef enum _PerfxPluginFlags PerfxPluginFlags;


enum _PerfxPluginFlags
{
   PERFX_PLUGIN_LAZY = 1 << 0,
   PERFX_PLUGIN_NOW  = 1 << 1,
};


PerfxPlugin *PerfxPlugin_Create          (const char *filename,
                                          PerfxPluginFlags flags);
const char  *PerfxPlugin_GetError        (PerfxPlugin *plugin);
const char  *PerfxPlugin_GetPath         (PerfxPlugin *plugin);
bool         PerfxPlugin_GetSymbol       (PerfxPlugin *plugin,
                                          const char *name,
                                          void **symbol);
void         PerfxPlugin_Free            (PerfxPlugin *plugin);
bool         PerfxPlugin_LooksLikePlugin (const char *filename);


PERFX_END_DECLS


#endif /* PERFX_PLUGIN_H */
