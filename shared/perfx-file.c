/* perfx-file.c
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "perfx-array.h"
#include "perfx-debug.h"
#include "perfx-file.h"
#include "perfx-quark.h"


char *
PerfxFile_GetContents(const char *filename, /* IN */
                      size_t *len,          /* OUT */
                      PerfxError **error)   /* OUT */
{
   PerfxArray array = PERFX_ARRAY_INITIALIZER(char);
   char buf[1024];
   char *ret = NULL;
   int fd;
   int rv;

   ASSERT(filename);
   ASSERT(len);

   *len = 0;

   if (-1 == (fd = open(filename, O_RDONLY))) {
      PerfxError_Set(error,
                     PerfxQuark_FromStaticString("file"),
                     1, "%s", strerror(errno));
      return NULL;
   }

again:
   rv = read(fd, buf, sizeof buf);
   switch (rv) {
   case -1:
      if ((errno == EAGAIN) || (errno == EINTR)) {
         goto again;
      }
      PerfxError_Set(error,
                     PerfxQuark_FromStaticString("file"),
                     1, "%s", strerror(errno));
      break;
   case 0:
      ret = PerfxArray_StealData(&array, len);
      break;
   default:
      PerfxArray_AppendRange(&array, rv, buf);
      goto again;
   }

   close(fd);
   PerfxArray_Destroy(&array);

   return ret;
}
