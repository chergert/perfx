/* perfx-path.h
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_PATH_H
#define PERFX_PATH_H


#include "perfx-macros.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


typedef enum
{
   PERFX_PATH_TEST_IS_REGULAR    = 1 << 0,
   PERFX_PATH_TEST_IS_DIR        = 1 << 1,
   PERFX_PATH_TEST_IS_EXECUTABLE = 1 << 2,
   PERFX_PATH_TEST_IS_SYMLINK    = 1 << 3,
} PerfxPathTestFlags;


char *PerfxPath_Build       (const char *first_part,
                             ...);
char *PerfxPath_GetDirectory(const char *filename);
bool  PerfxPath_HasPrefix   (const char *filename,
                             const char *prefix);
bool  PerfxPath_HasSuffix   (const char *filename,
                             const char *suffix);
bool  PerfxPath_IsDirectory (const char *filename);
bool  PerfxPath_IsRegular   (const char *filename);
bool  PerfxPath_IsExecutable(const char *filename);
bool  PerfxPath_IsSymlink   (const char *filename);
char *PerfxPath_ResolveLink (const char *filename);
bool  PerfxPath_Test        (const char *filename,
                             PerfxPathTestFlags flags);


PERFX_END_DECLS


#endif /* PERFX_PATH_H */
