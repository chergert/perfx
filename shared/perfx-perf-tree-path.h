/* perfx-perf-tree-path.h
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef PERFX_PERF_TREE_PATH_H
#define PERFX_PERF_TREE_PATH_H


#include "perfx-macros.h"
#include "perfx-types.h"


#define PERFX_PERF_TREE_PATH_MAX (127)


PERFX_BEGIN_DECLS


typedef struct _PerfxPerfTreePath PerfxPerfTreePath;


struct _PerfxPerfTreePath
{
   uint8_t len;
   uint8_t data_len;
   uint8_t data[0];
};


int                PerfxPerfTreePath_Compare  (PerfxPerfTreePath *left,
                                               PerfxPerfTreePath *right);
PerfxPerfTreePath *PerfxPerfTreePath_Create   (const char *pathstr);
void               PerfxPerfTreePath_Free     (PerfxPerfTreePath *path);
const char        *PerfxPerfTreePath_GetPart  (PerfxPerfTreePath *path,
                                               uint8_t idx);
char              *PerfxPerfTreePath_ToString (PerfxPerfTreePath *path);


PERFX_END_DECLS


#endif /* PERFX_PERF_TREE_PATH_H */
