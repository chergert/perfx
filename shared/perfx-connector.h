/* perfx-connector.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_CONNECTOR_H
#define PERFX_CONNECTOR_H


#include "perfx-types.h"


PERFX_BEGIN_DECLS


#define PERFX_TYPE_CONNECTOR (PerfxConnector_GetType())


typedef struct _PerfxConnector PerfxConnector;


typedef void (*PerfxConnectorCallback) (PerfxConnector *connector,
                                        bool success,
                                        void *data);


struct _PerfxConnector {
   PerfxObject parent;

   /*< private >*/
   int state;

   int read_fd;
   int write_fd;

   uint32_t fd_handler;

   PerfxConnectorCallback callback;
   void *callback_data;

   int type;
   char *errmsg;

   union {
      struct {
         char *hostname;
         uint16_t port;
      } tcp;
      struct {
         char *path;
      } unix_;
   } u;
};


typedef struct {
   PerfxClass parent_class;
} PerfxConnectorClass;


PerfxType       PerfxConnector_GetType     (void) PERFX_GNUC_CONST;
PerfxConnector *PerfxConnector_CreatePipe  (void);
PerfxConnector *PerfxConnector_CreateTcp   (const char *hostname,
                                            uint16_t port);
PerfxConnector *PerfxConnector_CreateUnix  (const char *path);
void            PerfxConnector_Connect     (PerfxConnector *connector);
bool            PerfxConnector_GetFds      (PerfxConnector *connector,
                                            int *read_fd,
                                            int *write_fd);
void            PerfxConnector_SetCallback (PerfxConnector *connector,
                                            PerfxConnectorCallback callback,
                                            void *data);


PERFX_END_DECLS


#endif /* PERFX_CONNECTOR_H */
