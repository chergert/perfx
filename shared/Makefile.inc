noinst_LTLIBRARIES =
noinst_LTLIBRARIES += libshared.la


libshared_la_SOURCES =
libshared_la_SOURCES += shared/perfx-array.c
libshared_la_SOURCES += shared/perfx-array.h
libshared_la_SOURCES += shared/perfx-async.c
libshared_la_SOURCES += shared/perfx-async.h
libshared_la_SOURCES += shared/perfx-atomic.h
libshared_la_SOURCES += shared/perfx-bson.c
libshared_la_SOURCES += shared/perfx-bson.h
libshared_la_SOURCES += shared/perfx-buffer.c
libshared_la_SOURCES += shared/perfx-buffer.h
libshared_la_SOURCES += shared/perfx-clock.c
libshared_la_SOURCES += shared/perfx-clock.h
libshared_la_SOURCES += shared/perfx-connector.c
libshared_la_SOURCES += shared/perfx-connector.h
libshared_la_SOURCES += shared/perfx-connection.c
libshared_la_SOURCES += shared/perfx-connection.h
libshared_la_SOURCES += shared/perfx-cpu.c
libshared_la_SOURCES += shared/perfx-cpu.h
libshared_la_SOURCES += shared/perfx-debug.h
libshared_la_SOURCES += shared/perfx-error.c
libshared_la_SOURCES += shared/perfx-error.h
libshared_la_SOURCES += shared/perfx-file.c
libshared_la_SOURCES += shared/perfx-file.h
libshared_la_SOURCES += shared/perfx-hashtable.c
libshared_la_SOURCES += shared/perfx-hashtable.h
libshared_la_SOURCES += shared/perfx-line-protocol.c
libshared_la_SOURCES += shared/perfx-line-protocol.h
libshared_la_SOURCES += shared/perfx-list.c
libshared_la_SOURCES += shared/perfx-list.h
libshared_la_SOURCES += shared/perfx-listener.c
libshared_la_SOURCES += shared/perfx-listener.h
libshared_la_SOURCES += shared/perfx-log.c
libshared_la_SOURCES += shared/perfx-log.h
libshared_la_SOURCES += shared/perfx-main.c
libshared_la_SOURCES += shared/perfx-main.h
libshared_la_SOURCES += shared/perfx-memory.c
libshared_la_SOURCES += shared/perfx-memory.h
libshared_la_SOURCES += shared/perfx-message.c
libshared_la_SOURCES += shared/perfx-message.h
libshared_la_SOURCES += shared/perfx-path.c
libshared_la_SOURCES += shared/perfx-path.h
libshared_la_SOURCES += shared/perfx-perf-tree-path.h
libshared_la_SOURCES += shared/perfx-perf-tree-path.c
libshared_la_SOURCES += shared/perfx-plugin.c
libshared_la_SOURCES += shared/perfx-plugin.h
libshared_la_SOURCES += shared/perfx-port.c
libshared_la_SOURCES += shared/perfx-port.h
libshared_la_SOURCES += shared/perfx-protocol.c
libshared_la_SOURCES += shared/perfx-protocol.h
libshared_la_SOURCES += shared/perfx-quark.c
libshared_la_SOURCES += shared/perfx-quark.h
libshared_la_SOURCES += shared/perfx-resolver.c
libshared_la_SOURCES += shared/perfx-resolver.h
libshared_la_SOURCES += shared/perfx-string.c
libshared_la_SOURCES += shared/perfx-string.h
libshared_la_SOURCES += shared/perfx-tree.c
libshared_la_SOURCES += shared/perfx-tree.h
libshared_la_SOURCES += shared/perfx-types.c
libshared_la_SOURCES += shared/perfx-types.h
libshared_la_SOURCES += shared/perfx-utf8.c
libshared_la_SOURCES += shared/perfx-utf8.h
libshared_la_SOURCES += shared/perfx-value.c
libshared_la_SOURCES += shared/perfx-value.h

libshared_la_LIBADD =
libshared_la_LIBADD += -lpthread
libshared_la_LIBADD += -ldl
if PLATFORM_LINUX
libshared_la_LIBADD += -lrt
libshared_la_LIBADD += -lunistring
endif

libshared_la_LDFLAGS =
if PLATFORM_DARWIN
libshared_la_LDFLAGS += -framework CoreFoundation
endif
