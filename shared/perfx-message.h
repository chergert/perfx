/* perfx-message.h
 *
 * Copyright (C) 2011-2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_MESSAGE_H
#define PERFX_MESSAGE_H


#include "perfx-bson.h"
#include "perfx-macros.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


#define PerfxMessage_WantsReply(m) (!!(m)->reply_port)


typedef struct _PerfxMessage PerfxMessage;
typedef struct _PerfxPort    PerfxPort;


struct _PerfxMessage
{
   PerfxMessage *next;         /* Pointer to next message in port queue. */
   PerfxPort *port;            /* Pointer to the delivery port. */
   PerfxFreeFunc data_destroy; /* Destroy callback for message data. */
   uint8_t data[0];            /* Dynamically sized data section. */
};


void         *PerfxMessage_Create      (size_t struct_len,
                                        PerfxFreeFunc data_destroy);
PerfxMessage *PerfxMessage_CreateEmpty (void);
void          PerfxMessage_Free        (void *data);


PERFX_END_DECLS


#endif /* PERFX_MESSAGE_H */
