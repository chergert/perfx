/* perfx-buffer.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_BUFFER_H
#define PERFX_BUFFER_H


#include "perfx-buffer.h"
#include "perfx-macros.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


typedef struct {
   volatile int32_t ref_count;
   uint8_t *data;
   size_t len;
   PerfxFreeFunc destroy;
} PerfxBuffer;


PerfxBuffer *PerfxBuffer_Create       (uint8_t *data,
                                       size_t len,
                                       PerfxFreeFunc destroy);
PerfxBuffer *PerfxBuffer_CreateCopied (const uint8_t *data,
                                       size_t len);
PerfxBuffer *PerfxBuffer_Ref          (PerfxBuffer *buffer);
void         PerfxBuffer_Unref        (PerfxBuffer *buffer);



PERFX_END_DECLS


#endif /* PERFX_BUFFER_H */
