/* perfx-protocol.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <unistd.h>

#include "perfx-debug.h"
#include "perfx-main.h"
#include "perfx-protocol.h"


void
PerfxProtocol_RecvData(PerfxProtocol *protocol, /* IN */
                       PerfxBuffer *buffer)     /* IN */
{
   PerfxProtocolClass *class;

   ENTRY;

   ASSERT(protocol);
   ASSERT(PERFX_PROTOCOL(protocol));
   ASSERT(buffer);

   class = PERFX_PROTOCOL_GET_CLASS(protocol);
   ASSERT(class);

   if (class->DataReceived) {
      class->DataReceived(protocol, buffer);
   }

   EXIT;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxProtocol_SendData --
 *
 *       Sends a buffer to the protocols destination.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxProtocol_SendData(PerfxProtocol *protocol, /* IN */
                       PerfxBuffer *buffer)     /* IN */
{
   ASSERT(protocol);
   ASSERT(protocol->connection);
   ASSERT(buffer);

   PerfxConnection_Send(protocol->connection, buffer);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxProtocol_ConnectionLost --
 *
 *       Calls the ConnectionLost handler if it is specified.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxProtocol_ConnectionLost(PerfxProtocol *protocol) /* IN */
{
   PerfxProtocolClass *class;
   uint32_t reason = 0;

   ENTRY;

   ASSERT(protocol);

   /*
    * TODO: determine reason.
    */

   class = PERFX_PROTOCOL_GET_CLASS(protocol);
   if (class->ConnectionLost) {
      class->ConnectionLost(protocol, reason);
   }

   protocol->connection = NULL;

   EXIT;
}


void
PerfxProtocol_ConnectionMade(PerfxProtocol *protocol,     /* IN */
                             PerfxConnection *connection) /* IN */
{
   PerfxProtocolClass *class;

   ENTRY;

   ASSERT(protocol);
   ASSERT(connection);
   ASSERT(protocol->connection == NULL);

   protocol->connection = connection;

   class = PERFX_PROTOCOL_GET_CLASS(protocol);
   if (class->ConnectionMade) {
      class->ConnectionMade(protocol, connection);
   }

   EXIT;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxProtocol_Destroy --
 *
 *       Handle destruction of the PerfxProtocol instance.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       Frees any lingering resources.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxProtocol_Destroy(PerfxObject *object) /* IN */
{
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxProtocol_Init --
 *
 *       Initialize the PerfxProtocol structure.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxProtocol_Init(PerfxObject *object) /* IN */
{
   ENTRY;
   EXIT;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxProtocol_GetType --
 *
 *       Retrieve the type id for the PerfxProtocol class.
 *
 * Returns:
 *       A PerfxType id.
 *
 * Side effects:
 *       Type may be registered.
 *
 *--------------------------------------------------------------------------
 */

PerfxType
PerfxProtocol_GetType(void)
{
   PerfxClass *class;
   static PerfxType type = 0;

   if (!type) {
      type = PerfxClass_Register("PerfxProtocol", 0,
                                 sizeof(PerfxProtocolClass),
                                 sizeof(PerfxProtocol),
                                 TRUE);
      class = PerfxType_GetClass(type);
      class->Init = PerfxProtocol_Init;
      class->Destroy = PerfxProtocol_Destroy;
   }

   return type;
}
