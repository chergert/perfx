/* perfx-tree.c
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


/*
 * Implementation based upon Mastering Algorithms in C, First edition.
 * By Kyle Loudon. Chapter 9: Trees.
 */


#include <string.h>

#include "perfx-debug.h"
#include "perfx-memory.h"
#include "perfx-tree.h"


#define LEFT_HEAVY   1
#define BALANCED     0
#define RIGHT_HEAVY -1


static void PerfxTree_RemoveLeft  (PerfxTree *tree,
                                   PerfxTreeNode *node);
static void PerfxTree_RemoveRight (PerfxTree *tree,
                                   PerfxTreeNode *node);


void
PerfxTree_Init(PerfxTree *tree,              /* IN */
               PerfxCompareFunc key_compare, /* IN */
               PerfxFreeFunc key_destroy,    /* IN */
               PerfxFreeFunc value_destroy)  /* IN */
{
   ASSERT(tree);
   ASSERT(key_compare);

   memset(tree, 0, sizeof *tree);

   tree->size = 0;
   tree->key_compare = key_compare;
   tree->key_destroy = key_destroy;
   tree->value_destroy = value_destroy;
   tree->root = NULL;
}


static bool
PerfxTree_InsertLeft(PerfxTree *tree,     /* IN */
                     PerfxTreeNode *node, /* IN */
                     void *key,           /* IN */
                     void *value)         /* IN */
{
   PerfxTreeNode *new_node;
   PerfxTreeNode **position;

   ASSERT(tree);

   if (node == NULL) {
      if (tree->size > 0) {
         ASSERT_NOT_REACHED();
         return FALSE;
      }
      position = &tree->root;
   } else {
      if (node->left != NULL) {
         ASSERT_NOT_REACHED();
         return FALSE;
      }
      position = &node->left;
   }

   new_node = perfx_new0(PerfxTreeNode, 1);
   new_node->key = key;
   new_node->value = value;
   new_node->left = NULL;
   new_node->right = NULL;
   new_node->hidden = FALSE;
   *position = new_node;

   tree->size++;

   return TRUE;
}


static bool
PerfxTree_InsertRight(PerfxTree *tree,     /* IN */
                      PerfxTreeNode *node, /* IN */
                      void *key,           /* IN */
                      void *value)         /* IN */
{
   PerfxTreeNode *new_node;
   PerfxTreeNode **position;

   ASSERT(tree);

   if (node == NULL) {
      if (tree->size > 0) {
         ASSERT_NOT_REACHED();
         return FALSE;
      }
      position = &tree->root;
   } else {
      if (node->right != NULL) {
         ASSERT_NOT_REACHED();
         return FALSE;
      }
      position = &node->right;
   }

   new_node = perfx_new0(PerfxTreeNode, 1);
   new_node->key = key;
   new_node->value = value;
   new_node->left = NULL;
   new_node->right = NULL;
   new_node->hidden = FALSE;
   *position = new_node;

   tree->size++;

   return TRUE;
}


static void
PerfxTree_DestroyNode(PerfxTree *tree,     /* IN */
                      PerfxTreeNode *node) /* IN */
{
   ASSERT(tree);
   ASSERT(node);

   if (tree->key_destroy) {
      tree->key_destroy(node->key);
   }

   if (tree->value_destroy) {
      tree->value_destroy(node->value);
   }
}


static void
PerfxTree_RemoveLeft(PerfxTree *tree,     /* IN */
                     PerfxTreeNode *node) /* IN */
{
   PerfxTreeNode **position;

   ASSERT(tree);

   if (!tree->size) {
      return;
   }

   position = (!node) ? &tree->root : &node->left;

   if (*position != NULL) {
      PerfxTree_RemoveLeft(tree, *position);
      PerfxTree_RemoveRight(tree, *position);
      PerfxTree_DestroyNode(tree, *position);
      perfx_free(*position);
      *position = NULL;
      tree->size--;
   }
}


static void
PerfxTree_RemoveRight(PerfxTree *tree,     /* IN */
                      PerfxTreeNode *node) /* IN */
{
   PerfxTreeNode **position;

   ASSERT(tree);

   if (!tree->size) {
      return;
   }

   position = (!node) ? &tree->root : &node->right;

   if (*position != NULL) {
      PerfxTree_RemoveLeft(tree, *position);
      PerfxTree_RemoveRight(tree, *position);
      PerfxTree_DestroyNode(tree, *position);
      perfx_free(*position);
      *position = NULL;
      tree->size--;
   }
}


void
PerfxTree_Destroy(PerfxTree *tree) /* IN */
{
   ASSERT(tree);

   PerfxTree_RemoveLeft(tree, NULL);
   memset(tree, 0, sizeof *tree);
}


static void
PerfxTreeNode_RotateLeft(PerfxTreeNode **node) /* IN/OUT */
{
   PerfxTreeNode *left;
   PerfxTreeNode *grandchild;

   ASSERT(node);
   ASSERT(*node);

   left = (*node)->left;

   if (left->factor == LEFT_HEAVY) {
      (*node)->left = left->right;
      left->right = *node;
      (*node)->factor = BALANCED;
      left->factor = BALANCED;
      *node = left;
   } else {
      grandchild = left->right;
      left->right = grandchild->left;
      grandchild->left = left;
      (*node)->left = grandchild->right;
      grandchild->right = *node;
      switch (grandchild->factor) {
      case LEFT_HEAVY:
         (*node)->factor = RIGHT_HEAVY;
         left->factor = BALANCED;
      case BALANCED:
         (*node)->factor = BALANCED;
         left->factor = BALANCED;
         break;
      case RIGHT_HEAVY:
         (*node)->factor = BALANCED;
         left->factor = LEFT_HEAVY;
         break;
      default:
         ASSERT_NOT_REACHED();
      }

      grandchild->factor = BALANCED;
      *node = grandchild;
   }
}


static void
PerfxTreeNode_RotateRight(PerfxTreeNode **node) /* IN/OUT */
{
   PerfxTreeNode *right;
   PerfxTreeNode *grandchild;

   ASSERT(node);
   ASSERT(*node);

   right = (*node)->right;

   if (right->factor == RIGHT_HEAVY) {
      (*node)->right = right->left;
      right->left = *node;
      (*node)->factor = BALANCED;
      right->factor = BALANCED;
      *node = right;
   } else {
      grandchild = right->left;
      right->left = grandchild->right;
      grandchild->right = right;
      (*node)->right = grandchild->left;
      grandchild->left = *node;
      switch (grandchild->factor) {
      case LEFT_HEAVY:
         (*node)->factor = BALANCED;
         right->factor = RIGHT_HEAVY;
         break;
      case BALANCED:
         (*node)->factor = BALANCED;
         right->factor = BALANCED;
         break;
      case RIGHT_HEAVY:
         (*node)->factor = LEFT_HEAVY;
         right->factor = BALANCED;
         break;
      default:
         ASSERT_NOT_REACHED();
      }

      grandchild->factor = BALANCED;
      *node = grandchild;
   }
}


static void
PerfxTree_DoInsert(PerfxTree *tree,      /* IN */
                   PerfxTreeNode **node, /* IN/OUT */
                   void *key,            /* IN */
                   void *value,          /* IN */
                   bool *balanced)       /* OUT */
{
   int cmpval;

   ASSERT(tree);
   ASSERT(node);
   ASSERT(balanced);

   if (!*node) {
      PerfxTree_InsertLeft(tree, NULL, key, value);
      return;
   } else {
      cmpval = tree->key_compare(key, (*node)->key);
      if (cmpval < 0) {
         if (!(*node)->left) {
            PerfxTree_InsertLeft(tree, *node, key, value);
            *balanced = FALSE;
            return;
         } else {
            PerfxTree_DoInsert(tree, &(*node)->left, key, value, balanced);
         }

         if (!*balanced) {
            switch ((*node)->factor) {
            case LEFT_HEAVY:
               PerfxTreeNode_RotateLeft(node);
               *balanced = TRUE;
               break;
            case BALANCED:
               (*node)->factor = LEFT_HEAVY;
               break;
            case RIGHT_HEAVY:
               (*node)->factor = BALANCED;
               *balanced = TRUE;
               break;
            default:
               ASSERT_NOT_REACHED();
               break;
            }
         }
      } else if (cmpval > 0) {
         if (!(*node)->right) {
            PerfxTree_InsertRight(tree, *node, key, value);
            *balanced = FALSE;
         } else {
            PerfxTree_DoInsert(tree, &(*node)->right, key, value, balanced);
         }

         if (!*balanced) {
            switch ((*node)->factor) {
            case LEFT_HEAVY:
               (*node)->factor = BALANCED;
               *balanced = TRUE;
               break;
            case BALANCED:
               (*node)->factor = RIGHT_HEAVY;
               break;
            case RIGHT_HEAVY:
               PerfxTreeNode_RotateRight(node);
               *balanced = TRUE;
               break;
            default:
               ASSERT_NOT_REACHED();
               break;
            }
         }
      } else {
         PerfxTree_DestroyNode(tree, *node);
         (*node)->key = key;
         (*node)->value = value;
         (*node)->hidden = FALSE;
         *balanced = TRUE;
      }
   }
}


void
PerfxTree_Insert(PerfxTree *tree, /* IN */
                 void *key,       /* IN */
                 void *value)     /* IN */
{
   bool balanced = FALSE;
   ASSERT(tree);
   PerfxTree_DoInsert(tree, &tree->root, key, value, &balanced);
}


static PerfxTreeNode *
PerfxTree_DoLookup(PerfxTree *tree,     /* IN */
                   PerfxTreeNode *node, /* IN */
                   const void *key)     /* IN */
{
   int cmpval;

   ASSERT(tree);

   if (!node) {
      return NULL;
   }

   cmpval = tree->key_compare(key, node->key);

   if (cmpval < 0) {
      return PerfxTree_DoLookup(tree, node->left, key);
   } else if (cmpval > 0) {
      return PerfxTree_DoLookup(tree, node->right, key);
   }

   return (!node->hidden) ? node : NULL;
}


void *
PerfxTree_Lookup(PerfxTree *tree, /* IN */
                 const void *key) /* IN */
{
   PerfxTreeNode *node;

   ASSERT(tree);

   if ((node = PerfxTree_DoLookup(tree, tree->root, key))) {
      return node->value;
   }

   return NULL;
}


static void
PerfxTree_Hide(PerfxTree *tree,     /* IN */
               PerfxTreeNode *node, /* IN */
               const void *key)     /* IN */
{
   int cmpval;

   ASSERT(tree);

   if (!node) {
      return;
   }

   cmpval = tree->key_compare(key, node->key);

   if (cmpval < 0) {
      PerfxTree_Hide(tree, node->left, key);
   } else if (cmpval > 0) {
      PerfxTree_Hide(tree, node->right, key);
   } else {
      node->hidden = TRUE;
   }
}


void
PerfxTree_Remove(PerfxTree *tree, /* IN */
                 const void *key) /* IN */
{
   ASSERT(tree);
   PerfxTree_Hide(tree, tree->root, key);
}
