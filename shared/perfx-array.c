/* perfx-array.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include <stdlib.h>
#include <string.h>

#include "perfx-array.h"
#include "perfx-debug.h"
#include "perfx-memory.h"


/*
 *--------------------------------------------------------------------------
 *
 * PerfxArray_Grow --
 *
 *       Grow the allocated size of the array to the new length, counted
 *       in number of elements.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxArray_Grow(PerfxArray *array, /* IN */
                uint32_t len)      /* IN */
{
   size_t bytes;

   ASSERT(array);
   ASSERT(len > array->len);

   bytes = ((size_t)len) * ((size_t)array->element_size);

   array->data = realloc(array->data, bytes);
   ASSERT(array->data);

   array->allocated_len = len;

   if (array->zeroed) {
      memset(array->data + (array->len * array->element_size), 0,
             (array->allocated_len - array->len) * array->element_size);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxArray_Create --
 *
 *       Create a new PerfxArray. If zereoed is TRUE, then memory will be
 *       zeroed on allocation. Element size should be set to the number of
 *       bytes in each element.
 *
 * Returns:
 *       A newly allocated PerfxArray that should be freed with
 *       PerfxArray_Free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxArray *
PerfxArray_Create(bool zeroed,           /* IN */
                  uint32_t element_size) /* IN */
{
   return PerfxArray_CreateSized(zeroed, element_size, 16);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxArray_CreateSized --
 *
 *       Like PerfxArray_Create() except that you can specify the amount
 *       of memory to allocate right away. This can save time if you know
 *       the number of items you will need in the array upon creation.
 *
 * Returns:
 *       A PerfxArray that should be freed with PerfxArray_Free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxArray *
PerfxArray_CreateSized(bool zeroed,           /* IN */
                       uint32_t element_size, /* IN */
                       uint32_t count)        /* IN */
{
   PerfxArray *array;

   array = perfx_new0(PerfxArray, 1);
   array->zeroed = zeroed;
   array->element_size = element_size;
   if (count) {
      PerfxArray_Grow(array, count);
   }
   return array;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxArray_Destroy --
 *
 *       Like PerfxArray_Free() except the structure is not freed. This
 *       is useful for static PerfxArray structures initialized with
 *       PERFX_ARRAY_INITIALIZER().
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       Structures resources are released.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxArray_Destroy(PerfxArray *array) /* IN */
{
   ASSERT(array);

   perfx_free(array->data);

   array->data = NULL;
   array->len = 0;
   array->allocated_len = 0;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxArray_Free --
 *
 *       Frees an array and associated resources.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       The array is freed.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxArray_Free(PerfxArray *array) /* IN */
{
   ASSERT(array);

   PerfxArray_Destroy(array);
   perfx_free(array);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxArray_Remove --
 *
 *       Remove an element from the array.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       The item is removed from array.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxArray_Remove(PerfxArray *array, /* IN */
                  uint32_t idx)    /* IN */
{
   ASSERT(array);
   ASSERT(idx < array->len);

   if ((idx + 1) == array->len) {
      array->len--;
      return;
   }

   memmove(array->data + (array->element_size * idx),
           array->data + (array->element_size * (idx + 1)),
           ((array->len - 1 - idx) * array->element_size));
   array->len--;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxArray_RemoveFast --
 *
 *       Remove an item from the array. The item is replaced with the last
 *       item in the array allowing this to perform in O(1) running time.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       The item is removed from the array.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxArray_RemoveFast(PerfxArray *array, /* IN */
                      uint32_t idx)      /* IN */
{
   ASSERT(array);
   ASSERT(idx < array->len);

   if ((idx + 1) == array->len) {
      array->len--;
      return;
   }

   memcpy(array->data + (array->element_size * idx),
          array->data + (array->element_size * (array->len - 1)),
          array->element_size);
   array->len--;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxArray_AppendRange --
 *
 *       Append a range of items to the array. range should be a pointer
 *       to the first item and count the number of elements.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       The range of items is added to the array.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxArray_AppendRange(PerfxArray *array, /* IN */
                       uint32_t count,    /* IN */
                       const void *range) /* IN */
{
   uint32_t alen;

   ASSERT(array);
   ASSERT(range);

   if ((array->len + count) > array->allocated_len) {
      alen = MAX(16, array->allocated_len);
      while ((array->len + count) > alen) {
         alen <<= 1;
      }
      PerfxArray_Grow(array, alen);
   }

   memcpy(array->data + (array->element_size * array->len),
          range, (count * array->element_size));
   array->len += count;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxArray_StealData --
 *
 *       Steals the data from the array and resizes it to zero length.
 *       The caller becomes the owner of the data. The number of elements
 *       is stored in len.
 *
 * Returns:
 *       The previous array->data pointer.
 *
 * Side effects:
 *       len is set to number of elements.
 *
 *--------------------------------------------------------------------------
 */

void *
PerfxArray_StealData(PerfxArray *array, /* IN */
                     size_t *len)       /* OUT */
{
   void *ret = NULL;
   size_t retlen = 0;

   ASSERT(array);

   if (array->len) {
      ret = array->data;
      retlen = array->len;

      array->allocated_len = 0;
      array->data = NULL;
      array->len = 0;
   }

   if (len) {
      *len = retlen;
   }

   return ret;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxArray_Clear --
 *
 *       Remove all items from the array. The allocated size of the array
 *       does not change.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxArray_Clear(PerfxArray *array) /* IN */
{
   ASSERT(array);

   array->len = 0;

   if (array->zeroed) {
      memset(array->data, 0, array->allocated_len * array->element_size);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * quicksort --
 *
 *       A simple quicksort implementation that works with variable sized
 *       element sizes using memcpy. @temp is expected to be at least
 *       @element_size bytes.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
quicksort(void *data,               /* IN */
          uint32_t element_size,    /* IN */
          uint8_t *temp,            /* IN */
          PerfxCompareFunc compare, /* IN */
          uint32_t begin,           /* IN */
          uint32_t end)             /* IN */
{
   uint32_t i = begin;
   uint32_t j = end;

   ASSERT(data);
   ASSERT(element_size);
   ASSERT(temp);
   ASSERT(compare);

   if (i > j) {
      return;
   }

#define AT_OFFSET(x) (data + (element_size * (x)))

   while (i < j) {
      for (; compare(AT_OFFSET(i), AT_OFFSET(j)) < 0; i++);
      memcpy(temp, AT_OFFSET(i), element_size);
      memcpy(AT_OFFSET(i), AT_OFFSET(j-1), element_size);
      memcpy(AT_OFFSET(j-1), AT_OFFSET(j), element_size);
      memcpy(AT_OFFSET(j), temp, element_size);
      j--;
   }

#undef AT_OFFSET

   if (i > 0) {
      quicksort(data, element_size, temp, compare, begin, i - 1);
   }

   if (j < end) {
      quicksort(data, element_size, temp, compare, j + 1, end);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxArray_Sort --
 *
 *       Sorts the contents of @array using @compare to compare elements.
 *       The sort is performed using quicksort().
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxArray_Sort(PerfxArray *array,        /* IN */
                PerfxCompareFunc compare) /* IN */
{
   uint8_t *temp;

   ASSERT(array);
   ASSERT(compare);

   if (array->len > 1) {
      temp = perfx_malloc(array->element_size);
      quicksort(array->data, array->element_size, temp,
                compare, 0, array->len - 1);
      perfx_free(temp);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxArray_SearchFull --
 *
 *       Performs a binary search on the array using the compare func
 *       provided. If the item could not be found, then -1 is returned
 *       and @lower_bound and @upper_bound will be set if non-NULL.
 *       These values represent the index directly lower and the index
 *       directly upper to where the item should have existed in the
 *       sorted array. This is useful for when the item is not found, but
 *       you'd like to retrieve the nearest item.
 *
 *       The array must be sorted before calling this function.
 *
 * Returns:
 *       The index of the matching item if successful; otherwise -1.
 *
 * Side effects:
 *       @lower_bound set to the item lower of intended target.
 *       @upper_bound set to the item above of intended target.
 *
 *--------------------------------------------------------------------------
 */

int32_t
PerfxArray_SearchFull(PerfxArray *array,        /* IN */
                      const void *target,       /* IN */
                      PerfxCompareFunc compare, /* IN */
                      int32_t *lower_bound,     /* OUT */
                      int32_t *upper_bound)     /* OUT */
{
   int32_t left;
   int32_t middle;
   int32_t right;
   int32_t cmpval;

   ASSERT(array);
   ASSERT(compare);

   if (lower_bound) {
      *lower_bound = -1;
   }

   if (upper_bound) {
      *upper_bound = -1;
   }

   if (!array->len) {
      return -1;
   }

   left = 0;
   right = array->len - 1;

#define AT_OFFSET(x) (array->data + (array->element_size * (x)))

   while (left <= right) {
      middle = (left + right) / 2;
      cmpval = compare(AT_OFFSET(middle), target);
      if (cmpval < 0) {
         left = middle + 1;
      } else if (cmpval > 0) {
         right = middle - 1;
      } else {
         return middle;
      }
   }

#undef AT_OFFSET

   if (lower_bound) {
      if (right < 0) {
         *lower_bound = -1;
      } else if (left >= array->len) {
         *lower_bound = array->len - 1;
      } else {
         *lower_bound = left - 1;
      }
   }

   if (upper_bound) {
      if (left >= array->len) {
         *upper_bound = array->len;
      } else if (right < 0) {
         *upper_bound = 0;
      } else {
         *upper_bound = right + 1;
      }
   }

   return -1;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxArray_Search --
 *
 *       Like PerfxArray_SearchFull() with @lower_bound and @upper_bound
 *       set to NULL.
 *
 * Returns:
 *       The index if successful; otherwise -1.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

int32_t
PerfxArray_Search(PerfxArray *array,        /* IN */
                  const void *target,       /* IN */
                  PerfxCompareFunc compare) /* IN */
{
   return PerfxArray_SearchFull(array, target, compare, NULL, NULL);
}
