/* perfx-port.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <errno.h>
#include <pthread.h>
#include <string.h>
#if defined(__linux__)
#include <sys/sysinfo.h>
#elif defined(__APPLE__)
#include <sys/types.h>
#include <sys/sysctl.h>
#endif

#include "perfx-array.h"
#include "perfx-atomic.h"
#include "perfx-debug.h"
#include "perfx-memory.h"
#include "perfx-port.h"


#define DEFAULT_QUEUE_SIZE 32


typedef struct
{
   pthread_mutex_t mutex;
   pthread_cond_t cond;
   PerfxMessage *head;
   PerfxMessage *tail;
} PerfxPortQueue;


struct _PerfxPort
{
   PerfxPortHandler handler;
   void *handler_data;
   PerfxPortQueue queue;
   uint16_t max_active;
   uint16_t active;
   bool shutdown;
};


typedef struct
{
   pthread_mutex_t mutex;
   uint32_t mask;
   volatile uint32_t head_idx;
   volatile uint32_t tail_idx;
   uint32_t datalen;
   void **data;
} PerfxPortDispatcherQueue;


typedef struct
{
   uint32_t idx;
   pthread_t thread;
   PerfxPortDispatcherQueue queue;
} PerfxPortDispatcher;


static PerfxPortQueue       gPortQueue;
static PerfxPortDispatcher *gPortDispatchers;
static pthread_key_t        gPortDispatchersCurrentKey;
static uint32_t             gPortDispatchersLen;


static void *PerfxPortDispatcher_Thread(void *) __attribute__((noreturn));
static void  PerfxPort_HandleMessage(PerfxPort *, PerfxMessage *);


static PerfxMessage *
PerfxPortQueue_TryPopLocked(PerfxPortQueue *queue) /* IN */
{
   PerfxMessage *message;

   ASSERT(queue);

   if ((message = queue->head)) {
      queue->head = message->next;
      message->next = NULL;
   }
   if (queue->tail == message) {
      queue->tail = NULL;
   }

   return message;
}

static PerfxMessage *
PerfxPortQueue_TryPop(PerfxPortQueue *queue) /* IN */
{
   PerfxMessage *message;

   ASSERT(queue);

   pthread_mutex_lock(&queue->mutex);
   message = PerfxPortQueue_TryPopLocked(queue);
   pthread_mutex_unlock(&queue->mutex);

   return message;
}


static PerfxMessage *
PerfxPortQueue_Pop(PerfxPortQueue *queue) /* IN */
{
   PerfxMessage *message;

   ASSERT(queue);

   pthread_mutex_lock(&queue->mutex);
   while (!(message = queue->head)) {
      pthread_cond_wait(&queue->cond, &queue->mutex);
   }
   queue->head = message->next;
   if (queue->tail == message) {
      queue->tail = NULL;
   }
   pthread_mutex_unlock(&queue->mutex);
   message->next = NULL;

   return message;
}


static void
PerfxPortQueue_PushLocked(PerfxPortQueue *queue, /* IN */
                          PerfxMessage *message) /* IN */
{
   ASSERT(queue);
   ASSERT(message);

   if (queue->tail) {
      queue->tail->next = message;
   }
   queue->tail = message;
   if (!queue->head) {
      queue->head = message;
   }

   pthread_cond_signal(&queue->cond);
}


static void
PerfxPortQueue_Push(PerfxPortQueue *queue, /* IN */
                    PerfxMessage *message) /* IN */
{
   ASSERT(queue);
   ASSERT(message);

   message->next = NULL;

   pthread_mutex_lock(&queue->mutex);
   PerfxPortQueue_PushLocked(queue, message);
   pthread_mutex_unlock(&queue->mutex);
}


static void
PerfxPortQueue_Destroy(PerfxPortQueue *queue) /* IN */
{
   ASSERT(queue);

   queue->head = NULL;
   queue->tail = NULL;
   pthread_mutex_destroy(&queue->mutex);
   pthread_cond_destroy(&queue->cond);
}


static void
PerfxPortQueue_Init(PerfxPortQueue *queue) /* IN */
{
   ASSERT(queue);

   queue->head = NULL;
   queue->tail = NULL;
   pthread_mutex_init(&queue->mutex, NULL);
   pthread_cond_init(&queue->cond, NULL);
}


static PerfxMessage *
PerfxPortDispatcherQueue_PopRemote(PerfxPortDispatcherQueue *queue) /* IN */
{
   PerfxMessage *message = NULL;
   uint32_t head;

   ASSERT(queue);

   if (!pthread_mutex_trylock(&queue->mutex)) {
      head = queue->head_idx;
      queue->head_idx = head + 1;
      if (head < queue->tail_idx) {
         message = queue->data[head & queue->mask];
      } else {
         queue->head_idx = head;
      }
      pthread_mutex_unlock(&queue->mutex);
   }

   return message;
}


static PerfxMessage *
PerfxPortDispatcherQueue_PopLocal(PerfxPortDispatcherQueue *queue) /* IN */
{
   PerfxMessage *message = NULL;
   uint32_t tail;

   ASSERT(queue);

   tail = queue->tail_idx;
   if (queue->head_idx >= tail) {
      return NULL;
   }

   tail--;
   queue->tail_idx = tail;

   if (queue->head_idx <= tail) {
      message = queue->data[tail & queue->mask];
   } else {
      pthread_mutex_lock(&queue->mutex);
      if (queue->head_idx <= tail) {
         message = queue->data[tail & queue->mask];
      } else {
         queue->tail_idx = tail + 1;
      }
      pthread_mutex_unlock(&queue->mutex);
   }

   return message;
}


static void
PerfxPortDispatcherQueue_Push(PerfxPortDispatcherQueue *queue, /* IN */
                              PerfxMessage *message)           /* IN */
{
   void **old_data = NULL;
   void **new_data;
   uint32_t tail;
   uint32_t datalen;

   ASSERT(queue);

   tail = queue->tail_idx;

   if (tail < (queue->head_idx + queue->mask)) {
      queue->data[tail & queue->mask] = message;
      queue->tail_idx = tail + 1;
   } else {
      pthread_mutex_lock(&queue->mutex);

      datalen = queue->tail_idx - queue->head_idx;
      old_data = queue->data;

      if (datalen >= queue->mask) {
         new_data = perfx_new0(void *, (queue->datalen << 1));
         memcpy(new_data,
                old_data,
                sizeof(void *) * queue->datalen);
         queue->data = new_data;
         queue->head_idx = 0;
         queue->tail_idx = tail = datalen;
         queue->mask = (queue->mask << 1) | 1;
         queue->datalen = datalen;
      } else {
         old_data = NULL;
      }

      queue->data[tail & queue->mask] = message;
      queue->tail_idx = tail + 1;

      pthread_mutex_unlock(&queue->mutex);

      if (old_data) {
         perfx_free(old_data);
      }
   }
}


static void
PerfxPortDispatcherQueue_Destroy(PerfxPortDispatcherQueue *queue) /* IN */
{
   ASSERT(queue);

   pthread_mutex_destroy(&queue->mutex);
   perfx_free(queue->data);
}


static void
PerfxPortDispatcherQueue_Init(PerfxPortDispatcherQueue *queue) /* IN */
{
   ASSERT(queue);

   memset(queue, 0, sizeof *queue);
   pthread_mutex_init(&queue->mutex, NULL);
   queue->mask = DEFAULT_QUEUE_SIZE - 1;
   queue->datalen = DEFAULT_QUEUE_SIZE;
   queue->data = perfx_new0(void *, DEFAULT_QUEUE_SIZE);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPortDispatcher_Pop --
 *
 *       Remove a message from the head of the dispatcher queue.
 *
 * Returns:
 *       A PerfxMessage* or NULL.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static PerfxMessage *
PerfxPortDispatcher_Pop(PerfxPortDispatcher *dispatcher) /* IN */
{
   PerfxMessage *message = NULL;
   uint32_t idx;
   uint32_t i;

   ASSERT(dispatcher);

   idx = dispatcher->idx;

   if ((message = PerfxPortDispatcherQueue_PopLocal(&dispatcher->queue))) {
      return message;
   }

   if ((message = PerfxPortQueue_TryPop(&gPortQueue))) {
      return message;
   }

   for (i = 1; i < gPortDispatchersLen; i++) {
      dispatcher = &gPortDispatchers[(idx + i) % gPortDispatchersLen];
      if ((message = PerfxPortDispatcherQueue_PopRemote(&dispatcher->queue))) {
         return message;
      }
   }

   return PerfxPortQueue_Pop(&gPortQueue);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPortDispatcher_GetCurrent --
 *
 *       Fetch PerfxPortDispatcher used by the current thread.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxPortDispatcher *
PerfxPortDispatcher_GetCurrent(void)
{
   return pthread_getspecific(gPortDispatchersCurrentKey);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPortDispatcher_Thread --
 *
 *       Thread worker for dispatching messages for a given port.
 *
 * Returns:
 *       NULL, after message queue has been processed.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void *
PerfxPortDispatcher_Thread(void *data) /* IN */
{
   PerfxPortDispatcher *dispatcher = data;
   PerfxMessage *message;

   ASSERT(dispatcher);

   pthread_setspecific(gPortDispatchersCurrentKey, dispatcher);

   while ((message = PerfxPortDispatcher_Pop(dispatcher))) {
      ASSERT(message->port);
      PerfxPort_HandleMessage(message->port, message);
   }

   pthread_setspecific(gPortDispatchersCurrentKey, NULL);

   PerfxPortDispatcherQueue_Destroy(&dispatcher->queue);

   ASSERT_NOT_REACHED();
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPortDispatcher_Init --
 *
 *       Initializes a port dispatcher.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxPortDispatcher_Init(PerfxPortDispatcher *dispatcher, /* IN */
                         uint32_t idx)                    /* IN */
{
   ASSERT(dispatcher);

   dispatcher->idx = idx;
   PerfxPortDispatcherQueue_Init(&dispatcher->queue);
   pthread_create(&dispatcher->thread, NULL,
                  PerfxPortDispatcher_Thread, dispatcher);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPort_InitOnce --
 *
 *       Initializes the port dispatchers which will process incoming
 *       messages for various ports.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxPort_InitOnce(void)
{
   uint32_t i;

   ASSERT(!gPortDispatchers);
   ASSERT(!gPortDispatchersLen);

   pthread_key_create(&gPortDispatchersCurrentKey, NULL);

#if defined(__linux__)
   gPortDispatchersLen = get_nprocs();
#elif defined(__APPLE__)
   {
      int count = 0;
      size_t size = sizeof(count);
      sysctlbyname("hw.ncpu", &count, &size, NULL, 0);
      gPortDispatchersLen = count;
   }
#else
#error "You're platform is not supported."
#endif

   PerfxPortQueue_Init(&gPortQueue);

   gPortDispatchers = perfx_new0(PerfxPort, gPortDispatchersLen);
   for (i = 0; i < gPortDispatchersLen; i++) {
      PerfxPortDispatcher_Init(&gPortDispatchers[i], i);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPort_Create --
 *
 *       Creates a new PerfxPort that can be used to delivery messages to
 *       a particular handler.
 *
 * Returns:
 *       A newly allocated PerfxPort that should be freed with
 *       PerfxPort_Free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxPort *
PerfxPort_Create(int32_t max_active,       /* IN */
                 PerfxPortHandler handler, /* IN */
                 void *handler_data)       /* IN */
{
   static pthread_once_t once = PTHREAD_ONCE_INIT;

   PerfxPort *port;

   ASSERT(handler);
   ASSERT_INT(max_active, >=, -1);

   pthread_once(&once, PerfxPort_InitOnce);

   if (!max_active) {
      max_active = gPortDispatchersLen;
   }

   port = perfx_new0(PerfxPort, 1);
   port->max_active = max_active;
   port->handler = handler;
   port->handler_data = handler_data;
   PerfxPortQueue_Init(&port->queue);

   return port;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPort_Post --
 *
 *       Post a message to the port. The handler registered for the port
 *       will given the message when the port next allows it.
 *
 *       The port could already be processing a callback, which means it
 *       wont be delivered until the last message has been processed. If
 *       the port is configured to handle messages concurrently, the
 *       message may be processed immediately.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxPort_Post(PerfxPort *port,       /* IN */
               PerfxMessage *message) /* IN */
{
   PerfxPortDispatcher *dispatcher;

   ASSERT(port);
   ASSERT(message);

   message->port = port;
   message->next = NULL;

   pthread_mutex_lock(&port->queue.mutex);

   if (port->active < port->max_active) {
      port->active++;
      if ((dispatcher = PerfxPortDispatcher_GetCurrent())) {
         PerfxPortDispatcherQueue_Push(&dispatcher->queue, message);
      } else {
         pthread_mutex_unlock(&port->queue.mutex);
         PerfxPortQueue_Push(&gPortQueue, message);
         return;
      }
   } else {
      PerfxPortQueue_PushLocked(&port->queue, message);
   }

   pthread_mutex_unlock(&port->queue.mutex);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPort_Free --
 *
 *       Frees the PerfxPort. This function will block until all items
 *       that have been posted to the port have been flushed.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxPort_Free(PerfxPort *port) /* IN */
{
   if (port) {
      pthread_mutex_lock(&port->queue.mutex);
      port->shutdown = TRUE;
      while (port->active || port->queue.head) {
         pthread_cond_wait(&port->queue.cond, &port->queue.mutex);
      }
      pthread_mutex_unlock(&port->queue.mutex);
      PerfxPortQueue_Destroy(&port->queue);
      perfx_free(port);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPort_HandleMessage --
 *
 *       Handles the processing of a message for a given port. The message
 *       is passed to the handler callback provided on port creation.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       message is freed.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxPort_HandleMessage(PerfxPort *port,       /* IN */
                        PerfxMessage *message) /* IN */
{
   PerfxPortDispatcher *dispatcher;

   ASSERT(port);
   ASSERT(port->handler);
   ASSERT(message);

   /*
    * Dispatch the message to the given port handler.
    */
   port->handler(port, message, port->handler_data);

   /*
    * Release the message.
    */
   PerfxMessage_Free(message);
   message = NULL;

   /*
    * Queue the next item in the port to our local queue.
    */
   pthread_mutex_lock(&port->queue.mutex);
   port->active--;
   if ((message = PerfxPortQueue_TryPopLocked(&port->queue))) {
      port->active++;
   } else if (!port->active && port->shutdown) {
      pthread_cond_signal(&port->queue.cond);
   }
   pthread_mutex_unlock(&port->queue.mutex);
   if (message) {
      dispatcher = PerfxPortDispatcher_GetCurrent();
      PerfxPortDispatcherQueue_Push(&dispatcher->queue, message);
   }
}
