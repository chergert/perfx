/* perfx-quark.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <pthread.h>
#include <stdlib.h>
#include <string.h>

#include "perfx-array.h"
#include "perfx-debug.h"
#include "perfx-hashtable.h"
#include "perfx-quark.h"


typedef struct {
   uint32_t quark;
   char *string;
   bool is_static;
} PerfxQuarkEntry;


static PerfxArray       *gQuarkEntries;
static PerfxHashTable   *gQuarks;
static PerfxHashTable   *gStrings;
static pthread_rwlock_t  gRWLock;


/*
 *--------------------------------------------------------------------------
 *
 * PerfxQuark_CreateEntry --
 *
 *       Creates a new entry for the quark. If is_static is set, then
 *       the string will not be copied.
 *
 *       The caller is expected to hold the write lock for gRWLock.
 *
 * Returns:
 *       A newly allocated PerfxQuarkEntry which should not be modified
 *       or freed.
 *
 * Side effects:
 *       PerfxQuarkEntry is allocated and inserted into global resources.
 *
 *--------------------------------------------------------------------------
 */

static PerfxQuarkEntry *
PerfxQuark_CreateEntry(const char *string, /* IN */
                       bool is_static)     /* IN */
{
   PerfxQuarkEntry entry = { 0 };
   PerfxQuarkEntry *ptr;

   ASSERT(string);

   entry.string = is_static ? (char *)string : strdup(string);
   entry.is_static = is_static;
   entry.quark = gQuarkEntries->len + 1;

   PerfxArray_Append(gQuarkEntries, entry);

   ptr = &PerfxArray_Index(gQuarkEntries, PerfxQuarkEntry,
                           gQuarkEntries->len - 1);
   PerfxHashTable_Insert(gQuarks, &entry.quark, ptr);
   PerfxHashTable_Insert(gStrings, entry.string, ptr);

   return ptr;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxQuark_Init --
 *
 *       Allocates the required resources for the quark system.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       Global resources are allocated.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxQuark_Init(void)
{
   ASSERT(!gQuarkEntries);
   ASSERT(!gQuarks);
   ASSERT(!gStrings);

   pthread_rwlock_init(&gRWLock, NULL);
   gQuarkEntries = PerfxArray_Create(FALSE, sizeof(PerfxQuarkEntry));
   gStrings = PerfxHashTable_Create(1024,
                                    perfx_str_hash,
                                    perfx_str_equal,
                                    NULL, NULL);
   gQuarks = PerfxHashTable_Create(1024,
                                   perfx_int_hash,
                                   perfx_int_equal,
                                   NULL, NULL);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxQuark_Destroy --
 *
 *       Cleans up the quark system.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       Global allocates are released.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxQuark_Destroy(void)
{
   PerfxQuarkEntry *entry;
   uint32_t i;

   ASSERT(gQuarkEntries);
   ASSERT(gQuarks);
   ASSERT(gStrings);

   PerfxHashTable_Free(gQuarks);
   gQuarks = NULL;

   PerfxHashTable_Free(gStrings);
   gStrings = NULL;

   for (i = 0; i < gQuarkEntries->len; i++) {
      entry = &PerfxArray_Index(gQuarkEntries, PerfxQuarkEntry, i);
      if (!entry->is_static) {
         free((char *)entry->string);
      }
   }

   PerfxArray_Free(gQuarkEntries);
   gQuarkEntries = NULL;

   pthread_rwlock_destroy(&gRWLock);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxQuark_TryFromString --
 *
 *       Attempts to retrieve the PerfxQuark only if it has previously
 *       been registered. If the string has not been registered, then
 *       an invalid quark (zero) is returned.
 *
 * Returns:
 *       A PerfxQuark or zero.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxQuark
PerfxQuark_TryFromString(const char *string) /* IN */
{
   PerfxQuarkEntry *entry;

   ASSERT(gStrings);

   pthread_rwlock_rdlock(&gRWLock);
   entry = PerfxHashTable_Lookup(gStrings, string);
   pthread_rwlock_unlock(&gRWLock);

   return entry ? entry->quark : 0;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxQuark_FindOrCreateString --
 *
 *       Locates the quark for a given string. If it has not yet been
 *       registered, it will be. If is_static is set, no copy of the
 *       string will be made.
 *
 * Returns:
 *       A PerfxQuark.
 *
 * Side effects:
 *       The string may be registered.
 *
 *--------------------------------------------------------------------------
 */

static PerfxQuark
PerfxQuark_FindOrCreateString(const char *string, /* IN */
                              bool is_static)     /* IN */
{
   PerfxQuarkEntry *entry;
   PerfxQuark q;

   ASSERT(gStrings);

   if (!(q = PerfxQuark_TryFromString(string))) {
      pthread_rwlock_wrlock(&gRWLock);
      entry = PerfxHashTable_Lookup(gStrings, string);
      if (!entry) {
         entry = PerfxQuark_CreateEntry(string, is_static);
         ASSERT(entry);
      }
      q = entry->quark;
      pthread_rwlock_unlock(&gRWLock);
   }

   return q;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxQuark_FromString --
 *
 *       Retrieves the quark that represents the string. If the string has
 *       not yet been registered, it will be registered using a copy of
 *       the provided string.
 *
 * Returns:
 *       A PerfxQuark that represents the string.
 *
 * Side effects:
 *       The string may be registered.
 *
 *--------------------------------------------------------------------------
 */

PerfxQuark
PerfxQuark_FromString(const char *string) /* IN */
{
   return PerfxQuark_FindOrCreateString(string, FALSE);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxQuark_FromStaticString --
 *
 *       Retrieves the quark that represents the internal, static string.
 *       If the string has not been registered yet, it will be and the
 *       newly created quark identifier is returned.
 *
 *       This function must not be called by modules that can be
 *       unloaded.
 *
 * Returns:
 *       A PerfxQuark that represents the string.
 *
 * Side effects:
 *       The string may be registered.
 *
 *--------------------------------------------------------------------------
 */

PerfxQuark
PerfxQuark_FromStaticString(const char *string) /* IN */
{
   return PerfxQuark_FindOrCreateString(string, TRUE);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxQuark_ToString --
 *
 *       Retrieves the string that is represented by the quark. If the
 *       quark has not been registered, NULL is returned.
 *
 * Returns:
 *       The quarks string if successful; otherwise NULL.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

const char *
PerfxQuark_ToString(PerfxQuark quark) /* IN */
{
   PerfxQuarkEntry *entry;

   ASSERT(gQuarks);

   pthread_rwlock_rdlock(&gRWLock);
   entry = PerfxHashTable_Lookup(gQuarks, INT_TO_POINTER(quark));
   pthread_rwlock_unlock(&gRWLock);

   return entry ? entry->string : NULL;
}
