/* perfx-plugin.c
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <dlfcn.h>
#include <string.h>

#include "perfx-debug.h"
#include "perfx-memory.h"
#include "perfx-path.h"
#include "perfx-plugin.h"


#define MAX_LINK_RECURSION (12)
#if defined(__linux__)
#define MODULE_PREFIX "lib"
#define MODULE_SUFFIX ".so"
#elif defined(__APPLE__)
#define MODULE_PREFIX "lib"
#define MODULE_SUFFIX ".dylib"
#else
#error "You're platform is not supported."
#endif


struct _PerfxPlugin
{
   void *handle;
   char *filename;
   char *error;
};


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPlugin_GetPath --
 *
 *       Get the path used to load the plugin.
 *
 * Returns:
 *       A string that should not be modified or freed.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

const char *
PerfxPlugin_GetPath(PerfxPlugin *plugin) /* IN */
{
   ASSERT(plugin);
   return plugin->filename;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPlugin_Create --
 *
 *       Creates a new instance of PerfxPlugin by loading the module
 *       found at @filename. If there was a failure, NULL is returned.
 *       The error string can be found by calling PerfxPlugin_GetError()
 *       with NULL as the plugin.
 *
 *       flags can be one of:
 *
 *          PERFX_PLUGIN_NOW  -- Load all symbols immediately.
 *          PERFX_PLUGIN_LAZY -- Load symbols lazily.
 *
 * Returns:
 *       A PerfxPlugin if succesful; otherwise FALSE.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxPlugin *
PerfxPlugin_Create(const char *filename,   /* IN */
                   PerfxPluginFlags flags) /* IN */
{
   PerfxPlugin *plugin;
   int dlflags = 0;

   ENTRY;

   ASSERT(filename);

   if ((flags & PERFX_PLUGIN_LAZY)) {
      dlflags |= RTLD_LAZY;
   }

   if ((flags & PERFX_PLUGIN_NOW)) {
      dlflags |= RTLD_NOW;
   }

   plugin = perfx_new0(PerfxPlugin, 1);
   plugin->filename = PerfxPath_ResolveLink(filename);

   if (!(plugin->handle = dlopen(plugin->filename, dlflags))) {
      PerfxPlugin_Free(plugin);
      RETURN(NULL);
   }

   RETURN(plugin);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPlugin_GetError --
 *
 *       Fetches the last error associated with the plugin. If plugin is
 *       NULL, then dlerror() is called to fetch the last module error.
 *       This is useful if PerfxPlugin_Create() failed.
 *
 * Returns:
 *       A string which should not be modified.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

const char *
PerfxPlugin_GetError(PerfxPlugin *plugin) /* IN */
{
   ENTRY;
   if (!plugin) {
      RETURN(dlerror());
   }
   RETURN(plugin->error);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPlugin_SetError --
 *
 *       Sets the error for the plugin. The error is cached in the plugin
 *       structure for later use by PerfxPlugin_GetError().
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxPlugin_SetError(PerfxPlugin *plugin, /* IN */
                     const char *error)   /* IN */
{
   ENTRY;
   ASSERT(plugin);
   if (plugin->error) {
      perfx_free(plugin->error);
   }
   plugin->error = perfx_strdup(error);
   EXIT;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPlugin_GetSymbol --
 *
 *       Looks to see if a symbol named @name is found in the plugin. If
 *       so, a pointer to the symbol is stored in @symbol.
 *
 * Returns:
 *       TRUE if the symbol was found; otherwise FALSE.
 *
 * Side effects:
 *       @symbol is set if TRUE is returned.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxPlugin_GetSymbol(PerfxPlugin *plugin, /* IN */
                      const char *name,    /* IN */
                      void **symbol)       /* OUT */
{
   char *error;

   ENTRY;

   ASSERT(plugin);
   ASSERT(plugin->handle);
   ASSERT(name);
   ASSERT(symbol);

   /*
    * Clear any existing errors by calling dlerror().
    * See dlsym() manpage for more information.
    */
   PerfxPlugin_SetError(plugin, NULL);
   dlerror();

   /*
    * Attempt to load the symbol. It is possible to have a valid
    * NULL symbol, meaning we need to check dlerror() for an actual
    * non-NULL indicating failure.
    */
   if (!(*symbol = dlsym(plugin->handle, name))) {
      if ((error = dlerror())) {
         PerfxPlugin_SetError(plugin, error);
         RETURN(FALSE);
      }
   }

   RETURN(TRUE);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPlugin_Free --
 *
 *       Free a PerfxPlugin instance. The library handle is also closed.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxPlugin_Free(PerfxPlugin *plugin) /* IN */
{
   if (plugin) {
      if (plugin->handle) {
         dlclose(plugin->handle);
      }
      perfx_free(plugin->filename);
      perfx_free(plugin->error);
      memset(plugin, 0, sizeof *plugin);
      perfx_free(plugin);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPlugin_LooksLikePlugin --
 *
 *       Checks to see if the file found at path looks like a plugin
 *       to the application. This checks that it has a module suffix
 *       and is executable.
 *
 * Returns:
 *       TRUE if the file found at @filename looks like a plugin.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxPlugin_LooksLikePlugin(const char *filename) /* IN */
{
   char *resolved = NULL;
   bool ret = FALSE;

   ASSERT(filename);

   if (!(resolved = PerfxPath_ResolveLink(filename))) {
      GOTO(failure);
   }

   if (!PerfxPath_IsExecutable(resolved)) {
      GOTO(failure);
   }

   if (!PerfxPath_HasPrefix(resolved, MODULE_PREFIX)) {
      GOTO(failure);
   }

   if (!PerfxPath_HasSuffix(resolved, MODULE_SUFFIX) &&
       !strstr(resolved, MODULE_SUFFIX".")) {
      GOTO(failure);
   }

   ret = TRUE;

failure:
   perfx_free(resolved);
   RETURN(ret);
}
