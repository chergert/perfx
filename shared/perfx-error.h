/* perfx-error.h
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_ERROR_H
#define PERFX_ERROR_H


#include <stdarg.h>

#include "perfx-macros.h"
#include "perfx-memory.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


typedef struct _PerfxError PerfxError;


struct _PerfxError
{
   uint32_t domain;
   uint32_t code;
   char *message;
};


void        PerfxError_Clear   (PerfxError **error);
PerfxError *PerfxError_CreateV (uint32_t domain,
                                uint32_t code,
                                const char *format,
                                va_list args) PERFX_GNUC_PRINTF(3, 0);
PerfxError *PerfxError_Create  (uint32_t domain,
                                uint32_t code,
                                const char *format,
                                ...) PERFX_GNUC_PRINTF(3, 4);
void        PerfxError_Free    (PerfxError *error);
void        PerfxError_Set     (PerfxError **error,
                                uint32_t domain,
                                uint32_t code,
                                const char *format,
                                ...) PERFX_GNUC_PRINTF(4, 5);


PERFX_END_DECLS


#endif /* PERFX_ERROR_H */
