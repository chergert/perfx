/* perfx-connection.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

#include "perfx-connection.h"
#include "perfx-debug.h"
#include "perfx-main.h"
#include "perfx-memory.h"
#include "perfx-protocol.h"
#include "perfx-resolver.h"


#define BUFFER_CHUNK_SIZE 4096


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_CreateTcp --
 *
 *       Create a new PerfxConnection by connecting to the host found at
 *       hostname and the provided port. The address for hostname is not
 *       resolved until PerfxConnection_Connect() is called. Domain name
 *       resolution happens asynchronously using PerfxResolver.
 *
 * Returns:
 *       A newly created PerfxConnection that can be freed with
 *       PerfxObject_Unref().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxConnection *
PerfxConnection_CreateTcp(const char *hostname, /* IN */
                          uint16_t port)        /* IN */
{
   PerfxConnection *connection = NULL;

   ENTRY;

   ASSERT(hostname);
   ASSERT(port);

   connection = PerfxObject_Create(PERFX_TYPE_CONNECTION);
   connection->type = PERFX_CONNECTION_TCP;
   connection->u.tcp.hostname = strdup(hostname);
   connection->u.tcp.port = port;

   RETURN(connection);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_CreatePipe --
 *
 *       Create a new PerfxConnection using file-descriptors from pipe(2).
 *
 * Returns:
 *       A newly allocated PerfxConnection. Free with PerfxObject_Unref().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxConnection *
PerfxConnection_CreatePipe(int read_fd,  /* IN */
                           int write_fd) /* IN */
{
   PerfxConnection *connection = NULL;

   ENTRY;

   ASSERT(read_fd >= 0 || write_fd >= 0);

   connection = PerfxObject_Create(PERFX_TYPE_CONNECTION);
   connection->type = PERFX_CONNECTION_PIPE;
   connection->read_fd = read_fd;
   connection->write_fd = write_fd;

   RETURN(connection);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_CreateFd --
 *
 *       Create a new PerfxConnection, using a single file-descriptor for
 *       both reading and writing.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxConnection *
PerfxConnection_CreateFd(int fd) /* IN */
{
   PerfxConnection *connection = NULL;

   ENTRY;

   ASSERT(fd >= 0);

   connection = PerfxObject_Create(PERFX_TYPE_CONNECTION);
   connection->type = PERFX_CONNECTION_FD;
   connection->read_fd = fd;
   connection->write_fd = fd;

   RETURN(connection);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_RecvPollCb --
 *
 *       Handle incoming data for the connection. Creates a buffer to
 *       contain the incoming data and passes it along to the protocol.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxConnection_RecvPollCb(int fd,                   /* IN */
                           PerfxMainPollFlags flags, /* IN */
                           void *data)               /* IN */
{
   PerfxConnection *connection = data;
   PerfxBuffer *buffer;
   uint8_t *readbuf;
   ssize_t readbuflen;

   ENTRY;

   ASSERT_INT(fd, !=, -1);
   ASSERT(flags);
   ASSERT(connection);
   ASSERT_INT(connection->read_fd, >=, 0);

   readbuf = perfx_malloc(BUFFER_CHUNK_SIZE);
   readbuflen = read(fd, readbuf, BUFFER_CHUNK_SIZE);

   if (readbuflen < 0) {
      TODO("Handle read error");
      perfx_free(readbuf);
      EXIT;
   }

   if (!connection->protocol) {
      ASSERT_NOT_REACHED();
      perfx_free(readbuf);
      EXIT;
   }

   buffer = PerfxBuffer_Create(readbuf, readbuflen, perfx_free);
   PerfxProtocol_RecvData(connection->protocol, buffer);
   PerfxBuffer_Unref(buffer);

   EXIT;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_SetConnected --
 *
 *       Set the connection state to connected. The protocol will be
 *       initialized. If the connection is readable, then a main loop
 *       handler will be added for receiving data when it is available.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxConnection_SetConnected(PerfxConnection *connection) /* IN */
{
   ASSERT(connection);
   ASSERT((connection->read_fd >= 0) || (connection->write_fd >= 0));
   ASSERT(connection->protocol == NULL);
   ASSERT(connection->protocol_type);

   connection->protocol = PerfxObject_Create(connection->protocol_type);
   ASSERT(connection->protocol);
   PerfxProtocol_ConnectionMade(connection->protocol, connection);

   if (connection->state_callback) {
      connection->state_callback(connection, connection->state_data);
   }

   if (PerfxConnection_IsReadable(connection)) {
      connection->read_handler =
         PerfxMain_AddPoll(connection->read_fd,
                           PERFX_MAIN_POLL_IN | PERFX_MAIN_POLL_HUP,
                           PerfxConnection_RecvPollCb,
                           connection);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_SetDisconnected --
 *
 *       Set the connection as disconnected.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxConnection_SetDisconnected(PerfxConnection *connection) /* IN */
{
   ASSERT(connection);

   PerfxMain_ClearHandler(&connection->connect_handler);
   PerfxMain_ClearHandler(&connection->read_handler);
   PerfxMain_ClearHandler(&connection->write_handler);

   if ((connection->read_fd == -1) && (connection->write_fd == -1)) {
      return;
   }

   connection->read_fd = -1;
   connection->write_fd = -1;

   if (connection->protocol) {
      PerfxProtocol_ConnectionLost(connection->protocol);
   }

   if (connection->state_callback) {
      connection->state_callback(connection, connection->state_data);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_ConnectPollCb --
 *
 *       Callback upon the file-descriptor used in a connect() call being
 *       ready for write or an error. The connection should either
 *       succeed or fail at this point.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxConnection_ConnectPollCb(int fd,                   /* IN */
                              PerfxMainPollFlags flags, /* IN */
                              void *data)               /* IN */
{
   PerfxConnection *connection = data;

   ENTRY;

   ASSERT(fd >= 0);
   ASSERT(flags & PERFX_MAIN_POLL_OUT);
   ASSERT(connection);

   connection->read_fd = fd;
   connection->write_fd = fd;

   PerfxMain_ClearHandler(&connection->connect_handler);

   if (flags & PERFX_MAIN_POLL_OUT) {
      PerfxConnection_SetConnected(connection);
   } else {
      PerfxConnection_SetDisconnected(connection);
   }

   PerfxObject_Unref(connection);

   EXIT;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_GetHostByNameCb --
 *
 *       Callback upon the completion of domain name resolution.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxConnection_GetHostByNameCb(PerfxResolver *resolver, /* IN */
                                const char *hostname,    /* IN */
                                struct in_addr *addr,    /* IN */
                                void *data)              /* IN */
{
   PerfxConnection *connection = data;
   struct sockaddr_in saddr;
   int sd;

   ENTRY;

   ASSERT(resolver);
   ASSERT(hostname);
   ASSERT(connection);
   ASSERT(connection->type == PERFX_CONNECTION_TCP);

   if (addr) {
      sd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
      if (-1 == sd) {
         LOG("Failed to create tcp socket");
         goto failed;
      }

      memset(&saddr, 0, sizeof saddr);
      saddr.sin_family = AF_INET;
      saddr.sin_port = htons(connection->u.tcp.port);
      saddr.sin_addr = *addr;

      fcntl(sd, F_SETFL, O_NONBLOCK);

      if (-1 == connect(sd, (struct sockaddr *)&saddr, sizeof saddr)) {
         if (errno != EINPROGRESS) {
            LOG("Failed to connect to destination.");
            goto failed;
         } else {
            /*
             * A poll() for a write condition will return when the connection
             * has completed or failed.
             */
            connection->connect_handler =
               PerfxMain_AddPoll(sd, PERFX_MAIN_POLL_OUT | PERFX_MAIN_POLL_HUP,
                                 PerfxConnection_ConnectPollCb,
                                 connection);
         }
      } else {
         /*
          * We connected immediately.
          */
         connection->read_fd = sd;
         connection->write_fd = sd;
         PerfxConnection_SetConnected(connection);
         PerfxObject_Unref(connection);
      }
   }

   EXIT;

failed:
   TODO("Mark connection as failed.");

   connection->read_fd = -1;
   connection->write_fd = -1;

   if (connection->state_callback) {
      connection->state_callback(connection, connection->state_data);
   }

   PerfxObject_Unref(connection);

   EXIT;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_Connect --
 *
 *       Connect to the connections peer, using the proper transport.
 *       TCP connections will resolve the domain name if necessary,
 *       and then connect to the peer. Pipe and file-descriptor
 *       connections will not need to perform such work.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxConnection_Connect(PerfxConnection *connection) /* IN */
{
   PerfxResolver *resolver;

   ENTRY;

   ASSERT(connection);
   ASSERT(connection->protocol_type);

   switch (connection->type) {
   case PERFX_CONNECTION_TCP:
      resolver = PerfxResolver_Create();
      PerfxResolver_GetHostByName(resolver, connection->u.tcp.hostname,
                                  PerfxConnection_GetHostByNameCb,
                                  PerfxObject_Ref(connection));
      PerfxObject_Unref(resolver);
      break;
   case PERFX_CONNECTION_FD:
   case PERFX_CONNECTION_PIPE:
      if ((connection->read_fd >= 0) || (connection->write_fd >= 0)) {
         PerfxConnection_SetConnected(connection);
      }
      break;
   default:
      ASSERT_NOT_REACHED();
   }

   EXIT;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_Disconnect --
 *
 *       Disconncet from the peer. The protocol will be notified that
 *       the connection has been lost.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxConnection_Disconnect(PerfxConnection *connection) /* IN */
{
   int same_fd = -1;

   ASSERT(connection);

   if (connection->read_fd == connection->write_fd) {
      same_fd = connection->read_fd;
   }

   if (connection->read_fd) {
      shutdown(connection->read_fd, SHUT_RD);
      connection->read_fd = -1;
   }

   if (connection->write_fd) {
      shutdown(connection->write_fd, SHUT_WR);
      connection->write_fd = -1;
   }

   if (-1 != same_fd) {
      close(same_fd);
   }

   PerfxConnection_SetDisconnected(connection);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_IsConnected --
 *
 *       Check if the connection is currently connected to a peer.
 *
 * Returns:
 *       TRUE if the connection is connected to a peer.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxConnection_IsConnected(PerfxConnection *connection) /* IN */
{
   ASSERT(connection);
   return (connection->read_fd != -1) || (connection->write_fd != -1);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_IsWriteable --
 *
 *       Check if the connection is writeable.
 *
 * Returns:
 *       TRUE if the connection is writeable.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxConnection_IsWriteable(PerfxConnection *connection) /* IN */
{
   ASSERT(connection);
   return (connection->write_fd >= 0);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_IsReadable --
 *
 *       Checks if the connection is readable.
 *
 * Returns:
 *       TRUE if the connection is readable.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxConnection_IsReadable(PerfxConnection *connection) /* IN */
{
   ASSERT(connection);
   return (connection->read_fd >= 0);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_SetStateCallback --
 *
 *       Sets the callback to be executed when the connections state
 *       changes.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxConnection_SetStateCallback(PerfxConnection *connection,           /* IN */
                                 PerfxConnectionStateCallback callback, /* IN */
                                 void *data)                            /* IN */
{
   ENTRY;
   ASSERT(connection);
   connection->state_callback = callback;
   connection->state_data = data;
   EXIT;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_SetProtocolType --
 *
 *       Set the protocol type to be created when the connection has
 *       been established to the peer.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxConnection_SetProtocolType(PerfxConnection *connection, /* IN */
                                PerfxType protocol_type)     /* IN */
{
   ASSERT(connection);
   ASSERT(PerfxType_IsA(protocol_type, PERFX_TYPE_PROTOCOL));
   ASSERT(!connection->protocol_type);
   ASSERT(connection->protocol == NULL);

   connection->protocol_type = protocol_type;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_SendPollCb --
 *
 *       Callback upon there being space available to write the buffered
 *       outgoing data.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxConnection_SendPollCb(int fd,                   /* IN */
                           PerfxMainPollFlags flags, /* IN */
                           void *data)               /* IN */
{
   PerfxConnection *connection = data;
   PerfxBuffer *buffer;
   uint32_t i;
   int ret;

   ENTRY;

   ASSERT_INT(fd, !=, -1);
   ASSERT(flags);
   ASSERT(connection);
   ASSERT_INT(connection->write_fd, >=, 0);

   if (flags & PERFX_MAIN_POLL_HUP) {
      PerfxConnection_SetDisconnected(connection);
      EXIT;
   }

   ASSERT(flags & PERFX_MAIN_POLL_OUT);

   TODO("If we cannot do a complete write, we need to slice the buffer "
        "and replace the item with the slice.");

   for (i = 0; i < connection->writes->len; i++) {
      buffer = PerfxArray_Index(connection->writes, PerfxBuffer *, i);
      ASSERT(buffer);

      ret = write(connection->write_fd, buffer, buffer->len);
      ASSERT_INT(ret, ==, buffer->len);

      PerfxBuffer_Unref(buffer);
   }

   PerfxArray_Clear(connection->writes);
   PerfxMain_ClearHandler(&connection->write_handler);

   EXIT;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_Send --
 *
 *       Send a buffer of data to the connections peer. If the buffer
 *       cannot be sent immediately, it will be sent as soon as the
 *       underlying connections write buffer has enough space.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxConnection_Send(PerfxConnection *connection, /* IN */
                     PerfxBuffer *buffer)         /* IN */
{
   ENTRY;

   ASSERT(connection);
   ASSERT(buffer);
   ASSERT(buffer->len);
   ASSERT(PerfxConnection_IsConnected(connection));

   TODO("Send immediately if write buffer is empty");

   buffer = PerfxBuffer_Ref(buffer);
   PerfxArray_Append(connection->writes, buffer);

   if (!connection->write_handler) {
      connection->write_handler =
         PerfxMain_AddPoll(connection->write_fd,
                           PERFX_MAIN_POLL_OUT | PERFX_MAIN_POLL_HUP,
                           PerfxConnection_SendPollCb,
                           connection);
   }

   EXIT;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_Init --
 *
 *       Handle the initialization of the PerfxConnection object.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxConnection_Init(PerfxObject *object) /* IN */
{
   PerfxConnection *connection = (PerfxConnection *)object;
   ASSERT(connection);
   connection->writes = PerfxArray_Create(FALSE, sizeof(PerfxBuffer *));
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnection_GetType --
 *
 *       Retrieve the PerfxType for the PerfxConnection object.
 *
 * Returns:
 *       A PerfxType.
 *
 * Side effects:
 *       Type may be registered.
 *
 *--------------------------------------------------------------------------
 */

PerfxType
PerfxConnection_GetType(void)
{
   PerfxClass *class;
   static PerfxType type = 0;

   if (!type) {
      type = PerfxClass_Register("PerfxConnection", 0,
                                 sizeof(PerfxConnectionClass),
                                 sizeof(PerfxConnection),
                                 TRUE);
      class = PerfxType_GetClass(type);
      class->Init = PerfxConnection_Init;
   }

   return type;
}
