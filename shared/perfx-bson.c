/* perfx-bson.c
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include <inttypes.h>
#include <string.h>

#include "perfx-array.h"
#include "perfx-bson.h"
#include "perfx-debug.h"
#include "perfx-string.h"
#include "perfx-utf8.h"


struct _PerfxBson
{
   PerfxArray *buf;

   const uint8_t *static_data;
   size_t static_len;
   PerfxFreeFunc static_notify;
};


#define ITER_IS_TYPE(iter, type) \
   (POINTER_TO_INT(iter->user_data5) == type)


void
PerfxBson_Free(PerfxBson *bson) /* IN */
{
   if (bson->buf) {
      PerfxArray_Free(bson->buf);
   } else if (bson->static_notify) {
      bson->static_notify((uint8_t *)bson->static_data);
   }
}


PerfxBson *
PerfxBson_CreateFromData(const uint8_t *buffer, /* IN */
                         size_t length)         /* IN */
{
   PerfxBson *bson;
   uint32_t bson_len;

   ASSERT(buffer);

   /*
    * The first 4 bytes of a BSON are the length, including the 4 bytes
    * containing said length.
    */
   memcpy(&bson_len, buffer, sizeof bson_len);
   bson_len = UINT32_FROM_LE(bson_len);
   if (bson_len != length) {
      return NULL;
   }

   bson = perfx_new0(PerfxBson, 1);
   bson->buf = PerfxArray_CreateSized(FALSE, sizeof(uint8_t), length);
   PerfxArray_AppendRange(bson->buf, length, (uint8_t *)buffer);

   return bson;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBson_CreateFromStatic --
 *
 *       Creates a new instance of PerfxBson using the static buffer
 *       provided. The buffer will not be copied and instead used
 *       directly. Modifying the BSON document with the
 *       PerfxBson_Append*() methods are not supported in conjunction
 *       with this function.
 *
 * Returns:
 *       A PerfxBson that should be freed with PerfxBson_Free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxBson *
PerfxBson_CreateFromStatic(uint8_t *buffer,      /* IN */
                           size_t length,        /* IN */
                           PerfxFreeFunc notify) /* IN */
{
   PerfxBson *bson;
   uint32_t bson_len;

   ASSERT(buffer);

   /*
    * The first 4 bytes of a BSON are the length,
    * including the 4 bytes containing said length.
    */
   memcpy(&bson_len, buffer, sizeof bson_len);
   bson_len = UINT32_FROM_LE(bson_len);
   if (bson_len != length) {
      return NULL;
   }

   bson = perfx_new0(PerfxBson, 1);
   bson->static_data = buffer;
   bson->static_len = length;
   bson->static_notify = notify;

   return bson;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBson_Create --
 *
 *       Creates a new instance of PerfxBson.
 *
 * Returns:
 *       A newly allocated PerfxBson that should be freed with
 *       PerfxBson_Free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxBson *
PerfxBson_Create(void)
{
   PerfxBson *bson;
   uint32_t len = UINT32_TO_LE(5);
   uint8_t trailing = 0;

   bson = perfx_new0(PerfxBson, 1);
   bson->buf = PerfxArray_CreateSized(FALSE, sizeof(uint8_t), 16);

   PerfxArray_AppendRange(bson->buf, sizeof len, (uint8_t *)&len);
   PerfxArray_AppendRange(bson->buf, sizeof trailing, (uint8_t *)&trailing);

   ASSERT(bson);
   ASSERT(bson->buf);
   ASSERT(bson->buf->len == 5);

   return bson;
}


bool
PerfxBson_GetEmpty(PerfxBson *bson) /* IN */
{
   uint32_t len;
   ASSERT(bson != NULL);
   len = bson->buf ? bson->buf->len : bson->static_len;
   return (len <= 5);
}


const uint8_t *
PerfxBson_GetData(PerfxBson *bson,
                  size_t *length)
{
   ASSERT(bson);
   ASSERT(length);

   if (bson->buf) {
      *length = bson->buf->len;
      return bson->buf->data;
   } else {
      *length = bson->static_len;
      return bson->static_data;
   }
}


static void
PerfxBson_Append(PerfxBson *bson,      /* IN */
                 uint8_t type,         /* IN */
                 const char *key,      /* IN */
                 const uint8_t *data1, /* IN */
                 size_t len1,          /* IN */
                 const uint8_t *data2, /* IN */
                 size_t len2)          /* IN */
{
   const uint8_t trailing = 0;
   int32_t doc_len;

   ASSERT(bson);
   ASSERT(bson->buf);
   ASSERT(type);
   ASSERT(key);
   ASSERT(data1 || len1);
   ASSERT(data2 || !len2);
   ASSERT(!data2 || data1);

   /*
    * Overwrite our trailing byte with the type for this key.
    */
   ((uint8_t *)bson->buf->data)[bson->buf->len - 1] = type;

   /*
    * Append the field name as a BSON cstring.
    */
   PerfxArray_AppendRange(bson->buf, strlen(key) + 1, key);

   /*
    * Append the data sections if needed.
    */
   if (data1) {
      PerfxArray_AppendRange(bson->buf, len1, data1);
      if (data2) {
         PerfxArray_AppendRange(bson->buf, len2, data2);
      }
   }

   /*
    * Append our trailing byte.
    */
   PerfxArray_AppendRange(bson->buf, 1, &trailing);

   /*
    * Update the document length of the buffer.
    */
   doc_len = UINT32_TO_LE(bson->buf->len);
   memcpy(bson->buf->data, &doc_len, sizeof doc_len);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBson_AppendArray --
 *
 *       Appends a BSON array to the document under field @key.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxBson_AppendArray(PerfxBson *bson,  /* IN */
                      const char *key,  /* IN */
                      PerfxBson *value) /* IN */
{
   ASSERT(bson);
   ASSERT(key);
   ASSERT(value);

   PerfxBson_Append(bson, PERFX_BSON_ARRAY, key,
                     value->buf->data, value->buf->len,
                     NULL, 0);
}


void
PerfxBson_AppendBoolean(PerfxBson *bson, /* IN */
                        const char *key, /* IN */
                        bool value)      /* IN */
{
   uint8_t b = !!value;
   ASSERT(bson);
   ASSERT(key);
   PerfxBson_Append(bson, PERFX_BSON_BOOLEAN, key, &b, 1, NULL, 0);
}


void
PerfxBson_AppendDocument(PerfxBson *bson,  /* IN */
                         const char *key,  /* IN */
                         PerfxBson *value) /* IN */
{
   const uint8_t *data;
   size_t data_len;

   ASSERT(bson != NULL);
   ASSERT(key != NULL);
   ASSERT(value != NULL);

   data = PerfxBson_GetData(value, &data_len);
   PerfxBson_Append(bson, PERFX_BSON_DOCUMENT, key, data, data_len, NULL, 0);
}


void
PerfxBson_AppendDouble(PerfxBson *bson, /* IN */
                       const char *key, /* IN */
                       double value)    /* IN */
{
   ASSERT(bson);
   ASSERT(key);

   PerfxBson_Append(bson, PERFX_BSON_DOUBLE, key,
                    (const uint8_t *)&value, sizeof value,
                    NULL, 0);
}


void
PerfxBson_AppendInt(PerfxBson *bson, /* IN */
                    const char *key, /* IN */
                    int32_t value)   /* IN */
{
   ASSERT(bson);
   ASSERT(key);

   PerfxBson_Append(bson, PERFX_BSON_INT32, key,
                    (const uint8_t *)&value, sizeof value,
                    NULL, 0);
}


void
PerfxBson_AppendInt64(PerfxBson *bson, /* IN */
                      const char *key, /* IN */
                      int64_t value)   /* IN */
{
   ASSERT(bson);
   ASSERT(key);

   PerfxBson_Append(bson, PERFX_BSON_INT64, key,
                    (const uint8_t *)&value, sizeof value,
                    NULL, 0);
}


void
PerfxBson_AppendNull(PerfxBson *bson, /* IN */
                     const char *key) /* IN */
{
   ASSERT(bson);
   ASSERT(key);

   PerfxBson_Append(bson, PERFX_BSON_NULL, key, NULL, 0, NULL, 0);
}


void
PerfxBson_AppendId(PerfxBson *bson, /* IN */
                   const char *key, /* IN */
                   PerfxBsonId *id) /* IN */
{
   ASSERT(bson);
   ASSERT(key);
   ASSERT(id);

   PerfxBson_Append(bson, PERFX_BSON_OBJECT_ID, key,
                    (const uint8_t *)id, 12,
                    NULL, 0);
}


void
PerfxBson_AppendRegex(PerfxBson *bson,     /* IN */
                      const char *key,     /* IN */
                      const char *regex,   /* IN */
                      const char *options) /* IN */
{
   ASSERT(bson);
   ASSERT(key);
   ASSERT(regex);

   if (!options) {
      options = "";
   }

   PerfxBson_Append(bson, PERFX_BSON_REGEX, key,
                    (const uint8_t *)regex, strlen(regex) + 1,
                    (const uint8_t *)options, strlen(options) + 1);
}


void
PerfxBson_AppendString(PerfxBson *bson,   /* IN */
                       const char *key,   /* IN */
                       const char *value) /* IN */
{
   uint32_t value_len;
   uint32_t value_len_swab;

   ASSERT(bson);
   ASSERT(key);

   value = value ? value : "";
   value_len = strlen(value) + 1;
   value_len_swab = UINT32_TO_LE(value_len);

   PerfxBson_Append(bson, PERFX_BSON_UTF8, key,
                    (const uint8_t *)&value_len_swab, sizeof value_len_swab,
                    (const uint8_t *)value, value_len);
}


void
PerfxBson_AppendTimeval(PerfxBson *bson,       /* IN */
                        const char *key,       /* IN */
                        struct timeval *value) /* IN */
{
   uint64_t msec;

   ASSERT(bson);
   ASSERT(key);
   ASSERT(value);

   msec = (value->tv_sec * 1000) + (value->tv_usec / 1000);
   PerfxBson_Append(bson, PERFX_BSON_DATE_TIME, key,
                    (const uint8_t *)&msec, sizeof msec,
                    NULL, 0);
}


void
PerfxBson_AppendUndefined(PerfxBson *bson, /* IN */
                          const char *key) /* IN */
{
   ASSERT(bson);
   ASSERT(key);
   PerfxBson_Append(bson, PERFX_BSON_UNDEFINED, key,
                    NULL, 0, NULL, 0);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_Init --
 *
 *       Initializes a PerfxBsonIter to iterate through @bson.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxBsonIter_Init(PerfxBsonIter *iter, /* OUT */
                   PerfxBson *bson)     /* IN */
{
   ASSERT(iter);
   ASSERT(bson);

   memset(iter, 0, sizeof *iter);
   if (bson->static_data) {
      iter->user_data1 = (uint8_t *)bson->static_data;
      iter->user_data2 = INT_TO_POINTER(bson->static_len);
   } else {
      iter->user_data1 = bson->buf->data;
      iter->user_data2 = INT_TO_POINTER(bson->buf->len);
   }
   iter->user_data3 = INT_TO_POINTER(3); /* End of size buffer */
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_Find --
 *
 *       Increments the PerfxBsonIter until @key is found or the end of
 *       the PerfxBsonIter.
 *
 * Returns:
 *       TRUE if successful; otherwise FALSE.
 *
 * Side effects:
 *       Moves the position of the iterator forward.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxBsonIter_Find(PerfxBsonIter *iter, /* IN */
                   const char *key)     /* IN */
{
   ASSERT(iter);
   ASSERT(key);

   while (PerfxBsonIter_Next(iter)){
      if (!perfx_strcmp0(key, PerfxBsonIter_GetKey(iter))) {
         return TRUE;
      }
   }

   return FALSE;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_GetKey --
 *
 *       Fetches the key for the currently observed item of the iterator.
 *
 * Returns:
 *       A string which should not be modified or freed.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

const char *
PerfxBsonIter_GetKey(PerfxBsonIter *iter) /* IN */
{
   ASSERT(iter);
   return (const char *)iter->user_data4;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_GetBson --
 *
 *       Fetches the current BSON document if the iterator is currently
 *       observing a PERFX_BSON_DOCUMENT or a PERFX_BSON_ARRAY.
 *
 * Returns:
 *       A PerfxBson that should be freed with PerfxBson_Free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static PerfxBson *
PerfxBsonIter_GetBson(PerfxBsonIter *iter, /* IN */
                      PerfxBsonType type)  /* IN */
{
   const uint8_t *buffer;
   uint32_t array_len;
   void *endbuf;

   ASSERT(iter);
   ASSERT(iter->user_data1);
   ASSERT(iter->user_data2);
   ASSERT(iter->user_data6);
   ASSERT((type == PERFX_BSON_ARRAY) || (type == PERFX_BSON_DOCUMENT));

   if (ITER_IS_TYPE(iter, type)) {
      endbuf = SIZE_TO_POINTER(POINTER_TO_SIZE(iter->user_data1) +
                               POINTER_TO_SIZE(iter->user_data2));
      if ((iter->user_data6 + 5) > endbuf) {
         return NULL;
      }
      memcpy(&array_len, iter->user_data6, sizeof array_len);
      array_len = UINT32_FROM_LE(array_len);
      if ((iter->user_data6 + array_len) > endbuf) {
         return NULL;
      }
      buffer = iter->user_data6;
      return PerfxBson_CreateFromData(buffer, array_len);
   }

   if (type == PERFX_BSON_ARRAY) {
      LOG("Current key is not an array.");
   } else if (type == PERFX_BSON_DOCUMENT) {
      LOG("Current key is not a document.");
   } else {
      ASSERT_NOT_REACHED();
   }

   return NULL;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_GetArray --
 *
 *       Fetches the current value for the document if it is a
 *       PERFX_BSON_ARRAY.
 *
 * Returns:
 *       A PerfxBson that should be freed with PerfxBson_Free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxBson *
PerfxBsonIter_GetArray(PerfxBsonIter *iter) /* IN */
{
   return PerfxBsonIter_GetBson(iter, PERFX_BSON_ARRAY);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_GetBoolean --
 *
 *       Fetches the current value of the document if it is a
 *       PERFX_BSON_BOOLEAN.
 *
 * Returns:
 *       TRUE or FALSE.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxBsonIter_GetBoolean(PerfxBsonIter *iter) /* IN */
{
   uint8_t b;

   ASSERT(iter);
   ASSERT(iter->user_data6);

   if (ITER_IS_TYPE(iter, PERFX_BSON_BOOLEAN)) {
      memcpy(&b, iter->user_data6, sizeof b);
      return !!b;
   }

   LOG("Current key is not a boolean.");

   return FALSE;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_GetDocument --
 *
 *       Fetches a PerfxBson representing the current value of the document
 *       if it is a PERFX_BSON_DOCUMENT.
 *
 * Returns:
 *       A PerfxBson which should be freed with PerfxBson_Free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxBson *
PerfxBsonIter_GetDocument(PerfxBsonIter *iter) /* IN */
{
   return PerfxBsonIter_GetBson(iter, PERFX_BSON_DOCUMENT);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_GetDouble --
 *
 *       Fetches the current value of the document if it is a
 *       PERFX_BSON_DOUBLE.
 *
 * Returns:
 *       A double.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

double
PerfxBsonIter_GetDouble(PerfxBsonIter *iter) /* IN */
{
   double value;

   ASSERT(iter);
   ASSERT(iter->user_data6);

   if (ITER_IS_TYPE(iter, PERFX_BSON_DOUBLE)) {
      memcpy(&value, iter->user_data6, sizeof value);
      return value;
   }

   LOG("Current value is not a double.");

   return 0.0;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_GetId --
 *
 *       Fetches the current value of the document if it is a
 *       PERFX_BSON_OBJECT_ID.
 *
 * Returns:
 *       A PerfxBsonId.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxBsonId *
PerfxBsonIter_GetId(PerfxBsonIter *iter) /* IN */
{
   ASSERT(iter);
   ASSERT(iter->user_data6);

   if (ITER_IS_TYPE(iter, PERFX_BSON_OBJECT_ID)) {
      return PerfxBsonId_Create((const uint8_t *)iter->user_data6);
   }

   LOG("Current value is not an ObjectId.");

   return NULL;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_GetInt --
 *
 *       Fetches the current value of the document if it is a
 *       PERFX_BSON_INT32.
 *
 * Returns:
 *       An int32_t.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

int32_t
PerfxBsonIter_GetInt(PerfxBsonIter *iter) /* IN */
{
   int32_t value;

   ASSERT(iter);
   ASSERT(iter->user_data6);

   if (ITER_IS_TYPE(iter, PERFX_BSON_INT32)) {
      memcpy(&value, iter->user_data6, sizeof value);
      return UINT32_FROM_LE(value);
   }

   LOG("Current value is not an int32.");

   return 0;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_GetInt64 --
 *
 *       Fetches the current value of the document if it is a
 *       PERFX_BSON_INT64.
 *
 * Returns:
 *       An int64_t.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

int64_t
PerfxBsonIter_GetInt64(PerfxBsonIter *iter) /* IN */
{
   int64_t value;

   ASSERT(iter);
   ASSERT(iter->user_data6);

   if (ITER_IS_TYPE(iter, PERFX_BSON_INT64)) {
      memcpy(&value, iter->user_data6, sizeof value);
      return UINT64_FROM_LE(value);
   }

   LOG("Current value is not an int64.");

   return 0L;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_GetRegex --
 *
 *       Fetches the current value in the document if it is a
 *       PERFX_BSON_REGEX.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxBsonIter_GetRegex(PerfxBsonIter *iter, /* IN */
                       char **regex,        /* IN */
                       char **options)      /* IN */
{
   ASSERT(iter);

   if (regex) *regex = NULL;
   if (options) *options = NULL;

   if (ITER_IS_TYPE(iter, PERFX_BSON_REGEX)) {
      if (regex) *regex = iter->user_data6;
      if (options) *options = iter->user_data7;
      return;
   }

   LOG("Current value is not a Regex.");
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_GetString --
 *
 *       Fetches the current value if it is a PERFX_BSON_UTF8.
 *
 * Returns:
 *       A string which should not be modified or freed.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

const char *
PerfxBsonIter_GetString(PerfxBsonIter *iter, /* IN */
                        size_t *length)      /* OUT */
{
   int32_t real_length;

   ASSERT(iter);
   ASSERT(iter->user_data6);
   ASSERT(iter->user_data7);

   if (ITER_IS_TYPE(iter, PERFX_BSON_UTF8)) {
      if (length) {
         memcpy(&real_length, iter->user_data6, sizeof real_length);
         *length = UINT32_FROM_LE(real_length);
      }
      return iter->user_data7;
   }

   LOG("Current value is not a string.");

   return NULL;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_GetTimeval --
 *
 *       Fetches the current value from the document if it is a
 *       PERFX_BSON_DATE_TIME.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxBsonIter_GetTimeval(PerfxBsonIter *iter,   /* IN */
                         struct timeval *value) /* OUT */
{
   int64_t v_int64;

   ASSERT(iter);
   ASSERT(value);

   if (ITER_IS_TYPE(iter, PERFX_BSON_DATE_TIME)) {
      memcpy(&v_int64, iter->user_data6, sizeof v_int64);
      v_int64 = UINT64_FROM_LE(v_int64);
      value->tv_sec = v_int64 / 1000;
      value->tv_usec = v_int64 % 1000;
      return;
   }

   LOG("Current value is not a DateTime");
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_GetBsonType --
 *
 *       Get the type field currently observed by the iterator.
 *
 * Returns:
 *       A PerfxBsonType.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxBsonType
PerfxBsonIter_GetBsonType(PerfxBsonIter *iter) /* IN */
{
   PerfxBsonType type;

   ASSERT(iter);
   ASSERT(iter->user_data5);

   type = POINTER_TO_INT(iter->user_data5);

   switch (type) {
   case PERFX_BSON_DOUBLE:
   case PERFX_BSON_UTF8:
   case PERFX_BSON_DOCUMENT:
   case PERFX_BSON_ARRAY:
   case PERFX_BSON_UNDEFINED:
   case PERFX_BSON_OBJECT_ID:
   case PERFX_BSON_BOOLEAN:
   case PERFX_BSON_DATE_TIME:
   case PERFX_BSON_NULL:
   case PERFX_BSON_REGEX:
   case PERFX_BSON_INT32:
   case PERFX_BSON_INT64:
      return type;
   default:
      LOG("Unknown BSON type 0x%02x", type);
      return 0;
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_Recurse --
 *
 *       Starts iterating the child document found at the current position
 *       of @iter. @child is initialized to the beginning of the child
 *       document. You will need to call PerfxBsonIter_Next().
 *
 * Returns:
 *       TRUE if successful; otherwise FALSE.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxBsonIter_Recurse(PerfxBsonIter *iter,  /* IN */
                      PerfxBsonIter *child) /* OUT */
{
   int32_t buflen;

   ASSERT(iter);
   ASSERT(child);

   if (ITER_IS_TYPE(iter, PERFX_BSON_ARRAY) ||
       ITER_IS_TYPE(iter, PERFX_BSON_DOCUMENT)) {
      memset(child, 0, sizeof *child);
      memcpy(&buflen, iter->user_data6, sizeof buflen);
      child->user_data1 = iter->user_data6;
      child->user_data2 = INT_TO_POINTER(UINT32_FROM_LE(buflen));
      child->user_data3 = INT_TO_POINTER(3); /* End of size buffer */
      return TRUE;
   }

   LOG("Current value is not a BSON document or array.");

   return FALSE;
}


/*
 *--------------------------------------------------------------------------
 *
 * first_nul --
 *
 *       Fetches the index of the first character with '\0'.
 *
 * Returns:
 *       The index of the first '\0'.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static uint32_t
first_nul(const char *data,   /* IN */
          uint32_t max_bytes) /* IN */
{
   uint32_t i;

   ASSERT(data);
   ASSERT(max_bytes);

   for (i = 0; i < max_bytes; i++) {
      if (!data[i]) {
         return i;
      }
   }

   return 0;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonIter_Next --
 *
 *       Moves the iterator to the next item in the BSON document. If no
 *       more items are left, then FALSE is returned.
 *
 * Returns:
 *       TRUE if successful; otherwise FALSE.
 *
 * Side effects:
 *       Moves the position of the iterator forward.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxBsonIter_Next(PerfxBsonIter *iter) /* IN */
{
   const uint8_t *rawbuf;
   size_t rawbuf_len;
   size_t offset;
   const char *key;
   PerfxBsonType type;
   const uint8_t *value1;
   const uint8_t *value2;
   const char *end = NULL;
   uint32_t max_len;

   ASSERT(iter);

   /*
    * Copy values onto stack from iter.
    */
   rawbuf = iter->user_data1;
   rawbuf_len = POINTER_TO_SIZE(iter->user_data2);
   offset = POINTER_TO_SIZE(iter->user_data3);
   key = (const char *)iter->user_data4;
   type = POINTER_TO_INT(iter->user_data5);
   value1 = (const uint8_t *)iter->user_data6;
   value2 = (const uint8_t *)iter->user_data7;

   /*
    * Check for end of buffer.
    */
   if ((offset + 1) >= rawbuf_len) {
      goto failure;
   }

   /*
    * Get the type of the next field.
    */
   type = rawbuf[++offset];

   /*
    * Get the key of the next field.
    */
   key = (const char *)&rawbuf[++offset];
   max_len = first_nul(key, rawbuf_len - offset - 1);
   if (!iter->user_data8) {
      if (!perfx_utf8_validate(key, max_len, &end)) {
         goto failure;
      }
   }
   offset += strlen(key) + 1;

   switch (type) {
   case PERFX_BSON_UTF8:
      if ((offset + 5) < rawbuf_len) {
         value1 = &rawbuf[offset];
         offset += 4;
         value2 = &rawbuf[offset];
         max_len = UINT32_FROM_LE(*(uint32_t *)value1);
         if ((offset + max_len - 10) < rawbuf_len) {
            if (!iter->user_data8) {
               if (!perfx_utf8_validate((char *)value2, max_len - 1, &end)) {
                  goto failure;
               }
            }
            offset += max_len - 1;
            if (value2[max_len - 1] == '\0') {
               goto success;
            }
         }
      }
      goto failure;
   case PERFX_BSON_DOCUMENT:
   case PERFX_BSON_ARRAY:
      if ((offset + 5) < rawbuf_len) {
         value1 = &rawbuf[offset];
         value2 = NULL;
         memcpy(&max_len, value1, sizeof max_len);
         max_len = UINT32_FROM_LE(max_len);
         if ((offset + max_len) <= rawbuf_len) {
            offset += max_len - 1;
            goto success;
         }
      }
      goto failure;
   case PERFX_BSON_NULL:
   case PERFX_BSON_UNDEFINED:
      value1 = NULL;
      value2 = NULL;
      offset--;
      goto success;
   case PERFX_BSON_OBJECT_ID:
      if ((offset + 12) < rawbuf_len) {
         value1 = &rawbuf[offset];
         value2 = NULL;
         offset += 11;
         goto success;
      }
      goto failure;
   case PERFX_BSON_BOOLEAN:
      if ((offset + 1) < rawbuf_len) {
         value1 = &rawbuf[offset];
         value2 = NULL;
         goto success;
      }
      goto failure;
   case PERFX_BSON_DATE_TIME:
   case PERFX_BSON_DOUBLE:
   case PERFX_BSON_INT64:
      if ((offset + 8) < rawbuf_len) {
         value1 = &rawbuf[offset];
         value2 = NULL;
         offset += 7;
         goto success;
      }
      goto failure;
   case PERFX_BSON_REGEX:
      value1 = &rawbuf[offset];
      max_len = first_nul((char *)value1, rawbuf_len - offset - 1);
      if (!iter->user_data8) {
         if (!perfx_utf8_validate((char *)value1, max_len, &end)) {
            goto failure;
         }
      }
      offset += max_len + 1;
      if ((offset + 1) >= rawbuf_len) {
         goto failure;
      }
      value2 = &rawbuf[offset];
      max_len = first_nul((char *)value2, rawbuf_len - offset - 1);
      if (!iter->user_data8) {
         if (!perfx_utf8_validate((char *)value2, max_len, &end)) {
            goto failure;
         }
      }
      offset += max_len + 1;
      goto success;
   case PERFX_BSON_INT32:
      if ((offset + 4) < rawbuf_len) {
         value1 = &rawbuf[offset];
         value2 = NULL;
         offset += 3;
         goto success;
      }
      goto failure;
   default:
      goto failure;
   }

success:
   iter->user_data3 = SIZE_TO_POINTER(offset);
   iter->user_data4 = (void *)key;
   iter->user_data5 = INT_TO_POINTER(type);
   iter->user_data6 = (void *)value1;
   iter->user_data7 = (void *)value2;
   return TRUE;

failure:
   memset(iter, 0, sizeof *iter);
   return FALSE;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBson_ToString --
 *
 *       Converts a PerfxBson structure to a string using the format used
 *       by the mongo shell.
 *
 * Returns:
 *       A newly allocated string that should be freed with perfx_free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

char *
PerfxBson_ToString(PerfxBson *bson, /* IN */
                   bool is_array)   /* IN */
{
   PerfxBsonIter iter;
   PerfxBsonType type;
   PerfxString *str;

   ASSERT(bson);

   str = PerfxString_Create(is_array ? "[" : "{");

   PerfxBsonIter_Init(&iter, bson);
   if (PerfxBsonIter_Next(&iter)) {
again:
      if (!is_array) {
         PerfxString_AppendPrintf(str, "\"%s\": ",
                                  PerfxBsonIter_GetKey(&iter));
      }
      type = PerfxBsonIter_GetBsonType(&iter);
      switch (type) {
      case PERFX_BSON_DOUBLE:
         PerfxString_AppendPrintf(str, "%f",
                                  PerfxBsonIter_GetDouble(&iter));
         break;
      case PERFX_BSON_DATE_TIME:
         PerfxString_Append(str, "ISODate()");
         break;
      case PERFX_BSON_INT32:
         PerfxString_AppendPrintf(str, "NumberLong(%d)",
                                  PerfxBsonIter_GetInt(&iter));
         break;
      case PERFX_BSON_INT64:
         PerfxString_AppendPrintf(str, "NumberLong(%"PRIi64")",
                                  PerfxBsonIter_GetInt64(&iter));
         break;
      case PERFX_BSON_UTF8:
         PerfxString_Append(str, PerfxBsonIter_GetString(&iter, NULL));
         break;
      case PERFX_BSON_ARRAY:
         {
            PerfxBson *child;
            char *childstr;

            if ((child = PerfxBsonIter_GetArray(&iter))) {
               childstr = PerfxBson_ToString(child, TRUE);
               PerfxString_Append(str, childstr);
               PerfxBson_Free(child);
               perfx_free(childstr);
            }
         }
         break;
      case PERFX_BSON_DOCUMENT:
         {
            PerfxBson *child;
            char *childstr;

            if ((child = PerfxBsonIter_GetDocument(&iter))) {
               childstr = PerfxBson_ToString(child, FALSE);
               PerfxString_Append(str, childstr);
               PerfxBson_Free(child);
               perfx_free(childstr);
            }
         }
         break;
      case PERFX_BSON_BOOLEAN:
         PerfxString_AppendPrintf(str,
            PerfxBsonIter_GetBoolean(&iter) ? "true" : "false");
         break;
      case PERFX_BSON_OBJECT_ID:
         {
            PerfxBsonId *id;
            char *idstr;

            id = PerfxBsonIter_GetId(&iter);
            idstr = PerfxBsonId_ToString(id);
            PerfxString_AppendPrintf(str, "ObjectId(\"%s\")", idstr);
            PerfxBsonId_Free(id);
            perfx_free(idstr);
         }
         break;
      case PERFX_BSON_NULL:
         PerfxString_Append(str, "null");
         break;
      case PERFX_BSON_REGEX:
         /* TODO: */
         ASSERT_NOT_REACHED();
         break;
      case PERFX_BSON_UNDEFINED:
         PerfxString_Append(str, "undefined");
         break;
      default:
         ASSERT_NOT_REACHED();
      }

      if (PerfxBsonIter_Next(&iter)) {
         PerfxString_Append(str, ", ");
         goto again;
      }
   }

   PerfxString_Append(str, is_array ? "]" : "}");

   return PerfxString_Free(str, FALSE);
}


PerfxBsonId *
PerfxBsonId_Create(const uint8_t *bytes) /* IN */
{
   uint8_t *id;

   ASSERT(bytes);

   id = perfx_malloc(12);
   memcpy(id, bytes, 12);
   return (PerfxBsonId *)id;
}


void
PerfxBsonId_Free(PerfxBsonId *id) /* IN */
{
   perfx_free(id);
}


char *
PerfxBsonId_ToString(const PerfxBsonId *id) /* IN */
{
   const uint8_t *bytes = (uint8_t *)id;
   PerfxString *str;
   uint32_t i;

   ASSERT(id);

   str = PerfxString_Create(NULL);
   for (i = 0; i < 12; i++) {
      PerfxString_AppendPrintf(str, "%02x", bytes[i]);
   }

   return PerfxString_Free(str, FALSE);
}
