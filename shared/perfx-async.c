/* perfx-async.c
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "perfx-async.h"
#include "perfx-atomic.h"
#include "perfx-debug.h"
#include "perfx-main.h"
#include "perfx-memory.h"


/*
 *--------------------------------------------------------------------------
 *
 * PerfxAsyncResult_Create --
 *
 *       Create a new asynchronous result structure.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxAsyncResult *
PerfxAsyncResult_Create(void *source,                /* IN */
                        PerfxAsyncCallback callback, /* IN */
                        void *user_data)             /* IN */
{
   PerfxAsyncResult *res;

   res = perfx_new0(PerfxAsyncResult, 1);
   res->source = source;
   res->callback = callback;
   res->callback_data = user_data;
   res->ref_count = 1;

   return res;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxAsyncResult_Ref --
 *
 *       Increment the reference count of @result by one.
 *
 * Returns:
 *       @result.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxAsyncResult *
PerfxAsyncResult_Ref(PerfxAsyncResult *result) /* IN */
{
   ASSERT(result);
   ASSERT_INT(result->ref_count, >, 0);

   PerfxAtomicInt_Increment(&result->ref_count);
   return result;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxAsyncResult_Unref --
 *
 *       Unrefs the async structure. When the reference count reaches zero,
 *       the structure will be freed.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxAsyncResult_Unref(PerfxAsyncResult *result) /* IN */
{
   ASSERT(result);
   ASSERT_INT(result->ref_count, >, 0);

   if (PerfxAtomicInt_DecrementAndTest(&result->ref_count)) {
      PerfxValue_Unset(&result->value);
      perfx_free(result);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxAsyncResult_Callback --
 *
 *       Synchronously call the callback for the async structure.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxAsyncResult_Callback(PerfxAsyncResult *result) /* IN */
{
   ASSERT(result);
   ASSERT(result->callback);

   result->callback(result->source, result, result->callback_data);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxAsyncResult_CallbackIdleCb --
 *
 *       Main loop idle callback to execute the async results callback.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxAsyncResult_CallbackIdleCb(void *data) /* IN */
{
   PerfxAsyncResult *result = data;

   ASSERT(result);

   PerfxAsyncResult_Callback(result);
   PerfxAsyncResult_Unref(result);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxAsyncResult_CallbackIdle --
 *
 *       Queue the callback of the async result to happen on the main loop.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxAsyncResult_CallbackIdle(PerfxAsyncResult *result) /* IN */
{
   ASSERT(result);

   PerfxAsyncResult_Ref(result);
   PerfxMain_AddIdle(PerfxAsyncResult_CallbackIdleCb, result);
}
