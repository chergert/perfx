/* perfx-protocol.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_PROTOCOL_H
#define PERFX_PROTOCOL_H


#include "perfx-array.h"
#include "perfx-buffer.h"
#include "perfx-connection.h"
#include "perfx-macros.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


#define PERFX_TYPE_PROTOCOL         (PerfxProtocol_GetType())
#define PERFX_PROTOCOL(o)           (PERFX_OBJECT_CAST(o, PerfxProtocol, PERFX_TYPE_PROTOCOL))
#define PERFX_PROTOCOL_GET_CLASS(o) ((PerfxProtocolClass *)PERFX_OBJECT_GET_CLASS(o))


typedef struct {
   PerfxObject parent;

   /*< private >*/
   PerfxConnection *connection;
} PerfxProtocol;


typedef struct {
   PerfxClass parent;

   void (*ConnectionLost) (PerfxProtocol *protocol,
                           uint32_t reason);
   void (*ConnectionMade) (PerfxProtocol *protocol,
                           PerfxConnection *connection);
   void (*DataReceived)   (PerfxProtocol *protocol,
                           PerfxBuffer *buffer);
} PerfxProtocolClass;


void      PerfxProtocol_ConnectionLost (PerfxProtocol *protocol);
void      PerfxProtocol_ConnectionMade (PerfxProtocol *protocol,
                                        PerfxConnection *connection);
PerfxType PerfxProtocol_GetType        (void) PERFX_GNUC_CONST;
void      PerfxProtocol_RecvData       (PerfxProtocol *protocol,
                                        PerfxBuffer *buffer);
void      PerfxProtocol_SendData       (PerfxProtocol *protocol,
                                        PerfxBuffer *buffer);


PERFX_END_DECLS


#endif /* PERFX_PROTOCOL_H */
