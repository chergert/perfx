/* perfx-clock.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#if defined(__APPLE__)
#include <mach/mach_time.h>
#include <sys/time.h>
#endif

#include "perfx-clock.h"
#include "perfx-debug.h"


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClock_Now --
 *
 *       Get the current time of day and store it in @ts.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxClock_Now(struct timespec *ts) /* OUT */
{
   ASSERT(ts);

#if defined(__linux__)
   clock_gettime(CLOCK_REALTIME, ts);
#elif defined(__APPLE__)
   {
      struct timeval t = { 0 };
      gettimeofday(&t, NULL);
      ts->tv_sec = t.tv_sec;
      ts->tv_nsec = t.tv_usec * 1000;
   }
#else
#error "You're platform is not supported."
#endif
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClock_GetMonotonic --
 *
 *       Retrieve a time representing the monotonic clock.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       ts is set.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxClock_GetMonotonic(struct timespec *ts) /* OUT */
{
#if defined(__APPLE__)
   static mach_timebase_info_data_t info = { 0 };
   static double ratio;
   uint64_t atime;

   if (!info.denom) {
      mach_timebase_info(&info);
      ratio = info.numer / info.denom;
   }

   atime = mach_absolute_time() * ratio;
   ts->tv_sec = atime * 1e-9;
   ts->tv_nsec = atime - (ts->tv_sec * 1e9);
#elif defined(__linux__)
   clock_gettime(CLOCK_MONOTONIC, ts);
#else
#error "Your platform is not yet supported."
#endif
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClock_Subtract --
 *
 *       Subtract y from x. Store the result in z.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       z is set to the difference of (x - y).
 *
 *--------------------------------------------------------------------------
 */

void
PerfxClock_Subtract(struct timespec *x, /* IN */
                    struct timespec *y, /* IN */
                    struct timespec *z) /* OUT */
{
   struct timespec r;

   ASSERT(x);
   ASSERT(y);

   r.tv_sec = (x->tv_sec - y->tv_sec);
   if ((r.tv_nsec = (x->tv_nsec - y->tv_nsec)) < 0) {
      r.tv_nsec += 1000000000;
      r.tv_sec -= 1;
   }

   if (z) {
      *z = r;
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClock_Compare --
 *
 *       Compares the difference between ts1 and ts2. The result is a
 *       qsort() style value.
 *
 * Returns:
 *       -1 if ts1 is less than ts2.
 *        0 if ts1 is equal to ts2.
 *        1 if ts1 is greater than ts2.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

int32_t
PerfxClock_Compare(const struct timespec *ts1, /* IN */
                   const struct timespec *ts2) /* IN */
{
   ASSERT(ts1);
   ASSERT(ts2);

   if ((ts1->tv_sec == ts2->tv_sec) &&
       (ts1->tv_nsec == ts2->tv_nsec)) {
      return 0;
   } else if ((ts1->tv_sec > ts2->tv_sec) ||
              ((ts1->tv_sec == ts2->tv_sec) &&
               (ts1->tv_nsec > ts2->tv_nsec))) {
      return 1;
   } else {
      return -1;
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClock_CompareMsec --
 *
 *       Compare two timespecs, but drop all precision beyond milliseconds.
 *
 * Returns:
 *       A qsort style compare value (-1, 0, 1).
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

int32_t
PerfxClock_CompareMsec(const struct timespec *ts1, /* IN */
                       const struct timespec *ts2) /* IN */
{
   struct timespec t1;
   struct timespec t2;
   int32_t ret;

   ASSERT(ts1);
   ASSERT(ts2);

   t1.tv_sec = ts1->tv_sec;
   t1.tv_nsec = (ts1->tv_nsec / 1000000) * 1000000;

   t2.tv_sec = ts2->tv_sec;
   t2.tv_nsec = (ts2->tv_nsec / 1000000) * 1000000;

   ret = PerfxClock_Compare(&t1, &t2);

   return ret;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxClock_AddMsec --
 *
 *       Adds msec milliseconds to the timespec.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       ts is modified.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxClock_AddMsec(struct timespec *ts, /* OUT */
                   uint32_t msec)       /* IN */
{
   ASSERT(ts);

   ts->tv_nsec += (msec * 1000000);
   if (ts->tv_nsec > 1000000000) {
      ts->tv_sec++;
      ts->tv_nsec -= 1000000000;
   }
}
