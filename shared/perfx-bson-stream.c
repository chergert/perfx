/* perfx-bson-stream.c
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <string.h>

#include "perfx-bson-stream.h"
#include "perfx-debug.h"
#include "perfx-memory.h"

#define BUFFER_SIZE (1024 * 1024 * 16)

struct _PerfxBsonStream
{
   int fd;
   size_t end;
   size_t len;
   size_t offset;
   uint8_t *data;
};


static void
PerfxBsonStream_FillBuffer(PerfxBsonStream *stream) /* IN */
{
   ssize_t ret;

   ASSERT(stream);
   ASSERT(stream->fd >= 0);

   /*
    * Handle first read specially.
    */
   if ((stream->fd >= 0) && (!stream->offset) && (!stream->end)) {
      ret = read(stream->fd, &stream->data[0], stream->len);
      if (ret <= 0) {
         stream->fd = -1;
         return;
      }
      stream->end = ret;
      return;
   }

   /*
    * Move valid data to head.
    */
   memmove(&stream->data[0],
           &stream->data[stream->offset],
           stream->end - stream->offset);
   stream->end = stream->end - stream->offset;
   stream->offset = 0;

   /*
    * Read in data to fill the buffer.
    */
   ret = read(stream->fd,
              &stream->data[stream->end],
              stream->len - stream->end);
   stream->end += ret;

   ASSERT(stream->offset == 0);
   ASSERT(stream->end <= stream->len);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonStream_CreateUnix --
 *
 *       Create a PerfxBsonStream using a unix style file-descriptor
 *       as the input source.
 *
 * Returns:
 *       A PerfxBsonStream* that should be freed with
 *       PerfxBsonStream_Destroy().
 *
 * Side effects:
 *       fd will be owned by the PerfxBsonStream.
 *
 *--------------------------------------------------------------------------
 */

PerfxBsonStream *
PerfxBsonStream_CreateUnix(int fd) /* IN */
{
   PerfxBsonStream *stream;

   stream = perfx_new0(PerfxBsonStream, 1);
   stream->data = perfx_malloc0(BUFFER_SIZE);
   stream->fd = fd;
   stream->len = BUFFER_SIZE;
   stream->offset = 0;

   PerfxBsonStream_FillBuffer(stream);

   return stream;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonStream_Next --
 *
 *       Read the next PerfxBson from the underlying file.
 *
 * Returns:
 *       A PerfxBson* that should be freed with PerfxBson_Free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxBson *
PerfxBsonStream_Next(PerfxBsonStream *stream) /* IN */
{
   PerfxBson *bson;
   uint32_t blen;

   ASSERT(stream);

   while (stream->fd >= 0) {
      if ((stream->end - stream->offset) < 4) {
         PerfxBsonStream_FillBuffer(stream);
         continue;
      }

      blen = UINT32_FROM_LE(*(uint32_t *)&stream->data[stream->offset]);
      if (blen > (stream->end - stream->offset)) {
         PerfxBsonStream_FillBuffer(stream);
         continue;
      }

      bson = PerfxBson_CreateFromData(&stream->data[stream->offset], blen);
      stream->offset += blen;
      return bson;
   }

   return NULL;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxBsonStream_Destroy --
 *
 *       Frees the resources associated with stream.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxBsonStream_Destroy(PerfxBsonStream *stream) /* IN */
{
   perfx_free(stream->data);
   perfx_free(stream);
}
