/* perfx-listener.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <arpa/inet.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#include "perfx-connection.h"
#include "perfx-debug.h"
#include "perfx-listener.h"
#include "perfx-main.h"
#include "perfx-protocol.h"


#define DEFAULT_BACKLOG 10


PerfxListener *
PerfxListener_CreateTcp(struct in_addr *addr, /* IN/OPT */
                        uint16_t port)        /* IN */
{
   PerfxListener *listener;
   struct sockaddr_in saddr;
   int sd;

   ENTRY;

   ASSERT(port);

   /*
    * Create our TCP socket.
    */
   sd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
   if (-1 == sd) {
      LOG("Failed to create tcp socket.");
      RETURN(NULL);
   }

   /*
    * Setup the address for listening.
    */
   memset(&saddr, 0, sizeof saddr);
   saddr.sin_family = AF_INET;
   saddr.sin_port = htons(port);
   if (addr) {
      saddr.sin_addr = *addr;
   } else {
      inet_pton(AF_INET, "0.0.0.0", &(saddr.sin_addr.s_addr));
   }

   /*
    * Bind the socket so we can enable listening later.
    */
   if (-1 == bind(sd, (struct sockaddr *)&saddr, sizeof saddr)) {
      close(sd);
      LOG("Failed to bind tcp socket.");
      RETURN(NULL);
   }

   listener = PerfxObject_Create(PERFX_TYPE_LISTENER);
   listener->fd = sd;
   listener->backlog = DEFAULT_BACKLOG;

   RETURN(listener);
}


static void
PerfxListener_PollCb(int fd,                   /* IN */
                     PerfxMainPollFlags flags, /* IN */
                     void *data)               /* IN */
{
   PerfxListener *listener = data;
   struct sockaddr_in saddr;
   socklen_t saddr_len = sizeof saddr;
   PerfxConnection *connection;
   int sd;

   ASSERT(fd >= 0);
   ASSERT(flags & PERFX_MAIN_POLL_IN);
   ASSERT(listener);
   ASSERT(listener->fd);

   sd = accept(listener->fd, (struct sockaddr *)&saddr, &saddr_len);
   if (-1 == sd) {
      LOG("Failed to accept client connection.");
      return;
   }

   if (!listener->protocol_type) {
      shutdown(sd, SHUT_RDWR);
      close(sd);
      return;
   }

   connection = PerfxConnection_CreateFd(sd);
   PerfxConnection_SetProtocolType(connection, listener->protocol_type);
   PerfxConnection_Connect(connection);

   TODO("How should we manager lifecycle of protocol?");

   PerfxObject_Unref(connection);
}


void
PerfxListener_Start(PerfxListener *listener) /* IN */
{
   ENTRY;

   ASSERT(listener);
   ASSERT(listener->fd >= 0);
   ASSERT(listener->protocol_type);

   /*
    * Enable listening on the socket.
    */
   if (-1 == listen(listener->fd, listener->backlog)) {
      LOG("Failed to enable listening on socket.");
      close(listener->fd);
      listener->fd = -1;
      EXIT;
   }

   /*
    * Enable non-blocking IO so we can use poll to signal when there are
    * connections awaiting an accept().
    */
   fcntl(listener->fd, F_SETFL, O_NONBLOCK);
   listener->handler_id =
      PerfxMain_AddPoll(listener->fd, PERFX_MAIN_POLL_IN,
                        PerfxListener_PollCb, listener);

   EXIT;
}


void
PerfxListener_Stop(PerfxListener *listener) /* IN */
{
   ASSERT(listener);

   if (listener->fd) {
      close(listener->fd);
      listener->fd = 0;
   }
}


void
PerfxListener_SetProtocolType(PerfxListener *listener, /* IN */
                              PerfxType protocol_type) /* IN */
{
   ASSERT(listener);
   ASSERT(PerfxType_IsA(protocol_type, PERFX_TYPE_PROTOCOL));

   listener->protocol_type = protocol_type;
}


PerfxType
PerfxListener_GetType(void)
{
   static PerfxType type = 0;

   if (!type) {
      type = PerfxClass_Register("PerfxListener", 0,
                                 sizeof(PerfxListenerClass),
                                 sizeof(PerfxListener),
                                 TRUE);
   }

   return type;
}
