/* perfx-line-protocol.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <string.h>

#include "perfx-array.h"
#include "perfx-debug.h"
#include "perfx-line-protocol.h"
#include "perfx-memory.h"
#include "perfx-types.h"


/*
 *--------------------------------------------------------------------------
 *
 * PerfxLineProtocol_LineReceived --
 *
 *       Call the LineReceived virtual method.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxLineProtocol_LineReceived(PerfxLineProtocol *protocol, /* IN */
                               const char *line,            /* IN */
                               size_t len)                  /* IN */
{
   PerfxLineProtocolClass *class;

   ENTRY;

   ASSERT(protocol);
   ASSERT(line);
   ASSERT(len);

   class = PERFX_LINE_PROTOCOL_GET_CLASS(protocol);
   if (class->LineReceived) {
      class->LineReceived(protocol, line, len);
   }

   EXIT;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxLineProtocol_DataReceived --
 *
 *       Handle data received from the peer. For each line, dispatch
 *       to PerfxLineProtocol_LineReceived().
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       buffer content may be modified as we parse it to save a copy
 *       of the buffer.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxLineProtocol_DataReceived(PerfxProtocol *protocol, /* IN */
                               PerfxBuffer *buffer)     /* IN */
{
   PerfxLineProtocol *line_proto = (PerfxLineProtocol *)protocol;
   const char *line;
   uint32_t last = 0;
   uint32_t i;

   ASSERT(protocol);
   ASSERT(buffer);

   ENTRY;


   line = (const char *)buffer->data;

   TODO("Use encoding like utf-8 to iterate buffer, save leftovers");

   for (i = 0; i < buffer->len; i++) {
      if (buffer->data[i] == '\n') {
         buffer->data[i] = '\0';
         PerfxLineProtocol_LineReceived(line_proto, line, i - last);
         line = (const char *)&buffer->data[i + 1];
         last = i;
      }
   }

   EXIT;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxLineProtocol_SendLine --
 *
 *       Sends a line to the peer via the protocols transport. An end-line
 *       will be sent in addition to the content of the line passed in. If
 *       len is -1, then strlen is used to determine the length of the
 *       string.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxLineProtocol_SendLine(PerfxLineProtocol *proto, /* IN */
                           const char *line,         /* IN */
                           ssize_t len)              /* IN */
{
   static const char eol[] = "\n";
   PerfxProtocol *protocol = (PerfxProtocol *)proto;
   PerfxBuffer *buffer;
   PerfxArray *array;
   uint8_t *data;
   size_t datalen;

   ASSERT(proto);
   ASSERT(protocol);
   ASSERT(protocol->connection);
   ASSERT(line);

   /*
    * This should only support UTF-8. We need an UTF-8 validator.
    */

   TODO("Validate encoded line content. UTF-8 for example.");

   if (len < 0) {
      len = strlen(line);
   }

   array = PerfxArray_CreateSized(FALSE, sizeof(char), len + sizeof eol);
   PerfxArray_AppendRange(array, len, (void *)line);
   PerfxArray_AppendRange(array, sizeof eol, (void *)eol);
   data = PerfxArray_StealData(array, &datalen);
   PerfxArray_Free(array);

   buffer = PerfxBuffer_Create(data, datalen, perfx_free);
   PerfxProtocol_SendData(protocol, buffer);
   PerfxBuffer_Unref(buffer);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxLineProtocol_GetType --
 *
 *       Retrieve the PerfxType for the Line Protocol class.
 *
 * Returns:
 *       A PerfxType.
 *
 * Side effects:
 *       Type may be registered.
 *
 *--------------------------------------------------------------------------
 */

PerfxType
PerfxLineProtocol_GetType(void)
{
   PerfxProtocolClass *class;
   static PerfxType type = 0;

   if (!type) {
      type = PerfxClass_Register("PerfxLineProtocol",
                                 PERFX_TYPE_PROTOCOL,
                                 sizeof(PerfxLineProtocolClass),
                                 sizeof(PerfxLineProtocol),
                                 TRUE);
      class = (PerfxProtocolClass *)PerfxType_GetClass(type);
      class->DataReceived = PerfxLineProtocol_DataReceived;
   }

   return type;
}
