/* perfx-tree.h
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef PERFX_TREE_H
#define PERFX_TREE_H


#include "perfx-macros.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


typedef struct _PerfxTree     PerfxTree;
typedef struct _PerfxTreeNode PerfxTreeNode;


struct _PerfxTree
{
   int32_t size;
   PerfxCompareFunc key_compare;
   PerfxFreeFunc key_destroy;
   PerfxFreeFunc value_destroy;
   PerfxTreeNode *root;
};


struct _PerfxTreeNode
{
   void *key;
   void *value;
   bool hidden;
   int8_t factor;

   PerfxTreeNode *left;
   PerfxTreeNode *right;
};


void  PerfxTree_Destroy (PerfxTree *tree);
void  PerfxTree_Init    (PerfxTree *tree,
                         PerfxCompareFunc key_compare,
                         PerfxFreeFunc key_destroy,
                         PerfxFreeFunc value_destroy);
void  PerfxTree_Insert  (PerfxTree *tree,
                         void *key,
                         void *value);
void *PerfxTree_Lookup  (PerfxTree *tree,
                         const void *key);
void  PerfxTree_Remove  (PerfxTree *tree,
                         const void *key);


PERFX_END_DECLS


#endif /* PERFX_TREE_H */
