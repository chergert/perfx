/* perfx-list.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include "perfx-list.h"
#include "perfx-memory.h"


/*
 *--------------------------------------------------------------------------
 *
 * PerfxList_Length --
 *
 *       Retrieves the length of the list.
 *
 * Returns:
 *       The list length.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

uint32_t
PerfxList_Length(PerfxList *list) /* IN */
{
   uint32_t i = 0;
   for (i = 0; list; list = list->next) {
      i++;
   }
   return i;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxList_Append --
 *
 *       Appends an item to the linked list.
 *
 * Returns:
 *       The modified linked list.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxList *
PerfxList_Append(PerfxList *list, /* IN */
                 void *data)      /* IN */
{
   PerfxList *item;
   PerfxList *ret = list;

   item = perfx_new0(PerfxList, 1);
   item->data = data;

   while (list) {
      if (!list->next) {
         list->next = item;
         break;
      }
      list = list->next;
   }

   return ret ? ret : item;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxList_Prepend --
 *
 *       Prepends an item to the linked list.
 *
 * Returns:
 *       The modified linked list.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxList *
PerfxList_Prepend(PerfxList *list, /* IN */
                  void *data)      /* IN */
{
   PerfxList *ret;

   ret = perfx_new0(PerfxList, 1);
   ret->data = data;
   ret->next = list;

   return ret;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxList_Remove --
 *
 *       Remove an item from the linked list.
 *
 * Returns:
 *       The modified linked list.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxList *
PerfxList_Remove(PerfxList *list, /* IN */
                 void *data)      /* IN */
{
   PerfxList *last = NULL;
   PerfxList *ret = list;

   while (list) {
      if (list->data == data) {
         if (last) {
            last->next = list->next;
         } else {
            ret = list->next;
         }
         perfx_free(list);
         break;
      }
      last = list;
      list = list->next;
   }

   return ret;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxList_Free --
 *
 *       Frees all the links of the list.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       All links in the list are freed.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxList_Free(PerfxList *list) /* IN */
{
   PerfxList *removed;

   while ((removed = list)) {
      list = list->next;
      perfx_free(removed);
   }
}
