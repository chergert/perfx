/* perfx-tree-path.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_TREE_PATH_H
#define PERFX_TREE_PATH_H


#include "perfx-macros.h"
#include "perfx-quark.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


typedef struct {
   uint32_t len;
   PerfxQuark *indexes;
   char **names;
} PerfxTreePath;


PerfxTreePath *PerfxTreePath_Create    (const char *path);
PerfxTreePath *PerfxTreePath_CreateV   (const char **path);
void           PerfxTreePath_Free      (PerfxTreePath *path);
int            PerfxTreePath_Compare   (const PerfxTreePath *path1,
                                        const PerfxTreePath *path2);
bool           PerfxTreePath_HasPrefix (const PerfxTreePath *path,
                                        const PerfxTreePath *prefix);
bool           PerfxTreePath_HasSuffix (const PerfxTreePath *path,
                                        const PerfxTreePath *suffix);


PERFX_END_DECLS


#endif /* PERFX_TREE_PATH_H */
