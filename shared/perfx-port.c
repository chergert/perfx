/* perfx-port.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <errno.h>
#include <pthread.h>
#include <string.h>

#include "perfx-array.h"
#include "perfx-atomic.h"
#include "perfx-cpu.h"
#include "perfx-debug.h"
#include "perfx-memory.h"
#include "perfx-port.h"


struct _PerfxPort
{
   pthread_mutex_t mutex;
   pthread_cond_t cond;

   PerfxPortHandler handler;
   void *handler_data;

   PerfxMessage *head;
   PerfxMessage *tail;

   int32_t active;
   int32_t max_active;
   bool shutdown;
};


typedef struct
{
   pthread_t thread;
   uint32_t idx;
   pthread_mutex_t mutex;
   pthread_cond_t cond;
   PerfxMessage *head;
   PerfxMessage *tail;
} PerfxPortDispatcher;


/*
 * Globals.
 */
static PerfxPortDispatcher  gPortDispatcherShared;
static PerfxPortDispatcher *gPortDispatchers;
static pthread_key_t        gPortDispatchersCurrentKey;
static uint32_t             gPortDispatchersLen;


/*
 * Forward declarations.
 */
static void *PerfxPortDispatcher_Thread(void *) __attribute__((noreturn));
static void  PerfxPort_HandleMessage(PerfxPort *, PerfxMessage *);


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPortDispatcher_Push --
 *
 *       Push a message onto the end of the queue of work items for
 *       the port.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxPortDispatcher_Push(PerfxPortDispatcher *dispatcher, /* IN */
                         PerfxMessage *message)           /* IN */
{
   ASSERT(dispatcher);
   ASSERT(message);

   message->next = NULL;

   pthread_mutex_lock(&dispatcher->mutex);
   if (dispatcher->tail) {
      dispatcher->tail->next = message;
   }
   dispatcher->tail = message;
   if (!dispatcher->head) {
      dispatcher->head = message;
   }
   pthread_cond_signal(&dispatcher->cond);
   pthread_mutex_unlock(&dispatcher->mutex);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPortDispatcher_PopLocked --
 *
 *       Remove an item from the queue of a dispatcher. The caller is
 *       expected to hold the lock for the dispatcher.
 *
 * Returns:
 *       A PerfxMessage.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static PerfxMessage *
PerfxPortDispatcher_PopLocked(PerfxPortDispatcher *dispatcher, /* IN */
                              bool block)                      /* IN */
{
   PerfxMessage *message;

   ASSERT(dispatcher);

   while (!(message = dispatcher->head) && block) {
      pthread_cond_wait(&dispatcher->cond, &dispatcher->mutex);
   }

   if (message) {
      dispatcher->head = message->next;
      if (dispatcher->tail == message) {
         dispatcher->tail = NULL;
      }
   }

   return message;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPortDispatcher_Pop --
 *
 *       Remove a message from the head of the dispatcher queue.
 *
 * Returns:
 *       A PerfxMessage* or NULL.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static PerfxMessage *
PerfxPortDispatcher_Pop(PerfxPortDispatcher *dispatcher) /* IN */
{
   PerfxMessage *message = NULL;
   uint32_t idx;
   uint32_t i;
   bool is_shared;

   ASSERT(dispatcher);

   is_shared = (dispatcher == &gPortDispatcherShared);
   idx = dispatcher->idx;

   /*
    * Dequeuing a message happens in a certain order. This is done to help
    * prevent priority inversion as well as letting dispatchers help get
    * cache locality.
    *
    * 1) Check our local queue for a message.
    * 2) If we have no more items, check non-blocking from main queue
    *    for the next item.
    * 3) If the main queue is also empty, look at our neighbors starting
    *    from our index + 1.
    * 4) If our neighbors have no work, block on waiting for a message
    *    from the shared queue.
    *
    * The reason we first check the shared queue in a non-blocking fashion
    * before we check our neighbors is so that we don't kill the potential
    * for cache-locality by our neighbors. Additionally, it helps us from
    * getting priority inverted because of message handlers posting more
    * messages, thus starving messages from other ports sitting in the
    * shared queue.
    */

   if (!is_shared) {
      pthread_mutex_lock(&dispatcher->mutex);
      message = PerfxPortDispatcher_PopLocked(dispatcher, FALSE);
      pthread_mutex_unlock(&dispatcher->mutex);
      if (message) {
         return message;
      }

      pthread_mutex_lock(&gPortDispatcherShared.mutex);
      message = PerfxPortDispatcher_PopLocked(&gPortDispatcherShared, FALSE);
      pthread_mutex_unlock(&gPortDispatcherShared.mutex);
      if (message) {
         return message;
      }

      for (i = 1; i < gPortDispatchersLen; i++) {
         dispatcher = &gPortDispatchers[(idx + i) % gPortDispatchersLen];
         pthread_mutex_lock(&dispatcher->mutex);
         message = PerfxPortDispatcher_PopLocked(dispatcher, FALSE);
         pthread_mutex_unlock(&dispatcher->mutex);
         if (message) {
            return message;
         }
      }
   }

   pthread_mutex_lock(&gPortDispatcherShared.mutex);
   message = PerfxPortDispatcher_PopLocked(&gPortDispatcherShared, TRUE);
   pthread_mutex_unlock(&gPortDispatcherShared.mutex);

   return message;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPortDispatcher_GetCurrent --
 *
 *       Fetch PerfxPortDispatcher used by the current thread.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxPortDispatcher *
PerfxPortDispatcher_GetCurrent(void)
{
   return pthread_getspecific(gPortDispatchersCurrentKey);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPortDispatcher_Thread --
 *
 *       Thread worker for dispatching messages for a given port.
 *
 * Returns:
 *       NULL, after message queue has been processed.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void *
PerfxPortDispatcher_Thread(void *data) /* IN */
{
   PerfxPortDispatcher *dispatcher = data;
   PerfxMessage *message;

   ASSERT(dispatcher);

   pthread_setspecific(gPortDispatchersCurrentKey, dispatcher);

   while ((message = PerfxPortDispatcher_Pop(dispatcher))) {
      ASSERT(message->port);
      PerfxPort_HandleMessage(message->port, message);
   }

   pthread_setspecific(gPortDispatchersCurrentKey, NULL);

   ASSERT_NOT_REACHED();
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPortDispatcher_Init --
 *
 *       Initializes a port dispatcher.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxPortDispatcher_Init(PerfxPortDispatcher *dispatcher, /* IN */
                         uint32_t idx)                    /* IN */
{
   ASSERT(dispatcher);

   dispatcher->idx = idx;
   pthread_mutex_init(&dispatcher->mutex, NULL);
   pthread_cond_init(&dispatcher->cond, NULL);
   pthread_create(&dispatcher->thread, NULL,
                  PerfxPortDispatcher_Thread, dispatcher);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPort_InitOnce --
 *
 *       Initializes the port dispatchers which will process incoming
 *       messages for various ports.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxPort_InitOnce(void)
{
   uint32_t i;

   ASSERT(!gPortDispatchers);
   ASSERT(!gPortDispatchersLen);

   pthread_key_create(&gPortDispatchersCurrentKey, NULL);

   gPortDispatchersLen = PerfxCpu_GetCount();
   gPortDispatchers = perfx_new0(PerfxPort, gPortDispatchersLen);

   pthread_mutex_init(&gPortDispatcherShared.mutex, NULL);
   pthread_cond_init(&gPortDispatcherShared.cond, NULL);

   for (i = 0; i < gPortDispatchersLen; i++) {
      PerfxPortDispatcher_Init(&gPortDispatchers[i], i);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPort_Create --
 *
 *       Creates a new PerfxPort that can be used to delivery messages to
 *       a particular handler.
 *
 * Returns:
 *       A newly allocated PerfxPort that should be freed with
 *       PerfxPort_Free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxPort *
PerfxPort_Create(int32_t max_active,       /* IN */
                 PerfxPortHandler handler, /* IN */
                 void *handler_data)       /* IN */
{
   static pthread_once_t once = PTHREAD_ONCE_INIT;

   PerfxPort *port;

   ASSERT(handler);
   ASSERT_INT(max_active, >=, -1);

   pthread_once(&once, PerfxPort_InitOnce);

   if (!max_active) {
      max_active = gPortDispatchersLen;
   }

   port = perfx_new0(PerfxPort, 1);
   port->max_active = max_active;
   port->handler = handler;
   port->handler_data = handler_data;
   pthread_mutex_init(&port->mutex, NULL);
   pthread_cond_init(&port->cond, NULL);

   return port;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPort_GetDispatcher --
 *
 *       Retrieves the next dispatcher to be used by the port.
 *
 * Returns:
 *       A PerfxPortDispatcher.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static PerfxPortDispatcher *
PerfxPort_GetDispatcher(PerfxPort *port) /* IN */
{
   PerfxPortDispatcher *dispatcher;

   ASSERT(port);

   if (!(dispatcher = PerfxPortDispatcher_GetCurrent())) {
      dispatcher = &gPortDispatcherShared;
   }

   return dispatcher;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPort_Busy --
 *
 *       Checks if a PerfxPort is busy processing a workload.
 *
 * Returns:
 *       TRUE or FALSE.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxPort_Busy(PerfxPort *port) /* IN */
{
   ASSERT(port);
   return !!(PerfxAtomicInt_Get(&port->active) ||
             PerfxAtomicPtr_Get((void **)&port->head));
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPort_PostLocked --
 *
 *       Posts the message to the port or directly to the dispatcher if
 *       the port is in a state which permits it.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxPort_PostLocked(PerfxPort *port,       /* IN */
                     PerfxMessage *message) /* IN */
{
   PerfxPortDispatcher *dispatcher;

   ASSERT(port);
   ASSERT(message);

   if (port->active < port->max_active) {
      port->active++;
      dispatcher = PerfxPort_GetDispatcher(port);
      PerfxPortDispatcher_Push(dispatcher, message);
   } else {
      if (port->tail) {
         port->tail->next = message;
      }
      port->tail = message;
      if (!port->head) {
         port->head = message;
      }
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPort_Post --
 *
 *       Post a message to the port. The handler registered for the port
 *       will given the message when the port next allows it.
 *
 *       The port could already be processing a callback, which means it
 *       wont be delivered until the last message has been processed. If
 *       the port is configured to handle messages concurrently, the
 *       message may be processed immediately.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxPort_Post(PerfxPort *port,       /* IN */
               PerfxMessage *message) /* IN */
{
   ASSERT(port);
   ASSERT(message);
   ASSERT(!message->next);

   message->port = port;
   message->next = NULL;

   pthread_mutex_lock(&port->mutex);
   PerfxPort_PostLocked(port, message);
   pthread_mutex_unlock(&port->mutex);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPort_Free --
 *
 *       Frees the PerfxPort. This function will block until all items
 *       that have been posted to the port have been flushed.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxPort_Free(PerfxPort *port) /* IN */
{
   if (port) {
      pthread_mutex_lock(&port->mutex);
      port->shutdown = TRUE;
      while (port->active || port->head) {
         pthread_cond_wait(&port->cond, &port->mutex);
      }
      pthread_mutex_unlock(&port->mutex);
      pthread_mutex_destroy(&port->mutex);
      perfx_free(port);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPort_HandleMessage --
 *
 *       Handles the processing of a message for a given port. The message
 *       is passed to the handler callback provided on port creation.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       message is freed.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxPort_HandleMessage(PerfxPort *port,       /* IN */
                        PerfxMessage *message) /* IN */
{
   ASSERT(port);
   ASSERT(port->handler);
   ASSERT(message);

   /*
    * Dispatch the message to the given port handler.
    */
   port->handler(port, message, port->handler_data);

   /*
    * Release the message.
    */
   PerfxMessage_Free(message);
   message = NULL;

   /*
    * Process the next item on the port if necessary.
    */
   pthread_mutex_lock(&port->mutex);
   port->active--;
   if ((message = port->head)) {
      port->active++;
      port->head = port->head->next;
      if (port->tail == message) {
         port->tail = NULL;
      }
   } else if (!port->active && port->shutdown) {
      pthread_cond_signal(&port->cond);
   }
   pthread_mutex_unlock(&port->mutex);
   if (message) {
      PerfxPortDispatcher_Push(PerfxPortDispatcher_GetCurrent(),
                               message);
   }
}
