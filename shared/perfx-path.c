/* perfx-path.c
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include <libgen.h>
#include <limits.h>
#include <stdarg.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "perfx-debug.h"
#include "perfx-memory.h"
#include "perfx-path.h"
#include "perfx-string.h"


#define MAX_RECURSION_DEPTH (12)


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPath_ResolveLinkReal --
 *
 *       Recursively resolves the potential symlink @filename.
 *
 * Returns:
 *       A newly allocated string if successful; otherwise NULL.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

#include <stdio.h>

static char *
PerfxPath_ResolveLinkReal(const char *filename, /* IN */
                          uint32_t depth)       /* IN */
{
   char lnkpath[PATH_MAX];
   char *dir;
   char *path;
   char *ret;

   ENTRY;

   ASSERT(filename);

   if (PerfxPath_IsRegular(filename)) {
      RETURN(perfx_strdup(filename));
   }

   if (depth > MAX_RECURSION_DEPTH) {
      RETURN(NULL);
   }

   if (PerfxPath_IsSymlink(filename)) {
      memset(lnkpath, 0, sizeof lnkpath);
      if (-1 != readlink(filename, lnkpath, sizeof lnkpath - 1)) {
         dir = PerfxPath_GetDirectory(filename);
         path = PerfxPath_Build(dir, lnkpath, NULL);
         ret = PerfxPath_ResolveLinkReal(path, depth + 1);
         perfx_free(path);
         perfx_free(dir);
         RETURN(ret);
      }
   }

   if (PerfxPath_IsExecutable(filename)) {
      RETURN(perfx_strdup(filename));
   }

   RETURN(NULL);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPath_ResolveLink --
 *
 *       Recursively resolves the potential symlink found at @filename.
 *       If @filename is a regular file, a copy of @filename is returned.
 *
 *       The caller is responsibile for freeing the result with
 *       perfx_free().
 *
 * Returns:
 *       A newly allocated string if successful; otherwise NULL.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

char *
PerfxPath_ResolveLink(const char *filename) /* IN */
{
   return PerfxPath_ResolveLinkReal(filename, 0);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPath_Test --
 *
 *       Checks to see if the file found at @filename matches a series
 *       of tests.
 *
 *       @flags is a bitwise or of the following flags:
 *
 *          PERFX_PATH_TEST_IS_REGULAR
 *          PERFX_PATH_TEST_IS_DIR
 *          PERFX_PATH_TEST_IS_EXECUTABLE
 *          PERFX_PATH_TEST_IS_SYMLINK
 *
 * Returns:
 *       TRUE if all the requirements were met.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxPath_Test(const char *filename,     /* IN */
               PerfxPathTestFlags flags) /* IN */
{
   PerfxPathTestFlags real_flags = 0;
   struct stat st;

   ASSERT(filename);
   ASSERT(flags);

   memset(&st, 0, sizeof st);

   if (!lstat(filename, &st)) {
      switch ((st.st_mode & S_IFMT)) {
      case S_IFLNK:
         real_flags |= PERFX_PATH_TEST_IS_SYMLINK;
         break;
      case S_IFREG:
         real_flags |= PERFX_PATH_TEST_IS_REGULAR;
         break;
      case S_IFDIR:
         real_flags |= PERFX_PATH_TEST_IS_DIR;
         break;
      default:
         break;
      }
   }

   if ((flags & PERFX_PATH_TEST_IS_EXECUTABLE)) {
      if (!access(filename, X_OK)) {
         real_flags |= PERFX_PATH_TEST_IS_EXECUTABLE;
      }
   }

   return ((real_flags & flags) == flags);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPath_IsDirectory --
 *
 *       Helper function to check if a given file is a directory.
 *
 * Returns:
 *       TRUE if @filename is a directory.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxPath_IsDirectory(const char *filename) /* IN */
{
   ASSERT(filename);
   return PerfxPath_Test(filename, PERFX_PATH_TEST_IS_DIR);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPath_IsRegular --
 *
 *       Helper function to check if a given file is a regular file.
 *
 * Returns:
 *       TRUE if @filename is a regular file.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxPath_IsRegular(const char *filename) /* IN */
{
   ASSERT(filename);
   return PerfxPath_Test(filename, PERFX_PATH_TEST_IS_REGULAR);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPath_IsExecutable --
 *
 *       Helper function to check if a given file is an executable.
 *
 * Returns:
 *       TRUE if @filename is an executable.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxPath_IsExecutable(const char *filename) /* IN */
{
   ASSERT(filename);
   return PerfxPath_Test(filename, PERFX_PATH_TEST_IS_EXECUTABLE);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPath_IsSymlink --
 *
 *       Helper function to check if a given file is a symlink.
 *
 * Returns:
 *       TRUE if @filename is a symlink.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxPath_IsSymlink(const char *filename) /* IN */
{
   ASSERT(filename);
   return PerfxPath_Test(filename, PERFX_PATH_TEST_IS_SYMLINK);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPath_HasPrefix --
 *
 *       Checks if the basename of @filename starts with @prefix.
 *
 * Returns:
 *       TRUE if basename(@filename) starts with @prefix.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxPath_HasPrefix(const char *filename, /* IN */
                    const char *prefix)   /* IN */
{
   bool ret = FALSE;
   char *copy;
   char *base;

   ASSERT(filename);
   ASSERT(prefix);

   copy = perfx_strdup(filename);
   if ((base = basename(copy))) {
      if (!memcmp(base, prefix, strlen(prefix))) {
         ret = TRUE;
      }
   }
   perfx_free(copy);

   return ret;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPath_HasSuffix --
 *
 *       Checks if @filename ends with @suffix.
 *
 * Returns:
 *       TRUE if @filename ends with suffix.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxPath_HasSuffix(const char *filename, /* IN */
                    const char *suffix)   /* IN */
{
   uint32_t len;

   ASSERT(filename);
   ASSERT(suffix);

   len = strlen(suffix);
   return !memcmp(suffix, filename + strlen(filename) - len, len);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPath_GetDirectory --
 *
 *       Retrieve the directory for a given path.
 *
 * Returns:
 *       A newly allocated string that should be freed by the caller.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

char *
PerfxPath_GetDirectory(const char *filename) /* IN */
{
   char *copy;
   char *dir;

   ASSERT(filename);

   copy = perfx_strdup(filename);
   dir = dirname(copy);
   dir = perfx_strdup(dir);
   perfx_free(copy);

   return dir;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPath_Build --
 *
 *       Build a new path using the path portions provided.
 *       This function requries a NULL sentinel.
 *
 * Returns:
 *       A newly allocated string that should be freed by the caller.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

char *
PerfxPath_Build(const char *first_path, /* IN */
                ...)
{
   PerfxString *str;
   const char *part;
   va_list args;

   ASSERT(first_path);

   va_start(args, first_path);

   str = PerfxString_Create(NULL);
   PerfxString_Append(str, first_path);

   while ((part = va_arg(args, char *))) {
      PerfxString_Append(str, "/");
      PerfxString_Append(str, part);
   }

   va_end(args);

   return PerfxString_Free(str, FALSE);
}
