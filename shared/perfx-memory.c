/* perfx-memory.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdlib.h>

#include "perfx-log.h"
#include "perfx-memory.h"


/*
 *--------------------------------------------------------------------------
 *
 * perfx_alloc0 --
 *
 *       Allocates a block of memory that is initialized to zero. Providing
 *       file and lineno allows a proper error message to be presented if
 *       the memory could not be allocated.
 *
 *       This function will abort() if the memory could not be allocated.
 *
 * Returns:
 *       A pointer to the initialized memory. Should be freed with
 *       perfx_free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void *
perfx_alloc0(size_t size,      /* IN */
             uint32_t count,   /* IN */
             const char *file, /* IN */
             uint32_t lineno)  /* IN */
{
   unsigned long n_bytes;
   void *data;

   n_bytes = size * count;

   if (!(data = calloc(count, size))) {
      PANIC("Failed to allocate %lu bytes", n_bytes);
   }

   return data;
}


/*
 *--------------------------------------------------------------------------
 *
 * perfx_alloc --
 *
 *       Allocates a block of memory without initializing it. Providing file
 *       and lineno allows for a proper error message to be presented if
 *       the memory could not be allocated.
 *
 *       This function will abort() if the memory could not be allcoated.
 *
 * Returns:
 *       A pointer to the uninitialized memory. Should be freed with
 *       perfx_free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void *
perfx_alloc(size_t size,      /* IN */
            uint32_t count,   /* IN */
            const char *file, /* IN */
            uint32_t lineno)  /* IN */
{
   unsigned long n_bytes;
   void *data;

   n_bytes = size * count;

   if (!(data = malloc(n_bytes))) {
      PANIC("Failed to allocate %lu bytes", n_bytes);
   }

   return data;
}


/*
 *--------------------------------------------------------------------------
 *
 * perfx_free --
 *
 *       Frees the memory allcoated in data. If data is NULL, then no work
 *       is performed.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
perfx_free(void *data) /* IN */
{
   free(data);
}
