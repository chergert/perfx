/* perfx-line-protocol.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_LINE_PROTOCOL_H
#define PERFX_LINE_PROTOCOL_H


#include "perfx-protocol.h"


PERFX_BEGIN_DECLS


#define PERFX_TYPE_LINE_PROTOCOL         (PerfxLineProtocol_GetType())
#define PERFX_LINE_PROTOCOL_GET_CLASS(o) ((PerfxLineProtocolClass *)PERFX_OBJECT_GET_CLASS(o))


typedef struct _PerfxLineProtocol      PerfxLineProtocol;
typedef struct _PerfxLineProtocolClass PerfxLineProtocolClass;


struct _PerfxLineProtocol {
   PerfxProtocol parent;
};


struct _PerfxLineProtocolClass {
   PerfxProtocolClass parent_class;

   void (*LineReceived) (PerfxLineProtocol *protocol,
                         const char *line,
                         ssize_t len);
};


PerfxType      PerfxLineProtocol_GetType  (void) PERFX_GNUC_CONST;
PerfxProtocol *PerfxLineProtocol_Create   (void);
void           PerfxLineProtocol_SendLine (PerfxLineProtocol *protocol,
                                           const char *line,
                                           ssize_t len);


PERFX_END_DECLS


#endif /* PERFX_LINE_PROTOCOL_H */
