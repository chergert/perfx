/* perfx-value.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PERFX_VALUE_H
#define PERFX_VALUE_H


#include "perfx-types.h"


PERFX_BEGIN_DECLS


typedef enum {
   PERFX_VALUE_INT = 1,
   PERFX_VALUE_UINT,
   PERFX_VALUE_BOOL,
   PERFX_VALUE_STRING,
   PERFX_VALUE_DOUBLE,
} PerfxValueType;


typedef struct {
   PerfxValueType type;
   union {
      int32_t v_int;
      uint32_t v_uint;
      bool v_bool;
      char *v_str;
      double v_dbl;
   } data;
} PerfxValue;


typedef void (*PerfxValueCallback) (const PerfxValue *value,
                                    void *data);


void  PerfxValue_Copy      (const PerfxValue *src,
                            PerfxValue *dst);
void  PerfxValue_Init      (PerfxValue *value,
                            PerfxValueType type);
void  PerfxValue_Unset     (PerfxValue *value);
void  PerfxValue_SetInt    (PerfxValue *value,
                            int32_t v_int);
void  PerfxValue_SetUInt   (PerfxValue *value,
                            uint32_t v_uint);
void  PerfxValue_SetBool   (PerfxValue *value,
                            bool v_bool);
void  PerfxValue_SetDouble (PerfxValue *value,
                            double v_dbl);
void  PerfxValue_SetString (PerfxValue *value,
                            const char *v_str);
char *PerfxValue_ToString  (PerfxValue *value);


PERFX_END_DECLS


#endif /* PERFX_VALUE_H */
