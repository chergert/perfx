/* perfx-macros.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_MACROS_H
#define PERFX_MACROS_H


#ifdef __linux__
#include <endian.h>
#endif


#ifdef __APPLE__
#include <libkern/OSByteOrder.h>
#endif


#ifdef __cplusplus
#define PERFX_BEGIN_DECLS extern "C" {
#define PERFX_END_DECLS   }
#else
#define PERFX_BEGIN_DECLS
#define PERFX_END_DECLS
#endif


PERFX_BEGIN_DECLS


#ifndef MIN
#define MIN(a,b) ((a < b) ? a : b)
#endif


#ifndef MAX
#define MAX(a,b) ((a > b) ? a : b)
#endif


#ifndef ABS
#define ABS(a) (((a) < 0) ? ((a) * -1) : (a))
#endif


#define perfx_str_empty(s)  (!s[0])
#define perfx_str_empty0(s) (!s || !s[0])


#if __WORDSIZE == 64
#define INT_TO_POINTER(i) ((void *)  (int64_t) (i))
#define POINTER_TO_INT(p) ((int32_t) (int64_t) (p))
#define POINTER_TO_SIZE(p) ((size_t) (int64_t) (p))
#define SIZE_TO_POINTER(s) ((void *) (int64_t) (s))
#else
#define INT_TO_POINTER(i) ((void *)  (int32_t) (i))
#define POINTER_TO_INT(p) ((int32_t) (int32_t) (p))
#define POINTER_TO_SIZE(p) ((size_t) (int32_t) (p))
#define SIZE_TO_POINTER(s) ((void *) (int32_t) (s))
#endif


#define GCC_VERSION (__GNUC__ * 10000 \
                     + __GNUC_MINOR__ * 100 \
                     + __GNUC_PATCHLEVEL__)


#define PERFX_GNUC_CONST           __attribute__((const))
#define PERFX_GNUC_NULL_TERMINATED __attribute__((__sentinel__))


#if GCC_VERSION > 40400
#define PERFX_GNUC_PRINTF(f,v) __attribute__((format(gnu_printf, f, v)))
#else
#define PERFX_GNUC_PRINTF(f,v)
#endif


#if defined(__linux__)
#define UINT32_FROM_LE(v) ((uint32_t) le32toh(v))
#define UINT32_TO_LE(v)   ((uint32_t) htole32(v))
#define UINT32_TO_BE(v)   ((uint32_t) htobe32(v))
#define UINT64_FROM_LE(v) ((uint64_t) le64toh(v))
#define UINT64_TO_LE(v)   ((uint64_t) htole64(v))
#define UINT64_TO_BE(v)   ((uint64_t) htobe64(v))
#elif defined(__APPLE__)
#define UINT32_FROM_LE(v) ((uint32_t) OSSwapLittleToHostInt32((v)))
#define UINT32_TO_LE(v)   ((uint32_t) OSSwapHostToLittleInt32((v)))
#define UINT64_FROM_LE(v) ((uint64_t) OSSwapLittleToHostInt64((v)))
#define UINT64_TO_LE(v)   ((uint64_t) OSSwapHostToLittleInt64((v)))
#else
#error operating system not supported
#endif


PERFX_END_DECLS


#endif /* PERFX_MACROS_H */
