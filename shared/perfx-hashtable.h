/* perfx-hashtable.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_HASHTABLE_H
#define PERFX_HASHTABLE_H


#include "perfx-macros.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


typedef struct _PerfxHashTable PerfxHashTable;


PerfxHashTable *PerfxHashTable_Create    (uint32_t size,
                                          PerfxHashFunc hash_func,
                                          PerfxEqualFunc equal_func,
                                          PerfxFreeFunc key_free_func,
                                          PerfxFreeFunc value_free_func);
uint32_t        PerfxHashTable_CountKeys (PerfxHashTable *hashtable);
void            PerfxHashTable_Free      (PerfxHashTable *hash_table);
void            PerfxHashTable_Insert    (PerfxHashTable *hash_table,
                                          void *key,
                                          void *data);
void            PerfxHashTable_Remove    (PerfxHashTable *hash_table,
                                          const void *key);
bool            PerfxHashTable_Contains  (PerfxHashTable *hash_table,
                                          const void *key);
void           *PerfxHashTable_Lookup    (PerfxHashTable *hash_table,
                                          const void *key);


PERFX_END_DECLS


#endif /* PERFX_HASHTABLE_H */
