/* perfx-main.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <poll.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>

#include "perfx-array.h"
#include "perfx-clock.h"
#include "perfx-debug.h"
#include "perfx-list.h"
#include "perfx-log.h"
#include "perfx-main.h"
#include "perfx-memory.h"
#include "perfx-hashtable.h"


static PerfxHashTable *gHandlers;
static PerfxList      *gPending;
static PerfxArray     *gPollFds;
static PerfxArray     *gPollHandlers;
static int32_t         gPollTimeout;
static pthread_mutex_t gMutex = PTHREAD_MUTEX_INITIALIZER;
static uint32_t        gSequence;
static bool            gShutdown;
static PerfxArray     *gTimeoutHandlers;
static pthread_t       gThread;
static int             gWakeupPipe[2] = { -1, -1 };


typedef enum {
   PERFX_MAIN_HANDLER_POLL = 1,
   PERFX_MAIN_HANDLER_TIMEOUT,
} PerfxMainHandlerType;


typedef struct {
   PerfxMainHandlerType type;
   uint32_t id;
   bool removed;
   union {
      struct {
         int fd;
         short events;
         int32_t fds_idx;
         PerfxMainPollFunc func;
         void *data;
      } poll;
      struct {
         PerfxMainTimeoutFunc func;
         PerfxFreeFunc destroy;
         void *data;
         struct timespec ts;
         uint32_t interval_msec;
      } timeout;
   } u;
} PerfxMainHandler;


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_Wakeup --
 *
 *       Wakeup the main loop. This will write a single byte to the
 *       internal write side of the pipe(2) causing any calls to poll()
 *       to wake up.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       gWakeupPipe[1] is written to.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxMain_Wakeup(void)
{
   char buf[1] = { 0 };
   int ret;

   ASSERT_INT(gWakeupPipe[1], >=, 0);
   ret = write(gWakeupPipe[1], &buf, sizeof buf);
   ASSERT_INT(ret, ==, sizeof buf);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_CreateHandler --
 *
 *       Create a new handler and add it to the handlers hashtable. The
 *       caller is expected to hold the gMutex lock.
 *
 * Returns:
 *       A newly allocated PerfxMainHandler.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static PerfxMainHandler *
PerfxMain_CreateHandler(void)
{
   PerfxMainHandler *handler;

   ASSERT(gHandlers);

   handler = perfx_new0(PerfxMainHandler, 1);
   handler->id = ++gSequence;
   while (PerfxHashTable_Contains(gHandlers, &handler->id)) {
      handler->id = ++gSequence;
   }
   PerfxHashTable_Insert(gHandlers, &handler->id, handler);

   return handler;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_AddPending --
 *
 *       Add handler to the list of pending operations.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxMain_AddPending(PerfxMainHandler *handler) /* IN */
{
   ASSERT(handler);

   gPending = PerfxList_Prepend(gPending, INT_TO_POINTER(handler->id));
   PerfxMain_Wakeup();
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_AddPoll --
 *
 *       Add a function to be called every time the required events are
 *       met on the provided file descriptor.
 *
 * Returns:
 *       A handler id that can be provided to PerfxMain_Remove().
 *
 * Side effects:
 *       The poll handler is scheduled.
 *
 *--------------------------------------------------------------------------
 */

uint32_t
PerfxMain_AddPoll(int fd,                     /* IN */
                  PerfxMainPollFlags flags,   /* IN */
                  PerfxMainPollFunc callback, /* IN */
                  void *data)                 /* IN */
{
   PerfxMainHandler *handler;
   uint32_t handler_id;
   short events = 0;

   ASSERT(fd >= 0);
   ASSERT(callback);

   if (flags & PERFX_MAIN_POLL_IN) {
      events |= POLLIN;
   }
   if (flags & PERFX_MAIN_POLL_OUT) {
      events |= POLLOUT;
   }
   if (flags & PERFX_MAIN_POLL_HUP) {
      events |= POLLHUP;
   }

   pthread_mutex_lock(&gMutex);

   handler = PerfxMain_CreateHandler();
   handler_id = handler->id;
   handler->type = PERFX_MAIN_HANDLER_POLL;
   handler->u.poll.fd = fd;
   handler->u.poll.fds_idx = -1;
   handler->u.poll.events = events;
   handler->u.poll.func = callback;
   handler->u.poll.data = data;

   PerfxMain_AddPending(handler);

   pthread_mutex_unlock(&gMutex);

   return handler_id;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_AddTimeoutFull --
 *
 *       Add a timeout function to be called ever interval_msec
 *       milliseconds. callback will be removed when it returns FALSE.
 *       If destroy is provided, it will be called to destroy data.
 *
 * Returns:
 *       The handler id that can be supplied to PerfxMain_Remove().
 *
 * Side effects:
 *       Timeout callback is scheduled.
 *
 *--------------------------------------------------------------------------
 */

uint32_t
PerfxMain_AddTimeoutFull(uint32_t interval_msec,        /* IN */
                         PerfxMainTimeoutFunc callback, /* IN */
                         void *data,                    /* IN */
                         PerfxFreeFunc destroy)         /* IN */
{
   PerfxMainHandler *handler;
   uint32_t handler_id;

   ASSERT(callback);

   pthread_mutex_lock(&gMutex);

   handler = PerfxMain_CreateHandler();
   handler_id = handler->id;
   handler->type = PERFX_MAIN_HANDLER_TIMEOUT;
   handler->u.timeout.func = callback;
   handler->u.timeout.data = data;
   handler->u.timeout.destroy = destroy;
   handler->u.timeout.interval_msec = interval_msec;
   PerfxClock_GetMonotonic(&handler->u.timeout.ts);
   PerfxClock_AddMsec(&handler->u.timeout.ts, interval_msec);

   PerfxMain_AddPending(handler);

   pthread_mutex_unlock(&gMutex);

   return handler_id;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_AddTimeout --
 *
 *       Add a timeout function to be called ever interval_msec
 *       milliseconds. callback will be removed when it returns FALSE.
 *
 *       See PerfxMain_AddTimeoutFull() for more info.
 *
 * Returns:
 *       A handler id that can be passed to PerfxMain_Remove().
 *
 * Side effects:
 *       Timeout callback is scheduled.
 *
 *--------------------------------------------------------------------------
 */

uint32_t
PerfxMain_AddTimeout(uint32_t interval_msec,        /* IN */
                     PerfxMainTimeoutFunc callback, /* IN */
                     void *data)                    /* IN */
{
   ASSERT(interval_msec);
   ASSERT(callback);

   return PerfxMain_AddTimeoutFull(interval_msec, callback, data, NULL);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_AddTimeoutSeconds --
 *
 *       Add a timeout function to be called every interval_seconds.
 *       callback will be removed when it returns FALSE.
 *
 * Returns:
 *       A handler that can be provided to PerfxMain_Remove().
 *
 * Side effects:
 *       Timeout callback is scheduled.
 *
 *--------------------------------------------------------------------------
 */

uint32_t
PerfxMain_AddTimeoutSeconds(uint32_t interval_seconds,     /* IN */
                            PerfxMainTimeoutFunc callback, /* IN */
                            void *data)                    /* IN */
{
   ASSERT(interval_seconds);
   ASSERT(callback);

   return PerfxMain_AddTimeout(1000 * interval_seconds, callback, data);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_IdleCb --
 *
 *       Callback used to execute the given idle callback function from
 *       a timeout. The timeout is always removed.
 *
 * Returns:
 *       FALSE always.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static bool
PerfxMain_IdleCb(void *data) /* IN */
{
   void **closure = data;
   PerfxMainIdleFunc callback;

   ASSERT(closure);
   ASSERT(closure[0]);

   callback = closure[0];
   callback(closure[1]);
   return FALSE;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_AddIdle --
 *
 *       Adds an idle function to be executed the next time the main loop
 *       makes an iteration.
 *
 * Returns:
 *       A handler id that can be provided to PerfxMain_Remove().
 *
 * Side effects:
 *       Idle callback is scheduled.
 *
 *--------------------------------------------------------------------------
 */

uint32_t
PerfxMain_AddIdle(PerfxMainIdleFunc callback, /* IN */
                  void *data)                 /* IN */
{
   void **closure;

   ASSERT(callback);

   closure = perfx_new0(void *, 2);
   closure[0] = callback;
   closure[1] = data;

   return PerfxMain_AddTimeoutFull(0, PerfxMain_IdleCb, closure, perfx_free);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_OnWakeup --
 *
 *       Handle the callback for the internal pipe() used for waking up
 *       the main loop from a call to poll().
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxMain_OnWakeup(int fd,                   /* IN */
                   PerfxMainPollFlags flags, /* IN */
                   void *data)               /* IN */
{
   char buf[1];

   while (read(fd, buf, sizeof buf) == sizeof buf) {
      /* Do nothing */
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMainHandler_Free --
 *
 *       Frees a PerfxMainHandler and its associated resources.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       The handler is freed.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxMainHandler_Free(void *data) /* IN */
{
   PerfxMainHandler *handler = data;

   ASSERT(handler);

   if (handler->type == PERFX_MAIN_HANDLER_TIMEOUT) {
      if (handler->u.timeout.destroy) {
         handler->u.timeout.destroy(handler->u.timeout.data);
      }
   }

   perfx_free(handler);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_Create --
 *
 *       Creates the main loop and associated resources.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxMain_Create(void)
{
   ASSERT(!gHandlers);
   ASSERT(!gPending);
   ASSERT(!gPollFds);
   ASSERT(!gPollHandlers);
   ASSERT(!gTimeoutHandlers);

   gHandlers = PerfxHashTable_Create(1024,
                                     perfx_int_hash,
                                     perfx_int_equal,
                                     NULL,
                                     PerfxMainHandler_Free);
   gPollFds = PerfxArray_Create(FALSE, sizeof(struct pollfd));
   gPollHandlers = PerfxArray_Create(FALSE, sizeof(PerfxMainHandler*));
   gTimeoutHandlers = PerfxArray_Create(FALSE, sizeof(PerfxMainHandler*));

   if (!!pipe(gWakeupPipe)) {
      PANIC("Failed to initialize main loop.");
   }

   fcntl(gWakeupPipe[0], F_SETFL, O_NONBLOCK);
   fcntl(gWakeupPipe[1], F_SETFL, O_NONBLOCK);

   PerfxMain_AddPoll(gWakeupPipe[0],
                     PERFX_MAIN_POLL_IN | PERFX_MAIN_POLL_HUP,
                     PerfxMain_OnWakeup, NULL);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_Destroy --
 *
 *       Cleans up after the main loop.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxMain_Destroy(void)
{
   PerfxHashTable_Free(gHandlers);
   gHandlers = NULL;

   PerfxArray_Free(gPollFds);
   gPollFds = NULL;

   PerfxArray_Free(gPollHandlers);
   gPollHandlers = NULL;

   PerfxArray_Free(gTimeoutHandlers);
   gTimeoutHandlers = NULL;

   PerfxList_Free(gPending);
   gPending = NULL;

   close(gWakeupPipe[1]);
   gWakeupPipe[1] = 0;

   close(gWakeupPipe[0]);
   gWakeupPipe[0] = 0;

   gThread = (pthread_t)0;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_RemoveHandler --
 *
 *       Remove a handler.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxMain_RemoveHandler(PerfxMainHandler *handler) /* IN */
{
   ASSERT(handler);
   ASSERT(gHandlers);

   pthread_mutex_lock(&gMutex);

   handler->removed = TRUE;
   PerfxMain_AddPending(handler);

   pthread_mutex_unlock(&gMutex);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_Remove --
 *
 *       Remove a given handler from being executed again by the main loop.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       The handler is removed from the main loop.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxMain_Remove(uint32_t handler_id) /* IN */
{
   PerfxMainHandler *handler;

   ASSERT(handler_id);
   ASSERT(gHandlers);

   pthread_mutex_lock(&gMutex);

   if ((handler = PerfxHashTable_Lookup(gHandlers, &handler_id))) {
      ASSERT(handler->id == handler_id);
      handler->removed = TRUE;
      PerfxMain_AddPending(handler);
   }

   pthread_mutex_unlock(&gMutex);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_ClearHandler --
 *
 *       Utility function to remove a handler and clear the handler_id
 *       stored at the location handler_location.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       handler_location is zeroed if necessary.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxMain_ClearHandler(uint32_t *handler_location) /* IN/OUT */
{
   uint32_t handler_id;

   ASSERT(handler_location);

   if ((handler_id = *handler_location)) {
      *handler_location = 0;
      PerfxMain_Remove(handler_id);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_DispatchTimeoutHandlers --
 *
 *       Dispatch the handlers that have elapsed.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxMain_DispatchTimeoutHandlers(void)
{
   PerfxMainHandler *handler;
   struct timespec now;
   uint32_t i;

   ASSERT(gTimeoutHandlers);

   if (gTimeoutHandlers->len) {
      PerfxClock_GetMonotonic(&now);
      for (i = 0; i < gTimeoutHandlers->len; i++) {
         handler = PerfxArray_Index(gTimeoutHandlers, PerfxMainHandler *, i);
         ASSERT(handler);
         ASSERT(handler->type == PERFX_MAIN_HANDLER_TIMEOUT);

         if (PerfxClock_CompareMsec(&handler->u.timeout.ts, &now) <= 0) {
            if (!handler->removed) {
               if (!handler->u.timeout.func(handler->u.timeout.data)) {
                  PerfxMain_RemoveHandler(handler);
               } else {
                  PerfxClock_GetMonotonic(&handler->u.timeout.ts);
                  PerfxClock_AddMsec(&handler->u.timeout.ts,
                                     handler->u.timeout.interval_msec);
               }
            }
         }
      }
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_DispatchPollHandlers --
 *
 *       Dispatch all file-descriptor handlers that have events pending.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxMain_DispatchPollHandlers(void)
{
   PerfxMainPollFlags flags;
   PerfxMainHandler *handler;
   struct pollfd *pfd;
   uint32_t i;

   ASSERT(gPollFds);
   ASSERT(gPollFds->len);
   ASSERT(gPollHandlers);
   ASSERT(gPollHandlers->len);
   ASSERT(gPollHandlers->len == gPollFds->len);

   for (i = 0; i < gPollFds->len; i++) {
      pfd = &PerfxArray_Index(gPollFds, struct pollfd, i);
      if (pfd->revents) {
         handler = PerfxArray_Index(gPollHandlers, PerfxMainHandler *, i);
         ASSERT(handler);

         flags = 0;
         flags |= (pfd->revents & POLLIN)  ? PERFX_MAIN_POLL_IN  : 0;
         flags |= (pfd->revents & POLLOUT) ? PERFX_MAIN_POLL_OUT : 0;
         flags |= (pfd->revents & POLLHUP) ? PERFX_MAIN_POLL_HUP : 0;
         pfd->revents = 0;

         if (!handler->removed) {
            ASSERT(handler->u.poll.func);
            handler->u.poll.func(handler->u.poll.fd, flags,
                                 handler->u.poll.data);
         }
      }
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_HandlePending --
 *
 *       Handle the requests in the pending list.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxMain_HandlePending(void)
{
   PerfxMainHandler *handler;
   PerfxMainHandler *that;
   struct pollfd pfd;
   PerfxList *list;
   PerfxList *iter;
   uint32_t handler_id;
   uint32_t i;

   ASSERT(gHandlers);
   ASSERT(gPollHandlers);
   ASSERT(gPollFds);

   pthread_mutex_lock(&gMutex);

   list = gPending;
   gPending = NULL;

   for (iter = list; iter; iter = iter->next) {
      handler_id = POINTER_TO_INT(iter->data);
      if (!(handler = PerfxHashTable_Lookup(gHandlers, &handler_id))) {
         continue;
      }

      ASSERT(handler);
      ASSERT(handler->id);
      ASSERT((handler->type == PERFX_MAIN_HANDLER_POLL) ||
             (handler->type == PERFX_MAIN_HANDLER_TIMEOUT));

      if (handler->removed) {
         switch (handler->type) {
         case PERFX_MAIN_HANDLER_POLL:
            if (handler->u.poll.fds_idx >= 0) {
               PerfxArray_RemoveFast(gPollFds, handler->u.poll.fds_idx);
               PerfxArray_RemoveFast(gPollHandlers, handler->u.poll.fds_idx);
               handler->u.poll.fds_idx = -1;
            }
            PerfxHashTable_Remove(gHandlers, &handler->id);
            break;
         case PERFX_MAIN_HANDLER_TIMEOUT:
            for (i = 0; i < gTimeoutHandlers->len; i++) {
               that = PerfxArray_Index(gTimeoutHandlers, PerfxMainHandler*, i);
               ASSERT(that);

               if (that->id == handler->id) {
                  PerfxArray_RemoveFast(gTimeoutHandlers, i);
                  break;
               }
            }
            PerfxHashTable_Remove(gHandlers, &handler->id);
            break;
         default:
            ASSERT_NOT_REACHED();
         }
      } else {
         switch (handler->type) {
         case PERFX_MAIN_HANDLER_POLL:
            pfd.fd = handler->u.poll.fd;
            pfd.events = handler->u.poll.events;
            pfd.revents = 0;
            PerfxArray_Append(gPollFds, pfd);
            PerfxArray_Append(gPollHandlers, handler);
            handler->u.poll.fds_idx = gPollFds->len - 1;
            break;
         case PERFX_MAIN_HANDLER_TIMEOUT:
            PerfxArray_Append(gTimeoutHandlers, handler);
            break;
         default:
            ASSERT_NOT_REACHED();
         }
      }
   }

   pthread_mutex_unlock(&gMutex);

   PerfxList_Free(list);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_CalcPollTimeout --
 *
 *       Calculate the amount of time until poll() must return. The
 *       calculated value is in milliseconds.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       gPollTimeout is set.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxMain_CalcPollTimeout(void)
{
   PerfxMainHandler *handler;
   struct timespec now;
   struct timespec until;
   struct timespec soonest = { 0 };
   uint32_t i;
   bool found = FALSE;

   ASSERT(gTimeoutHandlers);
   ASSERT(PerfxMain_IsMainThread());

   if (!gTimeoutHandlers->len) {
      gPollTimeout = -1;
      return;
   }

   PerfxClock_GetMonotonic(&now);

   for (i = 0; i < gTimeoutHandlers->len; i++) {
      handler = PerfxArray_Index(gTimeoutHandlers, PerfxMainHandler *, i);
      ASSERT(handler);
      ASSERT(handler->type == PERFX_MAIN_HANDLER_TIMEOUT);
      if (handler->removed) {
         continue;
      }
      if (PerfxClock_CompareMsec(&handler->u.timeout.ts, &now) <= 0) {
         gPollTimeout = 0;
         return;
      }
      if (!found) {
         found = TRUE;
         soonest = handler->u.timeout.ts;
         continue;
      }
      if (PerfxClock_CompareMsec(&handler->u.timeout.ts, &soonest) < 0) {
         soonest = handler->u.timeout.ts;
      }
   }

   if (!found) {
      gPollTimeout = -1;
   } else {
      PerfxClock_Subtract(&soonest, &now, &until);
      gPollTimeout = (until.tv_sec * 1000) + (until.tv_nsec / 1000000) + 1;
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_IterationWithTimeout --
 *
 *       Run an iteration of the main loop, blocking up to timeout
 *       milliseconds. If timeout is less-than zero, then poll() will block
 *       until the next IO condition or scheduled timeout occurs.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxMain_IterationWithTimeout(int32_t timeout) /* IN */
{
   int32_t ret;

   ASSERT(PerfxMain_IsMainThread());

   PerfxMain_HandlePending();

   if (timeout < 0) {
      PerfxMain_CalcPollTimeout();
      timeout = gPollTimeout;
   }

   ret = poll(gPollFds->data, gPollFds->len, timeout);

   switch (ret) {
   case -1:
      ASSERT(FALSE);
      break;
   case 0:
      PerfxMain_DispatchTimeoutHandlers();
      break;
   default:
      PerfxMain_DispatchPollHandlers();
      break;
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_Iteration --
 *
 *       Perform an iteration of the main loop without blocking.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxMain_Iteration(void)
{
   PerfxMain_IterationWithTimeout(0);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_Run --
 *
 *       Runs the main loop. Method blocks until PerfxMain_Quit() is
 *       called.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxMain_Run(void)
{
   gShutdown = FALSE;
   gThread = pthread_self();
   while (!gShutdown) {
      PerfxMain_IterationWithTimeout(-1);
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_Quit --
 *
 *       Quits the Perfx main loop.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxMain_Quit(void)
{
   gShutdown = TRUE;
   PerfxMain_Wakeup();
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxMain_CountHandlers --
 *
 *       Count the number of active handlers registered, including
 *       internal handlers.
 *
 * Returns:
 *       An intenger containing the number of handlers.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

uint32_t
PerfxMain_CountHandlers(void)
{
   ASSERT(gHandlers);
   return PerfxHashTable_CountKeys(gHandlers);
}


bool
PerfxMain_IsMainThread(void)
{
   return pthread_equal(pthread_self(), gThread);
}
