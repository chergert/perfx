/* perfx-hashtable.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include "perfx-debug.h"
#include "perfx-hashtable.h"
#include "perfx-memory.h"


typedef struct _PerfxHashTableItem {
   struct _PerfxHashTableItem *next;
   void *key;
   void *value;
} PerfxHashTableItem;


struct _PerfxHashTable {
   uint32_t len;
   uint32_t key_count;
   PerfxHashFunc hash_func;
   PerfxEqualFunc equal_func;
   PerfxFreeFunc key_free_func;
   PerfxFreeFunc value_free_func;
   PerfxHashTableItem *table[0];
};


/*
 *--------------------------------------------------------------------------
 *
 * PerfxHashTable_Create --
 *
 *       Creates a new instance of PerfxHashTable.
 *
 * Returns:
 *       PerfxHashTable that should be freed with PerfxHashTable_Free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxHashTable *
PerfxHashTable_Create(uint32_t size,                 /* IN */
                      PerfxHashFunc hash_func,       /* IN */
                      PerfxEqualFunc equal_func,     /* IN */
                      PerfxFreeFunc key_free_func,   /* IN */
                      PerfxFreeFunc value_free_func) /* IN */
{
   PerfxHashTable *hash_table;
   uint32_t n_bytes;

   ASSERT(size > 0);
   ASSERT(hash_func);
   ASSERT(equal_func);

   n_bytes = sizeof *hash_table + (size * sizeof(PerfxHashTableItem *));
   hash_table = perfx_malloc0(n_bytes);
   hash_table->len = size;
   hash_table->hash_func = hash_func;
   hash_table->equal_func = equal_func;
   hash_table->key_free_func = key_free_func;
   hash_table->value_free_func = value_free_func;

   return hash_table;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxHashTable_Lookup --
 *
 *       Attempts to find the first item in the hash table matching key.
 *
 * Returns:
 *       The value stored in the hash table if successful; otherwise NULL.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void *
PerfxHashTable_Lookup(PerfxHashTable *hash_table, /* IN */
                      const void *key)            /* IN */
{
   PerfxHashTableItem *item;
   uint32_t pos;

   ASSERT(hash_table);

   pos = hash_table->hash_func(key) % hash_table->len;

   for (item = hash_table->table[pos]; item; item = item->next) {
      if (hash_table->equal_func(key, item->key)) {
         return item->value;
      }
   }

   return NULL;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxHashTable_Remove --
 *
 *       Removes the first item matching key from the hash table.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxHashTable_Remove(PerfxHashTable *hash_table, /* IN */
                      const void *key)            /* IN */
{
   PerfxHashTableItem *item;
   PerfxHashTableItem *last = NULL;
   uint32_t pos;

   ASSERT(hash_table);

   pos = hash_table->hash_func(key) % hash_table->len;

   for (item = hash_table->table[pos]; item; item = item->next) {
      if (hash_table->equal_func(key, item->key)) {
         if (last) {
            last->next = item->next;
         } else {
            hash_table->table[pos] = item->next;
         }
         hash_table->key_count--;
         if (hash_table->value_free_func) {
            hash_table->value_free_func(item->value);
         }
         if (hash_table->key_free_func) {
            hash_table->key_free_func(item->key);
         }
         perfx_free(item);
         break;
      }
      last = item;
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxHashTable_CountKeys --
 *
 *       Returns the number of keys stored in the hash table. This does
 *       include duplicated entries.
 *
 * Returns:
 *       The number of items in the PerfxHashTable.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

uint32_t
PerfxHashTable_CountKeys(PerfxHashTable *hash_table) /* IN */
{
   ASSERT(hash_table);
   return hash_table->key_count;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxHashTable_Contains --
 *
 *       Checks to see if hash_table contains an item matching key.
 *
 * Returns:
 *       TRUE if an item matches key; otherwise FALSE.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxHashTable_Contains(PerfxHashTable *hash_table, /* IN */
                        const void *key)            /* IN */
{
   PerfxHashTableItem *item;
   uint32_t pos;

   ASSERT(hash_table);

   pos = hash_table->hash_func(key) % hash_table->len;

   for (item = hash_table->table[pos]; item; item = item->next) {
      if (hash_table->equal_func(key, item->key)) {
         return TRUE;
      }
   }

   return FALSE;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxHashTable_Insert --
 *
 *       Inserts a new item into the hash table.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxHashTable_Insert(PerfxHashTable *hash_table, /* IN */
                      void *key,                  /* IN */
                      void *data)                 /* IN */
{
   PerfxHashTableItem *item;
   uint32_t pos;

   ASSERT(hash_table);

   pos = hash_table->hash_func(key) % hash_table->len;

   item = perfx_new0(PerfxHashTableItem, 1);
   item->key = key;
   item->value = data;
   item->next = hash_table->table[pos];

   hash_table->table[pos] = item;
   hash_table->key_count++;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxHashTable_Free --
 *
 *       Destroys the hash table and all the items contained within it.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       Everything.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxHashTable_Free(PerfxHashTable *hash_table) /* IN */
{
   PerfxHashTableItem *item;
   PerfxHashTableItem *removed;
   int32_t i;

   ASSERT(hash_table);

   for (i = 0; i < hash_table->len; i++) {
      item = hash_table->table[i];
      while (item) {
         removed = item;
         item = item->next;
         if (hash_table->key_free_func) {
            hash_table->key_free_func(removed->key);
         }
         if (hash_table->value_free_func) {
            hash_table->value_free_func(removed->value);
         }
         perfx_free(removed);
      }
   }

   perfx_free(hash_table);
}


/*
 *--------------------------------------------------------------------------
 *
 * perfx_int_hash --
 *
 *       Hash function for a pointer to an integer.
 *
 * Returns:
 *       A uint32_t containing the hash.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

uint32_t
perfx_int_hash(const void *data) /* IN */
{
   ASSERT(data);
   return *(uint32_t *)data;
}


/*
 *--------------------------------------------------------------------------
 *
 * perfx_int_equal --
 *
 *       Function to compare equality on two pointers to integers.
 *
 * Returns:
 *       TRUE if the values pointed to be each pointer are the same;
 *       otherwise FALSE.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
perfx_int_equal(const void *data1, /* IN */
                const void *data2) /* IN */
{
   ASSERT(data1);
   ASSERT(data2);
   return (*(uint32_t *)data1) == (*(uint32_t *)data2);
}


/*
 *--------------------------------------------------------------------------
 *
 * perfx_direct_hash --
 *
 *       Hash function for a pointer to a pointer.
 *
 * Returns:
 *       A uint32_t containing the hash.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

uint32_t
perfx_direct_hash(const void *data) /* IN */
{
   return POINTER_TO_INT(data);
}


/*
 *--------------------------------------------------------------------------
 *
 * perfx_direct_equal --
 *
 *       Function to compare equality on two pointers.
 *
 * Returns:
 *       TRUE if the pointers are the same; otherwise FALSE.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
perfx_direct_equal(const void *data1, /* IN */
                   const void *data2) /* IN */
{
   return (data1 == data2);
}


/*
 *--------------------------------------------------------------------------
 *
 * perfx_str_hash --
 *
 *       A hash-function for a string. This is from Daniel J. Bernstein.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

uint32_t
perfx_str_hash(const void *data) /* IN */
{
   const char *string = data;
   uint32_t hash = 5381;
   uint32_t i;

   ASSERT(string);

   for (i = 0; string[i]; i++) {
      hash = ((hash << 5) + hash) + string[i];
   }

   return hash;
}


/*
 *--------------------------------------------------------------------------
 *
 * perfx_str_equal --
 *
 *       Checks to see if two strings are identical. Neither string may
 *       be NULL.
 *
 * Returns:
 *       TRUE if the strings are identical; otherwise FALSE.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
perfx_str_equal(const void *data1, /* IN */
                const void *data2) /* IN */
{
   const char *a = data1;
   const char *b = data2;

   ASSERT(data1);
   ASSERT(data2);

   for (; *a && *b; a++, b++) {
      if (!*(a + 1) && !*(b + 1)) {
         return TRUE;
      }
   }

   return FALSE;
}
