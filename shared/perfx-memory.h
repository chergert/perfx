/* perfx-memory.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_MEMORY_H
#define PERFX_MEMORY_H


#include <unistd.h>

#include "perfx-macros.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


#define perfx_new0(type, num) perfx_alloc0(sizeof(type), num, __FILE__, __LINE__)
#define perfx_new(type, num) perfx_alloc(sizeof(type), num, __FILE__, __LINE__)
#define perfx_malloc0(n_bytes) perfx_alloc0(n_bytes, 1, __FILE__, __LINE__)
#define perfx_malloc(n_bytes) perfx_alloc(n_bytes, 1, __FILE__, __LINE__)


void *perfx_alloc  (size_t size,
                    uint32_t count,
                    const char *filename,
                    uint32_t lineno);
void *perfx_alloc0 (size_t size,
                    uint32_t count,
                    const char *filename,
                    uint32_t lineno);
void  perfx_free   (void *data);


PERFX_END_DECLS


#endif /* PERFX_MEMORY_H */
