/* perfx-string.c
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#define _GNU_SOURCE
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "perfx-debug.h"
#include "perfx-memory.h"
#include "perfx-string.h"


PerfxString *
PerfxString_Create(const char *str) /* IN */
{
   PerfxString *string;

   string = perfx_new0(PerfxString, 1);
   string->len = strlen(str ? str : "");
   string->str = strdup(str ? str : "");

   return string;
}


char *
PerfxString_Free(PerfxString *string, /* IN */
                 bool free_segment)   /* IN */
{
   char *ret = NULL;

   ASSERT(string);

   if (!free_segment) {
      ret = string->str;
   } else {
      perfx_free(string->str);
   }

   perfx_free(string);

   return ret;
}


PerfxString *
PerfxString_Append(PerfxString *string, /* IN */
                   const char *str)     /* IN */
{
   uint32_t len;
   char *s;

   ASSERT(string);
   ASSERT(str);

   len = strlen(str);
   if (!(s = realloc(string->str, string->len + len + 1))) {
      PANIC("OOM while allocating string.");
   }

   memcpy(s + string->len, str, len);
   s[string->len + len] = '\0';
   string->str = s;
   string->len += len;

   return string;
}


PerfxString *
PerfxString_AppendPrintf(PerfxString *string, /* IN */
                         const char *format,  /* IN */
                         ...)
{
   va_list args;
   char *str = NULL;

   ASSERT(string);
   ASSERT(format);

   va_start(args, format);
   vasprintf(&str, format, args);
   va_end(args);

   PerfxString_Append(string, str);
   perfx_free(str);

   return string;
}


void
PerfxString_Truncate(PerfxString *string, /* IN */
                     uint32_t len)        /* IN */
{
   char *s;

   ASSERT(string);

   len++;

   if (string->len > len) {
      if (!(s = realloc(string->str, len))) {
         PANIC("OOM while allocating string.");
      }
      string->str = s;
      string->len = len - 1;
      string->str[len] = '\0';
   }
}
