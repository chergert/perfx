/* perfx-async.h
 *
 * Copyright (C) 2012 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#ifndef PERFX_ASYNC_H
#define PERFX_ASYNC_H


#include "perfx-async.h"
#include "perfx-macros.h"
#include "perfx-types.h"
#include "perfx-value.h"


PERFX_BEGIN_DECLS


typedef struct _PerfxAsyncResult PerfxAsyncResult;


typedef void (*PerfxAsyncCallback) (void *source,
                                    PerfxAsyncResult *result,
                                    void *user_data);


struct _PerfxAsyncResult
{
   int32_t ref_count;
   void *source;
   PerfxAsyncCallback callback;
   void *callback_data;
   PerfxValue value;
};


void              PerfxAsyncResult_Callback     (PerfxAsyncResult *result);
void              PerfxAsyncResult_CallbackIdle (PerfxAsyncResult *result);
PerfxAsyncResult *PerfxAsyncResult_Create       (void *source,
                                                 PerfxAsyncCallback callback,
                                                 void *user_data);
PerfxAsyncResult *PerfxAsyncResult_Ref          (PerfxAsyncResult *result);
void              PerfxAsyncResult_Unref        (PerfxAsyncResult *result);


PERFX_END_DECLS


#endif /* PERFX_ASYNC_H */
