/* perfx-connector.c
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

#include "perfx-connector.h"
#include "perfx-debug.h"
#include "perfx-i18n.h"
#include "perfx-main.h"


enum {
   TYPE_PIPE,
   TYPE_TCP,
   TYPE_UNIX,
};


enum {
   STATE_0,
   STATE_CONNECTING,
   STATE_ESTABLISHED,
   STATE_FAILED,
};


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnector_Destroy --
 *
 *       Handle destruction of the connector object. Free any lingering
 *       resources or poll handlers.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       Everything.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxConnector_Destroy(PerfxObject *object) /* IN */
{
   PerfxConnector *connector = (PerfxConnector *)object;

   ENTRY;

   if (connector->fd_handler) {
      PerfxMain_Remove(connector->fd_handler);
      connector->fd_handler = 0;
   }

   free(connector->errmsg);

   switch (connector->type) {
   case TYPE_PIPE:
      break;
   case TYPE_TCP:
      free(connector->u.tcp.hostname);
      break;
   case TYPE_UNIX:
      free(connector->u.unix_.path);
      break;
   default:
      ASSERT_NOT_REACHED();
   }

   EXIT;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnector_GetType --
 *
 *       Retrieve the PerfxType for the PerfxConnector class.
 *
 * Returns:
 *       A PerfxType.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxType
PerfxConnector_GetType(void)
{
   static PerfxType type = 0;
   PerfxClass *class;

   if (!type) {
      type = PerfxClass_Register("PerfxConnector", 0,
                                 sizeof(PerfxConnectorClass),
                                 sizeof(PerfxConnector),
                                 TRUE);
      class = PerfxType_GetClass(type);
      class->Destroy = PerfxConnector_Destroy;
   }

   return type;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnector_CreatePipe --
 *
 *       Create a new connector object using pipe(2) for file-descriptors.
 *
 * Returns:
 *       A new PerfxConnector object that can be connected with
 *       PerfxConnector_Connect().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxConnector *
PerfxConnector_CreatePipe(void)
{
   PerfxConnector *c;

   c = PerfxObject_Create(PERFX_TYPE_CONNECTOR);
   c->type = TYPE_PIPE;
   c->state = STATE_0;
   c->read_fd = -1;
   c->write_fd = -1;

   return c;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnector_CreateUnix --
 *
 *       Create a new instance of PerfxConnector using a UNIX socket for
 *       the transport mechanism. The UNIX socket should be found at
 *       the path provided.
 *
 * Returns:
 *       A newly allocated PerfxConnector that can be connected with
 *       PerfxConnector_Connect(). Free with PerfxObject_Unref().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxConnector *
PerfxConnector_CreateUnix(const char *path) /* IN */
{
   PerfxConnector *c;

   ASSERT(path);

   c = PerfxObject_Create(PERFX_TYPE_CONNECTOR);
   c->type = TYPE_UNIX;
   c->state = STATE_0;
   c->read_fd = -1;
   c->write_fd = -1;
   c->u.unix_.path = strdup(path);

   return c;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnector_CreateTcp --
 *
 *       Create a new PerfxConnector for connecting to a TCP/IP host.
 *
 * Returns:
 *       A newly allocated PerfxConnector. It can be used with
 *       PerfxConnector_Connect() to connect to a host. Free the object
 *       with PerfxObject_Free().
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxConnector *
PerfxConnector_CreateTcp(const char *hostname, /* IN */
                         uint16_t port)        /* IN */
{
   PerfxConnector *c;

   ASSERT(hostname);
   ASSERT(port);

   c = PerfxObject_Create(PERFX_TYPE_CONNECTOR);
   c->type = TYPE_TCP;
   c->state = STATE_0;
   c->read_fd = -1;
   c->write_fd = -1;
   c->u.tcp.hostname = strdup(hostname);
   c->u.tcp.port = port;

   return c;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnector_Callback --
 *
 *       Main loop idle callback to complete a PerfxConnector_Connect()
 *       request asynchronously.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       Registered callback may be executed.
 *       Lingering reference on connector is dropped.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxConnector_Callback(void *data) /* IN */
{
   PerfxConnector *connector = data;

   ASSERT(connector);

   if (connector->callback) {
      connector->callback(connector, !connector->errmsg,
                          connector->callback_data);
   }

   PerfxObject_Unref(connector);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnector_FailErrno --
 *
 *       Fail a connection attempt using the errno provided to generate
 *       the failure string.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxConnector_FailErrno(PerfxConnector *connector, /* IN */
                         int err)                   /* IN */
{
   ASSERT(connector);
   ASSERT_INT(connector->state, ==, STATE_CONNECTING);
   ASSERT(!connector->errmsg);

   connector->state = STATE_FAILED;
   connector->errmsg = strdup(strerror(err));
   PerfxMain_AddIdle(PerfxConnector_Callback,
                     PerfxObject_Ref(connector));
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnector_ConnectPipe --
 *
 *       Handle a connection using pipe(2).
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxConnector_ConnectPipe(PerfxConnector *connector) /* IN */
{
   int fd[2] = { -1, -1 };

   ASSERT(connector);
   ASSERT_INT(connector->state, ==, STATE_CONNECTING);
   ASSERT_INT(connector->type, ==, TYPE_PIPE);

   errno = 0;

   if (pipe(fd) != 0) {
      PerfxConnector_FailErrno(connector, errno);
      return;
   }

   fcntl(fd[0], F_SETFL, O_NONBLOCK);
   fcntl(fd[1], F_SETFL, O_NONBLOCK);
   connector->read_fd = fd[0];
   connector->write_fd = fd[1];
   connector->state = STATE_ESTABLISHED;

   PerfxMain_AddIdle(PerfxConnector_Callback,
                     PerfxObject_Ref(connector));
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnector_ConnectCb --
 *
 *       Handle the FD being writable after connecting with O_NONBLOCK.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxConnector_ConnectCb(int fd,                   /* IN */
                         PerfxMainPollFlags flags, /* IN */
                         void *data)               /* IN */
{
   PerfxConnector *connector = data;
   socklen_t len = 0;
   int errcode = 0;

   ASSERT(connector);
   ASSERT_INT(connector->state, ==, STATE_CONNECTING);
   ASSERT(!connector->errmsg);

   PerfxMain_Remove(connector->fd_handler);
   connector->fd_handler = 0;

   if (flags & PERFX_MAIN_POLL_HUP) {
      connector->errmsg = strdup(_("Failed to connect to destination"));
      connector->state = STATE_FAILED;
      PerfxMain_AddIdle(PerfxConnector_Callback, connector);
      return;
   } else if (flags & PERFX_MAIN_POLL_OUT) {
      errno = 0;

      if (getsockopt(fd, SOL_SOCKET, SO_ERROR, &errcode, &len) != 0) {
         PerfxConnector_FailErrno(connector, errno);
         return;
      }

      if (errcode != 0) {
         PerfxConnector_FailErrno(connector, errcode);
         PerfxObject_Unref(connector);
         return;
      } else {
         connector->state = STATE_ESTABLISHED;
         PerfxMain_AddIdle(PerfxConnector_Callback, connector);
         return;
      }
   }

   ASSERT_NOT_REACHED();
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnector_ConnectUnix --
 *
 *       Connect to a UNIX socket.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxConnector_ConnectUnix(PerfxConnector *connector) /* IN */
{
   struct sockaddr_un saddr = { 0 };
   int rc;
   int sd;

   ASSERT(connector);
   ASSERT_INT(connector->state, ==, STATE_CONNECTING);
   ASSERT_INT(connector->type, ==, TYPE_UNIX);
   ASSERT(connector->u.unix_.path);

   errno = 0;

   if ((sd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
      PerfxConnector_FailErrno(connector, errno);
      return;
   }

   fcntl(sd, F_SETFL, O_NONBLOCK);
   connector->read_fd = sd;
   connector->write_fd = sd;

   saddr.sun_family = AF_UNIX;
   strncpy(saddr.sun_path, connector->u.unix_.path, sizeof saddr.sun_path);

   errno = 0;

   rc = connect(sd, (struct sockaddr *)&saddr, SUN_LEN(&saddr));
   if (rc < 0) {
      if (errno == EINPROGRESS) {
         connector->fd_handler =
            PerfxMain_AddPoll(sd, PERFX_MAIN_POLL_OUT | PERFX_MAIN_POLL_HUP,
                              PerfxConnector_ConnectCb,
                              PerfxObject_Ref(connector));
      } else {
         PerfxConnector_FailErrno(connector, errno);
      }
      return;
   }

   connector->state = STATE_ESTABLISHED;
   PerfxMain_AddIdle(PerfxConnector_Callback,
                     PerfxObject_Ref(connector));
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnector_ConnectTcp --
 *
 *       Handle connecting to a tcp host.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxConnector_ConnectTcp(PerfxConnector *connector) /* IN */
{
   struct sockaddr_in saddr = { 0 };
   struct hostent hent;
   int rc;
   int sd;

   ASSERT(connector);
   ASSERT_INT(connector->state, ==, STATE_CONNECTING);
   ASSERT_INT(connector->type, ==, TYPE_TCP);
   ASSERT(connector->u.tcp.hostname);

   /*
    * TODO: Make the resolver asynchronous.
    */
   hent = *gethostbyname(connector->u.tcp.hostname);

   errno = 0;

   if ((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
      PerfxConnector_FailErrno(connector, errno);
      return;
   }

   fcntl(sd, F_SETFL, O_NONBLOCK);
   connector->read_fd = sd;
   connector->write_fd = sd;

   saddr.sin_family = AF_INET;
   saddr.sin_addr.s_addr = ((struct in_addr *)(hent.h_addr))->s_addr;
   saddr.sin_port = htons(connector->u.tcp.port);

   errno = 0;

   rc = connect(sd, (struct sockaddr *)&saddr, sizeof saddr);
   if (rc < 0) {
      if (errno == EINPROGRESS) {
         connector->fd_handler =
            PerfxMain_AddPoll(sd, PERFX_MAIN_POLL_OUT | PERFX_MAIN_POLL_HUP,
                              PerfxConnector_ConnectCb,
                              PerfxObject_Ref(connector));
      } else {
         PerfxConnector_FailErrno(connector, errno);
      }
      return;
   }

   connector->state = STATE_ESTABLISHED;
   PerfxMain_AddIdle(PerfxConnector_Callback,
                     PerfxObject_Ref(connector));
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnector_Connect --
 *
 *       Try to connect to the target destination. The callback will be
 *       executed when the operation completes or fails.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxConnector_Connect(PerfxConnector *connector) /* IN */
{
   ASSERT(connector);
   ASSERT_INT(connector->state, ==, STATE_0);

   connector->state = STATE_CONNECTING;

   switch (connector->type) {
   case TYPE_PIPE:
      PerfxConnector_ConnectPipe(connector);
      break;
   case TYPE_UNIX:
      PerfxConnector_ConnectUnix(connector);
      break;
   case TYPE_TCP:
      PerfxConnector_ConnectTcp(connector);
      break;
   default:
      ASSERT_NOT_REACHED();
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnector_SetCallback --
 *
 *       Set a callback to be executed upon completion or failure.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxConnector_SetCallback(PerfxConnector *connector,       /* IN */
                           PerfxConnectorCallback callback, /* IN */
                           void *data)                      /* IN */
{
   ASSERT(connector);

   connector->callback = callback;
   connector->callback_data = data;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxConnector_GetFds --
 *
 *       Retrieve the file descriptors created by the connector.
 *
 * Returns:
 *       TRUE if fds were set, otherwise FALSE.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

bool
PerfxConnector_GetFds(PerfxConnector *connector, /* IN */
                      int *read_fd,              /* OUT */
                      int *write_fd)             /* OUT */
{
   ASSERT(connector);

   if (connector->state == STATE_ESTABLISHED) {
      if (read_fd) {
         *read_fd = connector->read_fd;
      }
      if (write_fd) {
         *write_fd = connector->write_fd;
      }
      return TRUE;
   }

   return FALSE;
}
