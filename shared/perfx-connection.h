/* perfx-connection.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_CONNECTION_H
#define PERFX_CONNECTION_H


#include "perfx-array.h"
#include "perfx-buffer.h"
#include "perfx-macros.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


#define PERFX_TYPE_CONNECTION (PerfxConnection_GetType())


typedef struct _PerfxConnection      PerfxConnection;
typedef struct _PerfxConnectionClass PerfxConnectionClass;
typedef enum   _PerfxConnectionType  PerfxConnectionType;


typedef void (*PerfxConnectionStateCallback) (PerfxConnection *connection,
                                              void *data);


enum _PerfxConnectionType {
   PERFX_CONNECTION_TCP = 1,
   PERFX_CONNECTION_FD,
   PERFX_CONNECTION_PIPE,
};


struct _PerfxConnection {
   PerfxObject parent;

   PerfxConnectionStateCallback state_callback;
   void *state_data;

   int type;

   int read_fd;
   int write_fd;

   uint32_t connect_handler;
   uint32_t read_handler;
   uint32_t write_handler;

   PerfxArray *writes;

   PerfxType protocol_type;
   void *protocol;

   union {
      struct {
         char *hostname;
         uint16_t port;
      } tcp;
   } u;
};


struct _PerfxConnectionClass {
   PerfxClass parent_class;
};


PerfxType        PerfxConnection_GetType          (void) PERFX_GNUC_CONST;
void             PerfxConnection_Connect          (PerfxConnection *connection);
PerfxConnection *PerfxConnection_CreateFd         (int fd);
PerfxConnection *PerfxConnection_CreatePipe       (int read_fd,
                                                   int write_fd);
PerfxConnection *PerfxConnection_CreateTcp        (const char *host,
                                                   uint16_t port);
void             PerfxConnection_Disconnect       (PerfxConnection *connection);
bool             PerfxConnection_IsConnected      (PerfxConnection *connection);
bool             PerfxConnection_IsReadable       (PerfxConnection *connection);
bool             PerfxConnection_IsWriteable      (PerfxConnection *connection);
void             PerfxConnection_Send             (PerfxConnection *connection,
                                                   PerfxBuffer *buffer);
void             PerfxConnection_SetStateCallback (PerfxConnection *connection,
                                                   PerfxConnectionStateCallback callback,
                                                   void *data);
void             PerfxConnection_SetProtocolType  (PerfxConnection *connection,
                                                   PerfxType protocol_type);


PERFX_END_DECLS


#endif /* PERFX_CONNECTION_H */
