/* perfx-listener.h
 *
 * Copyright (C) 2011 Christian Hergert <chris@dronelabs.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef PERFX_LISTENER_H
#define PERFX_LISTENER_H


#include <netdb.h>

#include "perfx-macros.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


#define PERFX_TYPE_LISTENER (PerfxListener_GetType())


typedef struct {
   PerfxObject parent;

   /*< private >*/
   PerfxType protocol_type;
   int handler_id;
   int fd;
   uint16_t backlog;
} PerfxListener;


typedef struct {
   PerfxClass parent;
} PerfxListenerClass;


PerfxType      PerfxListener_GetType         (void) PERFX_GNUC_CONST;
PerfxListener *PerfxListener_CreateTcp       (struct in_addr *addr,
                                              uint16_t port);
void           PerfxListener_SetProtocolType (PerfxListener *listener,
                                              PerfxType protocol_type);
void           PerfxListener_Start           (PerfxListener *listener);
void           PerfxListener_Stop            (PerfxListener *listener);


PERFX_END_DECLS


#endif /* PERFX_LISTENER_H */
