#include <dirent.h>
#include <dlfcn.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>

#define LOG_DOMAIN "app"

#include "perfx-app.h"
#include "perfx-array.h"
#include "perfx-debug.h"
#include "perfx-hashtable.h"
#include "perfx-listener.h"
#include "perfx-log.h"
#include "perfx-main.h"
#include "perfx-pfx-protocol.h"
#include "perfx-plugin.h"


static PerfxPerfTree  *gPerfTree;
static PerfxArray     *gListeners;
static bool            gInitialized;
static PerfxHashTable *gPlugins;


/*
 *--------------------------------------------------------------------------
 *
 * PerfxApp_LoadPlugin --
 *
 *       Attempts to load a given plugin.
 *
 * Returns:
 *       TRUE if the plugin was loaded; otherwise FALSE.
 *
 * Side effects:
 *       A given plugin may be loaded and PerfxPlugin_Init() called.
 *
 *--------------------------------------------------------------------------
 */

static bool
PerfxApp_LoadPlugin(PerfxPlugin *plugin) /* IN */
{
   void (*PerfxPlugin_Init) (void) = NULL;
   const char *path;

   ASSERT(plugin);

   path = PerfxPlugin_GetPath(plugin);
   if (PerfxHashTable_Lookup(gPlugins, path)) {
      PerfxLog_Log("plugin", "Plugin already loaded: %s", path);
      return FALSE;
   }

   if (!PerfxPlugin_GetSymbol(plugin,
                              "PerfxPlugin_Init",
                              (void **)&PerfxPlugin_Init)) {
      PerfxLog_Log("plugin", "Plugin missing PerfxPlugin_Init(): %s",
                   PerfxPlugin_GetPath(plugin));
      return FALSE;
   }

   PerfxPlugin_Init();
   PerfxHashTable_Insert(gPlugins, (char *)path, plugin);

   return TRUE;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxApp_LoadPlugins --
 *
 *       Load the plugins discovered at runtime.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       Plugins are loaded and dlopen() is called. The PerfxPlugin_Init()
 *       function may be called in the module.
 *
 *--------------------------------------------------------------------------
 */

static void
PerfxApp_LoadPlugins(void)
{
   PerfxPlugin *plugin;
   const char *plugin_dir;
   struct dirent *entry_p = NULL;
   struct dirent entry = { 0 };
   void *func = NULL;
   char path[PATH_MAX];
   DIR *dir;

   gPlugins = PerfxHashTable_Create(32, perfx_str_hash, perfx_str_equal,
                                    free, NULL);

   if (!(plugin_dir = getenv("PERFX_PLUGIN_DIR"))) {
      plugin_dir = PACKAGE_LIBDIR "/perfx/agent";
   }

   if (!(dir = opendir(plugin_dir))) {
      PerfxLog_Log("plugin", "Plugin directory not found: %s", plugin_dir);
      return;
   }

   while (!readdir_r(dir, &entry, &entry_p) && entry_p) {
      if (!perfx_strcmp0(entry.d_name, ".") ||
          !perfx_strcmp0(entry.d_name, "..")) {
         continue;
      }

      memset(path, 0, sizeof path);
      snprintf(path, sizeof path - 1, "%s/%s",
               plugin_dir, entry.d_name);

      if (PerfxPlugin_LooksLikePlugin(path)) {
         if (!(plugin = PerfxPlugin_Create(path, PERFX_PLUGIN_NOW))) {
            PerfxLog_Log("plugin", "%s", PerfxPlugin_GetError(NULL));
            continue;
         }

         if (!PerfxPlugin_GetSymbol(plugin, "PerfxPlugin_Init", &func)) {
            PerfxLog_Log("plugin",
                         "Plugin missing PerfxPlugin_Init(): %s",
                         path);
            PerfxPlugin_Free(plugin);
            continue;
         }

         if (!PerfxApp_LoadPlugin(plugin)) {
            PerfxPlugin_Free(plugin);
            continue;
         }

         PerfxLog_Log("plugin", "Plugin loaded: %s",
                      PerfxPlugin_GetPath(plugin));
      }
   }

   closedir(dir);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxApp_GetPerfTree --
 *
 *       Retrieves the PerfxPortTree for the agent.
 *
 * Returns:
 *       A PerfxPortTree that is owned by the application.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxPerfTree *
PerfxApp_GetPerfTree(void)
{
   ASSERT(gInitialized);
   return gPerfTree;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxApp_Init --
 *
 *       Initialize the agent on startup.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxApp_Init(void)
{
   ASSERT(!gInitialized);

   PerfxLog_SetUseStdout(TRUE);
   gListeners = PerfxArray_Create(FALSE, sizeof(PerfxListener *));
   gPerfTree = PerfxPerfTree_Create();
   PerfxMain_Create();
   gInitialized = TRUE;

   PerfxApp_LoadPlugins();
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxApp_Destroy --
 *
 *       Cleanup after running the agent.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxApp_Destroy(void)
{
   PerfxListener *listener;
   uint32_t i;

   ASSERT(gInitialized);

   for (i = 0; i < gListeners->len; i++) {
      listener = PerfxArray_Index(gListeners, PerfxListener *, i);
      ASSERT(listener);
      PerfxListener_Stop(listener);
      PerfxObject_Unref(listener);
   }

   PerfxArray_Free(gListeners);

   PerfxMain_Destroy();
}


static void
GetCpuValueCb(void *source,
              PerfxAsyncResult *result,
              void *user_data)
{
   PerfxValue value = { 0 };

   ASSERT(result);
   ASSERT(!user_data);

   PerfxPerfTree_GetFinish(source, result, &value, NULL);
   LOG("idle=%s", PerfxValue_ToString(&value));
   PerfxValue_Unset(&value);
}


static bool
GetCpuCb(void *data)
{
   PerfxPerfTree *tree;
   PerfxPerfTreePath *path;

   tree = PerfxApp_GetPerfTree();
   path = PerfxPerfTreePath_Create("/cpu/0/idle");
   PerfxPerfTree_Get(tree, path, GetCpuValueCb, NULL);

   return TRUE;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxApp_Run --
 *
 *       Blocking call to run the application.
 *
 * Returns:
 *       A code to pass back to the shell.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

int
PerfxApp_Run(int argc,    /* IN */
             char **argv) /* IN */
{
   PerfxListener *listener;

   ASSERT(gInitialized);

   if (TRUE) {
      TODO("Check parameters if we should start TCP port");

      listener = PerfxListener_CreateTcp(NULL, 2158);
      PerfxListener_SetProtocolType(listener, PERFX_TYPE_PFX_PROTOCOL);
      PerfxArray_Append(gListeners, listener);
      PerfxListener_Start(listener);
   }

   if (TRUE) {
      /*
       * XXX: Testing code while we get things bootstrapped.
       */
      PerfxMain_AddTimeout(1000, GetCpuCb, NULL);
   }

   PerfxMain_Run();

   return 0;
}
