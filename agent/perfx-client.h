#ifndef PERFX_CLIENT_H
#define PERFX_CLIENT_H


#include "perfx-auth-info.h"
#include "perfx-macros.h"
#include "perfx-types.h"
#include "perfx-tree-path.h"


PERFX_BEGIN_DECLS


typedef struct {
   uint32_t identity;
} PerfxClient;


void     PerfxClient_Authenticate (PerfxClient *client,
                                   PerfxAuthInfo *auth_info);
void     PerfxClient_Init         (PerfxClient *client);
void     PerfxClient_Destroy      (PerfxClient *client);
uint32_t PerfxClient_Subscribe    (PerfxClient *client,
                                   PerfxTreePath *path,
                                   uint32_t interval_msec);
void     PerfxClient_Unsubscribe  (PerfxClient *client,
                                   uint32_t subscription);


PERFX_END_DECLS


#endif /* PERFX_CLIENT_H */
