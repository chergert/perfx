bin_PROGRAMS =
bin_PROGRAMS += perfx-agent


perfx_agent_SOURCES =
perfx_agent_SOURCES += agent/perfx-agent.c
perfx_agent_SOURCES += agent/perfx-app.c
perfx_agent_SOURCES += agent/perfx-app.h
perfx_agent_SOURCES += agent/perfx-client.c
perfx_agent_SOURCES += agent/perfx-client.h
perfx_agent_SOURCES += agent/perfx-perf-tree.c
perfx_agent_SOURCES += agent/perfx-perf-tree.h
perfx_agent_SOURCES += agent/perfx-pfx-protocol.c
perfx_agent_SOURCES += agent/perfx-pfx-protocol.h


perfx_agent_LDADD =
perfx_agent_LDADD += libshared.la
perfx_agent_LDADD += -ldl


perfx_agent_CFLAGS =
perfx_agent_CFLAGS += -I$(top_srcdir)/shared
perfx_agent_CFLAGS += "-DPACKAGE_LIBDIR=\"$(libdir)\""
perfx_agent_CFLAGS += -rdynamic
