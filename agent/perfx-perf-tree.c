#include <stdio.h>
#include <string.h>

#include "perfx-array.h"
#include "perfx-debug.h"
#include "perfx-hashtable.h"
#include "perfx-perf-tree.h"


/*
 * TODO: Use a Trie to find the target port instead of a hashtable.
 */


struct _PerfxPerfTree
{
   PerfxHashTable *ports;
};


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPerfTree_Create --
 *
 *       Create a new instance of PerfxPerfTree.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

PerfxPerfTree *
PerfxPerfTree_Create(void)
{
   PerfxPerfTree *ret;

   ret = perfx_new0(PerfxPerfTree, 1);
   ret->ports = PerfxHashTable_Create(1024,
                                      perfx_str_hash,
                                      perfx_str_equal,
                                      perfx_free,
                                      (PerfxFreeFunc)PerfxPort_Free);

   return ret;
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPerfTree_Attach --
 *
 *       Attach a port to the tree to receive Get, Set, Subscribe, and
 *       Unsubscribe calls for your path and children.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxPerfTree_Attach(PerfxPerfTree *tree,     /* IN */
                     PerfxPerfTreePath *path, /* IN */
                     PerfxPort *port)         /* IN */
{
   char *str;

   ASSERT(tree);
   ASSERT(path);
   ASSERT(port);

   str = PerfxPerfTreePath_ToString(path);
   PerfxHashTable_Insert(tree->ports, str, port);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPerfTree_Detach --
 *
 *       Detaches the port found at a given path.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxPerfTree_Detach(PerfxPerfTree *tree,     /* IN */
                     PerfxPerfTreePath *path) /* IN */
{
   ASSERT(tree);
   ASSERT(path);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPerfTree_GetPortForPath --
 *
 *       Finds the nearest PerfxPort to the target path. This is done by
 *       starting as deep as possible and working our way up the tree
 *       until a port matching that subpath is found.
 *
 *       This is currently implemented using a hashtable and continually
 *       stripping off a part of the path until we reach the root.
 *
 * Returns:
 *       A PerfxPort if successful; otherwise NULL.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static PerfxPort *
PerfxPerfTree_GetPortForPath(PerfxPerfTree *tree,     /* IN */
                             PerfxPerfTreePath *path) /* IN */
{
   PerfxPort *port = NULL;
   int32_t i;
   char str[PERFX_PERF_TREE_PATH_MAX];

   ASSERT(tree);
   ASSERT(path);

   str[0] = '/';
   memcpy(str + 1, (char *)path->data + path->len, path->data_len - path->len);
   for (i = 1; i < path->len; i++) {
      str[path->data[i]] = '/';
   }

   if ((port = PerfxHashTable_Lookup(tree->ports, str))) {
      return port;
   }

   for (i = path->len - 1; i > 0; i--) {
      str[path->data[i]] = '\0';
      if ((port = PerfxHashTable_Lookup(tree->ports, str))) {
         return port;
      }
   }

   return NULL;
}


static PerfxMessage *
PerfxPerfTreeMessage_Create(PerfxPerfTree *tree,          /* IN */
                            PerfxPerfTreePath *path,      /* IN */
                            PerfxPerfTreeCommand command, /* IN */
                            PerfxAsyncCallback callback,  /* IN */
                            void *user_data)              /* IN */
{
   PerfxPerfTreeMessage *message;

   ASSERT(command);
   ASSERT(path);

   message = PerfxMessage_Create(sizeof *message + path->data_len, NULL);
   message->type = command;
   message->async = PerfxAsyncResult_Create(tree, callback, user_data);
   memcpy(&message->path, path, sizeof *path + path->data_len);

   return (PerfxMessage *)message;
}


void
PerfxPerfTree_Get(PerfxPerfTree *tree,         /* IN */
                  PerfxPerfTreePath *path,     /* IN */
                  PerfxAsyncCallback callback, /* IN */
                  void *user_data)             /* IN */
{
   PerfxPort *port;

   ASSERT(tree);
   ASSERT(path);
   ASSERT(callback);

   if ((port = PerfxPerfTree_GetPortForPath(tree, path))) {
      PerfxPort_Post(port,
                     PerfxPerfTreeMessage_Create(tree,
                                                 path,
                                                 PERFX_PERF_TREE_GET,
                                                 callback,
                                                 user_data));
   }
}


bool
PerfxPerfTree_GetFinish(PerfxPerfTree *tree,      /* IN */
                        PerfxAsyncResult *result, /* IN */
                        PerfxValue *value,        /* OUT */
                        PerfxError **error)       /* OUT */
{
   ASSERT(tree);
   ASSERT(result);
   ASSERT(result->source == tree);

   PerfxValue_Copy(&result->value, value);
   return TRUE;
}
