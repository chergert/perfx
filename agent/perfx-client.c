#include <string.h>

#include "perfx-client.h"
#include "perfx-debug.h"


void
PerfxClient_Init(PerfxClient *client) /* IN/OUT */
{
   ENTRY;
   ASSERT(client);
   memset(client, 0, sizeof *client);
   EXIT;
}


void
PerfxClient_Destroy(PerfxClient *client) /* IN/OUT */
{
   ENTRY;
   EXIT;
}
