#include "perfx-app.h"

int
main(int argc,     /* IN */
     char *argv[]) /* IN */
{
   int ret;

   PerfxApp_Init();
   ret = PerfxApp_Run(argc, argv);
   PerfxApp_Destroy();

   return ret;
}
