#ifndef PERFX_PERF_TREE_H
#define PERFX_PERF_TREE_H


#include "perfx-async.h"
#include "perfx-error.h"
#include "perfx-macros.h"
#include "perfx-perf-tree-path.h"
#include "perfx-port.h"
#include "perfx-types.h"
#include "perfx-value.h"


PERFX_BEGIN_DECLS


typedef struct _PerfxPerfTree        PerfxPerfTree;
typedef struct _PerfxPerfTreeMessage PerfxPerfTreeMessage;
typedef enum   _PerfxPerfTreeCommand PerfxPerfTreeCommand;


enum _PerfxPerfTreeCommand
{
   PERFX_PERF_TREE_GET   = (('G' << 24) | ('E' << 16) | ('T' << 8)),
   PERFX_PERF_TREE_SET   = (('S' << 24) | ('E' << 16) | ('T' << 8)),
   PERFX_PERF_TREE_SUB   = (('S' << 24) | ('U' << 16) | ('B' << 8)),
   PERFX_PERF_TREE_UNSUB = (('U' << 24) | ('S' << 16) | ('U' << 8) | 'B'),
};


struct _PerfxPerfTreeMessage
{
   PerfxMessage message;
   PerfxPerfTreeCommand type;
   PerfxAsyncResult *async;
   PerfxPerfTreePath path;
};


PerfxPerfTree *PerfxPerfTree_Create      (void);
void           PerfxPerfTree_Free        (PerfxPerfTree *tree);
void           PerfxPerfTree_Attach      (PerfxPerfTree *tree,
                                          PerfxPerfTreePath *path,
                                          PerfxPort *port);
void           PerfxPerfTree_Detach      (PerfxPerfTree *tree,
                                          PerfxPerfTreePath *path);
void           PerfxPerfTree_Get         (PerfxPerfTree *tree,
                                          PerfxPerfTreePath *path,
                                          PerfxAsyncCallback callback,
                                          void *user_data);
bool           PerfxPerfTree_GetFinish   (PerfxPerfTree *tree,
                                          PerfxAsyncResult *result,
                                          PerfxValue *value,
                                          PerfxError **error);
void           PerfxPerfTree_Set         (PerfxPerfTree *tree,
                                          PerfxPerfTreePath *path,
                                          const PerfxValue *value,
                                          PerfxAsyncCallback callback,
                                          void *user_data);
bool           PerfxPerfTree_SetFinish   (PerfxPerfTree *tree,
                                          PerfxAsyncResult *result,
                                          PerfxError **error);
uint32_t       PerfxPerfTree_Subscribe   (PerfxPerfTree *tree,
                                          PerfxPerfTreePath *path,
                                          PerfxValueCallback callback,
                                          void *user_data);
void           PerfxPerfTree_Unsubscribe (PerfxPerfTree *tree,
                                          uint32_t subscription_id);


PERFX_END_DECLS


#endif /* PERFX_PERF_TREE_H */
