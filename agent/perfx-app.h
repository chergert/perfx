#ifndef PERFX_APP_H
#define PERFX_APP_H


#include "perfx-macros.h"
#include "perfx-perf-tree.h"
#include "perfx-types.h"


PERFX_BEGIN_DECLS


void           PerfxApp_Destroy     (void);
PerfxPerfTree *PerfxApp_GetPerfTree (void);
void           PerfxApp_Init        (void);
int            PerfxApp_Run         (int argc,
                                     char **argv);


PERFX_END_DECLS


#endif /* PERFX_APP_H */
