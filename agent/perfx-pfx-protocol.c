#include "perfx-debug.h"
#include "perfx-pfx-protocol.h"


static void
PerfxPfxProtocol_ConnectionMade(PerfxProtocol *protocol,     /* IN */
                                PerfxConnection *conncetion) /* IN */
{
   PerfxPfxProtocol *pfx = (PerfxPfxProtocol *)protocol;

   ENTRY;
   ASSERT(pfx);
   ASSERT(conncetion);
   PerfxClient_Init(&pfx->client);
   EXIT;
}


static void
PerfxPfxProtocol_ConnectionLost(PerfxProtocol *protocol, /* IN */
                                uint32_t reason)         /* IN */
{
   PerfxPfxProtocol *pfx = (PerfxPfxProtocol *)protocol;

   ENTRY;
   ASSERT(pfx);
   PerfxClient_Destroy(&pfx->client);
   EXIT;
}


static void
PerfxPfxProtocol_DataReceived(PerfxProtocol *protocol, /* IN */
                              PerfxBuffer *buffer)     /* IN */
{
   ENTRY;
   TODO("Handle incoming buffered data");
   EXIT;
}


static void
PerfxPfxProtocol_Destroy(PerfxObject *object) /* IN */
{
   ENTRY;
   EXIT;
}


PerfxType
PerfxPfxProtocol_GetType(void)
{
   PerfxProtocolClass *class;
   PerfxClass *klass;
   static PerfxType type = 0;

   if (!type) {
      type = PerfxClass_Register("PerfxProtocol", PERFX_TYPE_PROTOCOL,
                                 sizeof(PerfxPfxProtocolClass),
                                 sizeof(PerfxPfxProtocol),
                                 TRUE);
      klass = (void *)PerfxType_GetClass(type);
      klass->Destroy = PerfxPfxProtocol_Destroy;
      class = (PerfxProtocolClass *)PerfxType_GetClass(type);
      class->ConnectionMade = PerfxPfxProtocol_ConnectionMade;
      class->ConnectionLost = PerfxPfxProtocol_ConnectionLost;
      class->DataReceived = PerfxPfxProtocol_DataReceived;
   }

   return type;
}
