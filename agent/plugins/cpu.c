#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "perfx-app.h"
#include "perfx-cpu.h"
#include "perfx-debug.h"
#include "perfx-file.h"
#include "perfx-message.h"
#include "perfx-port.h"
#include "perfx-types.h"


#undef LOG_DOMAIN
#define LOG_DOMAIN "cpu"


static struct {
   int user;
   int nice;
   int system;
   int idle;
   int iowait;
   int irq;
   int softirq;
   int vmstolen;
   int vmguest;
} cpuinfo[64];
static int n_cpuinfo;


static void
Cpu_ParseStat(void)
{
   size_t len;
   char *buf;
   char *line;
   int v[10];

   if (!(buf = PerfxFile_GetContents("/proc/stat", &len, NULL))) {
      return;
   }

   line = buf;

   for (line = buf; line; line = strstr(line, "\n")) {
      line++;
      if (10 == sscanf(line, "cpu%d %d %d %d %d %d %d %d %d %d",
                       &v[0], &v[1], &v[2], &v[3], &v[4],
                       &v[5], &v[6], &v[7], &v[8], &v[9])) {
         if (v[0] < n_cpuinfo) {
            /*
             * TODO: These are absolute values. we need to track
             * this + last version for difference.
             */
            cpuinfo[v[0]].user = v[1];
            cpuinfo[v[0]].nice = v[2];
            cpuinfo[v[0]].system = v[3];
            cpuinfo[v[0]].idle = v[4];
            cpuinfo[v[0]].iowait = v[5];
            cpuinfo[v[0]].irq = v[6];
            cpuinfo[v[0]].softirq = v[7];
            cpuinfo[v[0]].vmstolen = v[8];
            cpuinfo[v[0]].vmguest = v[9];
            LOG("Parsed cpu %d", v[0]);
         }
      }
   }

   perfx_free(buf);
}


static void
Cpu_HandleGet(PerfxPerfTreeMessage *message) /* IN */
{
   const char *part;
   double val;
   int32_t cpu;

   ASSERT(message);

   if (message->path.len < 2) {
      return;
   }

   if ((part = PerfxPerfTreePath_GetPart(&message->path, 1))) {
      if (perfx_int_parse(&cpu, part, 10)) {
         LOG("Got CPU %d", cpu);
         Cpu_ParseStat();
         PerfxValue_Init(&message->async->value, PERFX_VALUE_DOUBLE);
         val = cpuinfo[cpu].idle / (double)(cpuinfo[cpu].system + cpuinfo[cpu].idle + cpuinfo[cpu].nice);
         PerfxValue_SetDouble(&message->async->value, val);
      } else if (!perfx_strcmp0(part, "all")) {
         LOG("Got CPU 'all'");
         PerfxValue_Init(&message->async->value, PERFX_VALUE_DOUBLE);
         PerfxValue_SetDouble(&message->async->value, 12);
      }
   }
}


/*
 *--------------------------------------------------------------------------
 *
 * Cpu_PortHandler --
 *
 *       Handle incoming messages delivered to our plugin port.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       None.
 *
 *--------------------------------------------------------------------------
 */

static void
Cpu_PortHandler(PerfxPort *port,               /* IN */
                PerfxPerfTreeMessage *message, /* IN */
                void *data)                    /* IN */
{
   char *str;

   ASSERT(port);
   ASSERT(message);

   str = PerfxPerfTreePath_ToString(&message->path);
   LOG("GET -> %s", str);
   perfx_free(str);

   switch (message->type) {
   case PERFX_PERF_TREE_GET:
      Cpu_HandleGet(message);
      break;
   case PERFX_PERF_TREE_SET:
   case PERFX_PERF_TREE_SUB:
   case PERFX_PERF_TREE_UNSUB:
      LOG("Got command: %u", message->type);
      break;
   default:
      ASSERT_NOT_REACHED();
   }

   PerfxAsyncResult_Callback(message->async);
}


/*
 *--------------------------------------------------------------------------
 *
 * PerfxPlugin_Init --
 *
 *       Plugin entry point. Register our handlers on the PerfTree.
 *
 * Returns:
 *       None.
 *
 * Side effects:
 *       The plugin handlers are registered.
 *
 *--------------------------------------------------------------------------
 */

void
PerfxPlugin_Init(void)
{
   PerfxPort *port;

   n_cpuinfo = PerfxCpu_GetCount();

   LOG("Cpu plugin init.");
   port = PerfxPort_Create(1, (PerfxPortHandler)Cpu_PortHandler, NULL);
   PerfxPerfTree_Attach(PerfxApp_GetPerfTree(),
                        PerfxPerfTreePath_Create("/cpu"),
                        port);
}
