lib_LTLIBRARIES += libperfxcpu.la


libperfxcpu_la_SOURCES =
libperfxcpu_la_SOURCES += agent/plugins/cpu.c


libperfxcpu_la_CFLAGS =
libperfxcpu_la_CFLAGS += -I$(top_srcdir)/agent
libperfxcpu_la_CFLAGS += -I$(top_srcdir)/shared


libperfxcpu_la_LIBADD =
libperfxcpu_la_LIBADD += libshared.la
