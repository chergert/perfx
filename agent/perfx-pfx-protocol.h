#ifndef PERFX_PFX_PROTOCOL_H
#define PERFX_PFX_PROTOCOL_H


#include "perfx-client.h"
#include "perfx-protocol.h"


PERFX_BEGIN_DECLS


#define PERFX_TYPE_PFX_PROTOCOL (PerfxPfxProtocol_GetType())


typedef struct {
   PerfxProtocol parent;

   /*< private >*/
   PerfxClient client;
} PerfxPfxProtocol;


typedef struct {
   PerfxProtocolClass parent_class;
} PerfxPfxProtocolClass;


PerfxType PerfxPfxProtocol_GetType (void) PERFX_GNUC_CONST;


PERFX_END_DECLS


#endif /* PERFX_PFX_PROTOCOL_H */
