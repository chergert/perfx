#!/bin/sh

#
# --------------------------------------------------------------------------
#
# Initialize Auto* --
#
#     Ensure that we are running within the source directory. If so,
#     go ahead and initialize autoconf and configure the project.
#
# --------------------------------------------------------------------------
#


srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.


test -f "configure.ac" || {
   echo "You must run this script in the top-level perfx directory"
   exit 1
}


autoreconf -v --install || exit $?


test -n "$NOCONFIGURE" ||
(
   "$srcdir/configure" "$@" &&
   echo "Now type 'make' to compile perfx"
)
